*** WooCommerce Disability VAT Exemption Changelog ***

2020-11-10 - version 1.4.4
PLUGINS-791 - Test for compatibility with lastest versions of WP and WooCommerce.

2020-06-19 - version 1.4.3
PLUGINS-459 Fix - Remove PHP Notice on disability VAT variable
PLUGINS-463 Fix - Remove process of VAT after order has been placed
PLUGINS-464 Fix - Bug when fields are optional

2020-05-13 - version 1.4.2
* Fix - Customer changed their mind on filling up the fields and put options to NO and delete all disability field values
* Fix - Charity introduction message missing

2020-04-06 - version 1.4.1
* Add - Option to toggle basic and advanced fields.

2020-03-26 - version 1.4.0
* Add - Option to hide advanced fields
* Add - New styles for disability and charity fields
* Add - Only show fields and body text if claiming exemption
* Add - Add required name of person with disability
* Add - UK enhanced compliance - Charity Custom Text and Input fields
* Add - UK enhanced compliance - Disability Custom Text and Input fields

2018-10-09 - version 1.3.7
* Update - WC tested up to 3.5

2018-05-24 - version 1.3.6
* Update - WC tested up to 3.4
* Add - GDPR policy

2017-12-13 - version 1.3.5
* Add - WC minimum requirements to header.
* Fix - WC 3.3 compatibility.

2017-05-15 - version 1.3.4
* Fix - Remove tax suffix if product is exempt to avoid confusion.
* Fix - Additional updates for WC 3.0 compatibility.

2017-05-08 - version 1.3.3
* Fix - Issue where VAT form fields not showing for variable products.
* Fix - Notices of deprecated hook `woocommerce_product_tax_class`.

2017-04-03 - version 1.3.2
 * Fix - Update for WC 3.0 compatibility.

2016-06-12 - version 1.3.1
 * Fix - Trigger update checkout when VAT number fields are updated when user is not logged in

2013-01-11 - version 1.3.0
 * WC 2.0 Compat
 * General cleanup

2012-12-04 - version 1.2.1
 * New updater

2012-10-11 - version 1.2
 * Name of person to which VAT exemption applies field

2012-03-03 - version 1.1
 * Updated woo updater
 * Only show fields when eligible items are in the cart

2012-01-26 - version 1.0.3
 * WC 1.4 compatibility
 * Fixed notices
 * Form field for checkout - function change
 * Woo Updater
 * Store IP address

2011-11-15 - version 1.0.2
 * Changed textdomain

2011-11-01 - version 1.0.1
 * Show details on order emails

2011-10-13 - version 1.0
 * First Release
