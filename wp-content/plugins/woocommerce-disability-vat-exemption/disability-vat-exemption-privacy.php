<?php
if ( ! class_exists( 'WC_Abstract_Privacy' ) ) {
	return;
}

class WC_Disability_VAT_Exemption_Privacy extends WC_Abstract_Privacy {
	/**
	 * Constructor
	 *
	 */
	public function __construct() {
		parent::__construct( __( 'VAT Exemption', 'wc_disability_exemption' ) );

		$this->add_exporter( 'woocommerce-vat-exemption-order-data', __( 'WooCommerce VAT Exemption Order Data', 'wc_disability_exemption' ), array( $this, 'order_data_exporter' ) );

		$this->add_eraser( 'woocommerce-vat-exemption-order-data', __( 'WooCommerce VAT Exemption Data', 'wc_disability_exemption' ), array( $this, 'order_data_eraser' ) );
	}

	/**
	 * Returns a list of orders.
	 *
	 * @param string  $email_address
	 * @param int     $page
	 *
	 * @return array WP_Post
	 */
	protected function get_orders( $email_address, $page ) {
		$user = get_user_by( 'email', $email_address ); // Check if user has an ID in the DB to load stored personal data.

		$order_query    = array(
			'limit'          => 10,
			'page'           => $page,
		);

		if ( $user instanceof WP_User ) {
			$order_query['customer_id'] = (int) $user->ID;
		} else {
			$order_query['billing_email'] = $email_address;
		}

		return wc_get_orders( $order_query );
	}

	/**
	 * Gets the message of the privacy to display.
	 *
	 */
	public function get_privacy_message() {
		return wpautop( sprintf( __( 'By using this extension, you may be storing personal data or sharing data with an external service. <a href="%s" target="_blank">Learn more about how this works, including what you may want to include in your privacy policy.</a>', 'wc_disability_exemption' ), 'https://docs.woocommerce.com/document/marketplace-privacy/#woocommerce-disability-vat-exemption' ) );
	}

	/**
	 * Handle exporting data for Orders.
	 *
	 * @param string $email_address E-mail address to export.
	 * @param int    $page          Pagination of data.
	 *
	 * @return array
	 */
	public function order_data_exporter( $email_address, $page = 1 ) {
		$done           = false;
		$data_to_export = array();

		$orders = $this->get_orders( $email_address, (int) $page );

		$done = true;

		if ( 0 < count( $orders ) ) {
			foreach ( $orders as $order ) {
				$data_to_export[] = array(
					'group_id'    => 'woocommerce_orders',
					'group_label' => __( 'Orders', 'wc_disability_exemption' ),
					'item_id'     => 'order-' . $order->get_id(),
					'data'        => array(
						array(
							'name'  => __( 'Disability exemption', 'wc_disability_exemption' ),
							'value' => get_post_meta( $order->get_id(), 'disability_exemption', true ),
						),
						array(
							'name'  => __( 'Exemption reason', 'wc_disability_exemption' ),
							'value' => get_post_meta( $order->get_id(), 'VAT Exemption reason', true ),
						),
						array(
							'name'  => __( 'Exemption person', 'wc_disability_exemption' ),
							'value' => get_post_meta( $order->get_id(), 'VAT Exemption person', true ),
						),
						array(
							'name'  => __( 'Charity Reg. Name', 'wc_disability_exemption' ),
							'value' => get_post_meta( $order->get_id(), 'Charity Name/Address', true ),
						),
						array(
							'name'  => __( 'Charity Reg. No.', 'wc_disability_exemption' ),
							'value' => get_post_meta( $order->get_id(), 'Charity Reg. No', true ),
						),
						array(
							'name'  => __( 'Customer IP', 'wc_disability_exemption' ),
							'value' => get_post_meta( $order->get_id(), 'Customer IP', true ),
						),
					),
				);
			}

			$done = 10 > count( $orders );
		}

		return array(
			'data' => $data_to_export,
			'done' => $done,
		);
	}

	/**
	 * Finds and erases order data by email address.
	 *
	 * @since 3.4.0
	 * @param string $email_address The user email address.
	 * @param int    $page  Page.
	 * @return array An array of personal data in name value pairs
	 */
	public function order_data_eraser( $email_address, $page ) {
		$orders = $this->get_orders( $email_address, (int) $page );

		$items_removed  = false;
		$items_retained = false;
		$messages       = array();

		foreach ( (array) $orders as $order ) {
			$order = wc_get_order( $order->get_id() );

			list( $removed, $retained, $msgs ) = $this->maybe_handle_order( $order );
			$items_removed  |= $removed;
			$items_retained |= $retained;
			$messages        = array_merge( $messages, $msgs );
		}

		// Tell core if we have more orders to work on still
		$done = count( $orders ) < 10;

		return array(
			'items_removed'  => $items_removed,
			'items_retained' => $items_retained,
			'messages'       => $messages,
			'done'           => $done,
		);
	}

	/**
	 * Handle eraser of data tied to Orders
	 *
	 * @param WC_Order $order
	 * @return array
	 */
	protected function maybe_handle_order( $order ) {
		$order_id      = $order->get_id();
		$disability_exemption = get_post_meta( $order_id, 'disability_exemption', true );
		$exemption_reason     = get_post_meta( $order_id, 'VAT Exemption reason', true );
		$exemption_person     = get_post_meta( $order_id, 'VAT Exemption person', true );
		$charity_addr         = get_post_meta( $order_id, 'Charity Name/Address', true );
		$charity_reg          = get_post_meta( $order_id, 'Charity Reg. No', true );
		$customer_ip          = get_post_meta( $order_id, 'Customer IP', true );

		if ( empty( $disability_exemption ) && empty( $exemption_reason ) && empty( $exemption_person )
			&& empty( $charity_addr ) && empty( $charity_reg ) && empty( $customer_ip ) ) {
			return array( false, false, array() );
		}

		delete_post_meta( $order_id, 'disability_exemption' );
		delete_post_meta( $order_id, 'VAT Exemption reason' );
		delete_post_meta( $order_id, 'VAT Exemption person' );
		delete_post_meta( $order_id, 'Charity Name/Address' );
		delete_post_meta( $order_id, 'Charity Reg. No' );
		delete_post_meta( $order_id, 'Customer IP' );

		return array( true, false, array( __( 'VAT Exemption Order Data Erased.', 'wc_disability_exemption' ) ) );
	}
}

new WC_Disability_VAT_Exemption_Privacy();
