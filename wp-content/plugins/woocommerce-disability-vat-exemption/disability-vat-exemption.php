<?php
/**
 * Plugin Name: WooCommerce Disability VAT Exemption
 * Plugin URI: https://woocommerce.com/products/disability-vat-exemption/
 * Description: Lets charities and customers with a disability declare themselves VAT exempt. Requires tax rates for 'Zero Rate' set up. You can set up the zero rate to not apply to shipping via WooCommerce settings.
 * Version: 1.4.4
 * Author: WooCommerce
 * Author URI: https://woocommerce.com
 * Requires at least: 4.4
 * Tested up to: 5.5.3
 * WC requires at least: 2.6
 * WC tested up to: 4.6.2
 *
 * Copyright: © 2009-2017 WooCommerce.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package WC_Disability_VAT_Exemption
 *
 * Woo: 18603:25db92962c597a8754e8a05b300c6c86
 */

/**
 * Required functions.
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates.
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '25db92962c597a8754e8a05b300c6c86', '18603' );

if ( is_woocommerce_active() ) {

	/**
	 * Localisation.
	 */
	load_plugin_textdomain( 'wc_disability_exemption', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	if ( ! class_exists( 'WC_Disability_VAT_Exemption' ) ) {

		define( 'WC_DISABILITY_VAT_EXEMPTION_VERSION', '1.4.3' );

		/***
		 * Plugin's main class.
		 */
		class WC_Disability_VAT_Exemption {

			/**
			 * Disability exemption message during checkout, stored in option.
			 *
			 * @var string.
			 */
			public $checkout_message;


			/** Disability Person Name **/
			/**
			 * Disability person from checkout.
			 *
			 * @var string.
			 */
			public $disability_reason;

			/**
			 * Disability person name posted from checkout.
			 *
			 * @var string.
			 */
			public $disability_person_name;

			/**
			 * Disability person address checkout.
			 *
			 * @var string.
			 */
			public $disability_person_address;

			/**
			 * Disability signed from checkout.
			 *
			 * @var string.
			 */
			public $disability_person_signed;

			/**
			 * Disability date from checkout.
			 *
			 * @var string.
			 */
			public $disability_current_date;


			/**Charity fields**/
			/**
			 * Whether charity form is enabled or not, stored in option.
			 *
			 * @var string.
			 */
			public $enable_charity_form;

			/**
			 * Charity message during checkout, stored in option.
			 *
			 * @var string.
			 */
			public $checkout_charity_message;

			/**
			 * Charity name posted from checkout.
			 *
			 * @var string.
			 */
			public $charity_name;

			/**
			 * Charity registration number posted from checkout.
			 *
			 * @var string.
			 */
			public $charity_registration_number;


			/*** Charity **/
			/**
			 * Charity person name checkout.
			 *
			 * @var string.
			 */
			public $charity_your_name;

			/**
			 * Charity VAT Address checkout.
			 *
			 * @var string.
			 */
			public $charity_vat_address;

			/**
			 * Charity VAT number checkout.
			 *
			 * @var string.
			 */
			public $charity_vat_number;

			/**
			 * Charity Charity signed checkout.
			 *
			 * @var string.
			 */
			public $charity_signed;

			/**
			 * Set options / settings and WP hooks.
			 */
			public function __construct() {

				$this->checkout_message         = get_option( 'woocommerce_disability_exemption_checkout_message' );
				$this->checkout_charity_message         = get_option( 'woocommerce_charity_exemption_checkout_message' );
				$this->enable_charity_form      = get_option( 'woocommerce_enable_charity_form' );
				$this->require_disability_name      = get_option( 'woocommerce_require_disability_name' );
				$this->require_disability_name      = get_option( 'woocommerce_require_disability_name' );
				$this->show_advanced_fields = get_option( 'woocommerce_disability_advanced_fields' );
				$this->show_ip_address = get_option( 'woocommerce_disability_ip_address' );

				$this->default_disability_mesasge = "You should complete this declaration if you are ‘chronically sick or disabled’ and the goods or services are for your own personal or domestic use. A family member or carer can complete this on your behalf if you wish. You can find out more from the Helpsheets on the GOV.UK website. HMRC staff cannot advise whether or not an individual is chronically sickor disabled. A person is ‘chronically sick or disabled’ if he or she is a person:-	with a physical or mental impairment which has a long term and substantial adverse effect upon his or her ability to carry out everyday activities-	with a condition which the medical profession treats as a chronic sickness It does not include an elderly person who is not disabled or chronically sick or any person who is only temporarily disabled or incapacitated, such as with a broken limb. If you are unsure, you should seek guidance from your GP or other medical professional.";
				$this->default_charity_message = "If you are a registered charity providing care or surgical treatment for persons the majority of whom are handicapped please fill in the fields below to declare VAT exemption.";

				// Init settings.
				$this->settings = array(
					array(
						'name' => __( 'Require Disability and Charity fields?', 'wc_disability_exemption' ),
						'desc' => __( 'Require Disability and Charity fields?', 'wc_disability_exemption' ),
						'id'   => 'woocommerce_require_disability_name',
						'type' => 'checkbox',
					),
					array(
						'name' => __( 'Disability Introduction Message', 'wc_disability_exemption' ),
						'desc' => __( 'Enable default disability message (Warning: Your current disability message will be replaced)', 'wc_disability_exemption' ),
						'id'   => 'woocommerce_default_disability_intro_text',
						'type' => 'checkbox',
						'css'  => 'width:100%; height: 100px;',
					),
					array(
						'name' => __( 'Disability Message', 'wc_disability_exemption' ),
						'desc' => __( 'The message that appears at checkout above disability fields.', 'wc_disability_exemption' ),
						'id'   => 'woocommerce_disability_exemption_checkout_message',
						'type' => 'textarea',
						'css'  => 'width:100%; height: 100px;',
					),
					array(
						'name' => __( 'Show charity form?', 'wc_disability_exemption' ),
						'desc' => __( 'Enable the charity VAT exemption form?', 'wc_disability_exemption' ),
						'id'   => 'woocommerce_enable_charity_form',
						'type' => 'checkbox',
					),
					array(
						'name' => __( 'Charity Introduction Message', 'wc_disability_exemption' ),
						'desc' => __( 'Enable default charity message (Warning: Your current charity message will be replaced)', 'wc_disability_exemption' ),
						'id'   => 'woocommerce_default_charity_intro_text',
						'type' => 'checkbox',
						'css'  => 'width:100%; height: 100px;',
					),
					array(
						'name' => __( 'Charity Message', 'wc_disability_exemption' ),
						'desc' => __( 'The message that appears at checkout above charity fields.', 'wc_disability_exemption' ),
						'id'   => 'woocommerce_charity_exemption_checkout_message',
						'type' => 'textarea',
						'css'  => 'width:100%; height: 100px;',
					),
					array(
						'name' => __( 'Show Advanced Fields?', 'wc_disability_exemption' ),
						'desc' => __( 'Enable address, signed and date fields', 'wc_disability_exemption' ),
						'id'   => 'woocommerce_disability_advanced_fields',
						'type' => 'checkbox',
						'css'  => 'width:100%; height: 100px;',
					),
					array(
						'name' => __( 'Show IP address on email?', 'wc_disability_exemption' ),
						'desc' => __( 'Enable printing of IP address in emails', 'wc_disability_exemption' ),
						'id'   => 'woocommerce_disability_ip_address',
						'type' => 'checkbox',
						'css'  => 'width:100%; height: 100px;',
					),
				);

				// Default options.
				add_option( 'woocommerce_enable_charity_form', 'yes' );
				add_option( 'woocommerce_require_disability_name', 'no' );
				add_option( 'woocommerce_default_disability_intro_text', 'no' );
				add_option( 'woocommerce_default_charity_intro_text', 'no' );
				add_option( 'woocommerce_disability_advanced_fields', 'no' );
				add_option( 'woocommerce_disability_ip_address', 'no' );

				// Admin settings.
				add_action( 'woocommerce_settings_tax_options_end', array( $this, 'admin_settings' ) );
				add_action( 'woocommerce_update_options_tax', array( $this, 'save_admin_settings' ) );

				add_action( 'init', array( $this, 'init' ) );

				// Product meta in edit product screen.
				add_action( 'woocommerce_product_options_general_product_data', array( $this, 'write_panel' ) );
				add_action( 'woocommerce_process_product_meta', array( $this, 'write_panel_save' ) );

				// Checkout hooks.
				add_action( 'woocommerce_after_order_notes', array( $this, 'exemption_field' ) );
				add_action( 'woocommerce_checkout_update_order_review', array( $this, 'ajax_update_checkout_totals' ) );
				add_action( 'woocommerce_checkout_process', array( $this, 'process_checkout' ) );
				add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'update_order_meta' ) );

				// Email meta.
				add_filter( 'woocommerce_email_order_meta_keys', array( $this, 'order_meta_keys' ) );

				// Suffix.
				add_filter( 'woocommerce_get_price_suffix', array( $this, 'format_suffix' ), 10, 4 );

				// Inline script at checkout page.
				add_action( 'wp_footer', array( $this, 'inline_script' ) );
		    }

			/**
			 * Init.
			 *
			 * Any actions to perform init `init` hook.
			 *
			 * @since 1.3.3
			 */
			public function init() {
				// Tax class filter.
				if ( version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
					add_filter( 'woocommerce_product_get_tax_class', array( $this, 'get_tax_class' ), 1, 2 ); // >= 3.0
					add_filter( 'woocommerce_product_variation_get_tax_class', array( $this, 'get_tax_class' ), 1, 2 ); // >= 3.0
				} else {
					add_filter( 'woocommerce_product_tax_class', array( $this, 'get_tax_class' ), 1, 2 ); // < 3.0
				}

				require_once( dirname( __FILE__ ) . '/disability-vat-exemption-privacy.php' );
			}

			/**
			 * Render admin settings for Disability VAT exemption.
			 *
			 * @return void
			 */
			public function admin_settings() {
				woocommerce_admin_fields( $this->settings );
			}

			public function check_intro_text(){
				if (get_option('woocommerce_default_disability_intro_text')  == 'yes'){
					update_option('woocommerce_disability_exemption_checkout_message', __($this->default_disability_mesasge, 'wc_disability_exemption'));
				}else{
					add_option( 'woocommerce_disability_exemption_checkout_message', __( $this->checkout_message , 'wc_disability_exemption' ) );
				}

				if (get_option('woocommerce_default_charity_intro_text') == 'yes'){
					update_option('woocommerce_charity_exemption_checkout_message', __($this->default_charity_message, 'wc_disability_exemption'));
				}else{
					add_option( 'woocommerce_charity_exemption_checkout_message', __( $this->checkout_charity_message , 'wc_disability_exemption' ) );
				}
			}

			/**
			 * Save the settings.
			 *
			 * @return void
			 */
			public function save_admin_settings() {
				woocommerce_update_options( $this->settings );
				$this->check_intro_text();
			}

			/**
			 * Render option to enable/disable disability VAT exemption in
			 * general tab of product data.
			 *
			 * @return void
			 */
			public function write_panel() {
		    	echo '<div class="options_group">';
		    	woocommerce_wp_checkbox( array( 'id' => 'disability_exemption', 'label' => __( 'Disability VAT exemption', 'wc_disability_exemption' ), 'description' => __( 'Enable this option if this product is VAT exempt for people with a disability.', 'wc_disability_exemption' ) ) );
		    	echo '</div>';
		    }

		    /**
		     * Save the option in product meta.
		     *
		     * @param mixed $post_id Post ID.
		     * @return void
		     */
		    public function write_panel_save( $post_id ) {
				if ( isset( $_POST['disability_exemption'] ) ) {
		    		update_post_meta( $post_id, 'disability_exemption', 'yes' );
				} else {
					update_post_meta( $post_id, 'disability_exemption', 'no' );
				}
		    }

		    /**
		     * Checks if general vat exemption is initiated.
		     *
		     * @since 1.3.4
		     * @version 1.4.3
		     */
		    public function is_exemption() {
		    	if ( 1 === $this->get_session( 'has_disability' ) || 1 === $this->get_session( 'is_charity' ) ) {
		    		return true;
		    	}

		    	return false;
		    }

		    /**
		     * Checks if product exemption is enabled.
		     *
		     * @since 1.3.4
		     * @version 1.4.3
		     * @param object $product
		     */
		    public function is_product_exempt( $product = null ) {
		    	if ( is_object( $product ) && 'yes' === get_post_meta( $product->get_id(), 'disability_exemption', true ) ) {
		    		return true;
		    	}

		    	return false;
		    }

		    /**
		     * Formats the price tax suffix.
		     *
		     * @since 1.3.4
		     * @version 1.4.3
		     * @param html $html
		     * @param object $product
		     * @param float $price
		     * @param int $qty
		     */
		     public function format_suffix( $html, $product, $price, $qty ) {
		     	if ( ! $this->is_exemption() || ! $this->is_product_exempt( $product ) ) {
		     		return $html;
		     	}

		     	return '';
		     }

			/**
			 * Filter product tax class.
			 *
			 * @param mixed $tax_class Tax class.
			 * @param mixed $product   Product.
			 *
			 * @return mixed Tax class.
			 */
			public function get_tax_class( $tax_class, $product ) {
				$product_id = $product->is_type( 'variation' ) && version_compare( WC_VERSION, '3.0', '>=' ) ? $product->get_parent_id() : $product->get_id();

				if ( $this->get_session( 'has_disability' ) == '1' ) {
					$disability_exemption = get_post_meta( $product_id, 'disability_exemption', true );
					if ( 'yes' === $disability_exemption ) {
						$tax_class = 'Zero Rate';
					}
				}

				if ( $this->get_session( 'is_charity' ) == '1' ) {
					$disability_exemption = get_post_meta( $product_id, 'disability_exemption', true );
					if ( 'yes' === $disability_exemption ) {
						$tax_class = 'Zero Rate';
					}
				}
				return $tax_class;
			}

			//Radio button to choose exemption
			public function chooseExemption(){
				$radio_exemption = '<h3>Are you eligible for VAT Exemption?</h3>';
				$radio_exemption .= '<style>
																#wdve_radio_exemption label{
																	display:inline;
																}

																#wdve_vat_exemption_charity,
																#wdve_vat_exemption {
																    display: none;
																		transition:all 2s;
																}

																#wdve_vat_exemption .optional,
																.charity_fields .optional,
																#wdve_vat_exemption_charity .optional{
																	display:none;
																}

																#disability-flag_field{
																	visibility:hidden;
																}
														</style>

														<div id="wdve_radio_exemption" >
															<input onclick="wdveCheck(this.id)" type="radio" id="wdve_exempt_no" name="wdve_radio_exemption" checked><label for="wdve_exempt_no">No</label></br>
															<input onclick="wdveCheck(this.id)" type="radio" id="wdve_exempt_disability" name="wdve_radio_exemption"><label for="wdve_exempt_disability">Disability VAT Exemption</label></br>
															<input onclick="wdveCheck(this.id)" type="radio" id="wdve_exempt_charity" name="wdve_radio_exemption"><label id="wdve_exempt_charity_label" for="wdve_exempt_charity">Charity VAT Exemption</label>
														</div>';
				$radio_exemption .= '<script>
																function wdveCheck(id){
																		var today = new Date();

																		var dv_name = document.getElementById("disability_person_name");
																		var dv_address = document.getElementById("disability_person_address");
																		var dv_reason = document.getElementById("disability_reason");
																		var dv_signed = document.getElementById("disability_person_signed");

																		var cv_name = document.getElementById("charity_your_name");
																		var cv_address = document.getElementById("charity_vat_address");
																		var cv_number = document.getElementById("charity_vat_number");
																		var cv_signed = document.getElementById("charity_signed");

																	if(id == "wdve_exempt_disability"){
																			//Charity
																			if(cv_name !== null){
																				if (cv_name.value){
																					if (cv_name.value != "n/a"){
																						cv_name.focus();
																						document.execCommand("selectAll",true);
																						document.execCommand("forwardDelete",true);
																						cv_name.blur();
																					}
																				}
																			}
																			if(cv_address !== null){
																				if (cv_address.value != "n/a" && cv_address.value){
																					cv_address.focus();
																					document.execCommand("selectAll",true);
																					document.execCommand("forwardDelete",true);
																					cv_address.blur();
																				}
																			}
																			if(cv_signed !== null){
																				if (cv_signed.value != "n/a" && cv_signed.value){
																					cv_signed.focus();
																					document.execCommand("selectAll",true);
																					document.execCommand("forwardDelete",true);
																					cv_signed.blur();
																				}
																			}
																			if(cv_number !== null){
																				if (cv_number.value != "n/a" && cv_number.value){
																					cv_number.focus();
																					document.execCommand("selectAll",true);
																					document.execCommand("forwardDelete",true);
																					cv_number.blur();
																				}
																			}
																			document.getElementById("disability-date").value = today.getFullYear() + "-" + ("0" + (today.getMonth() + 1)).slice(-2) + "-" + ("0" + today.getDate()).slice(-2);
																			document.getElementById("disability-date").readOnly = true;
															        document.getElementById("wdve_vat_exemption").style.display = "block";
																		if(document.getElementById("wdve_vat_exemption_charity") != null){
															        document.getElementById("wdve_vat_exemption_charity").style.display = "none";
																		}
														        document.getElementById("disability-flag").value = "1";
																		jQuery(document.body).trigger("update_checkout");
															    }else if(id == "wdve_exempt_charity"){
																		//Disability
																		if(dv_name !== null){
																			if (dv_name.value != "n/a" && dv_name.value){
																				dv_name.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				dv_name.blur();
																			}
																		}
																		if(dv_address !== null){
																			if (cv_signed.value != "n/a" && dv_address.value){
																				dv_address.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				dv_address.blur();
																			}
																		}
																		if(dv_reason !== null){
																			if (dv_reason.value != "n/a" && dv_reason.value){
																				dv_reason.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				dv_reason.blur();
																			}
																		}
																		if(dv_signed !== null){
																			if (dv_signed.value != "n/a" && dv_signed.value){
																				dv_signed.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				dv_signed.blur();
																			}
																		}
															        document.getElementById("wdve_vat_exemption").style.display = "none";
															        document.getElementById("wdve_vat_exemption_charity").style.display = "block";
															        document.getElementById("disability-flag").value = "2";
																			document.getElementById("disability-date-charity").value = today.getFullYear() + "-" + ("0" + (today.getMonth() + 1)).slice(-2) + "-" + ("0" + today.getDate()).slice(-2);
																			document.getElementById("disability-date-charity").readOnly = true;
																			jQuery(document.body).trigger("update_checkout");
															    }else{
																		//Disability
																		if(dv_name !== null){
																			if (dv_name.value != "n/a" && dv_name.value){
																				dv_name.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				dv_name.blur();
																			}
																		}
																		if(dv_address !== null){
																			if (dv_address.value != "n/a" && dv_address.value){
																				dv_address.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				dv_address.blur();
																			}
																		}
																		if(dv_reason !== null){
																			if (dv_reason.value != "n/a" && dv_reason.value){
																				dv_reason.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				dv_reason.blur();
																			}
																		}
																		if(dv_signed !== null){
																			if (dv_signed.value != "n/a" && dv_signed.value){
																				dv_signed.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				dv_signed.blur();
																			}
																		}

																		//Charity
																		if(cv_name !== null){
																			if (cv_name.value != "n/a" && cv_name.value){
																				cv_name.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				cv_name.blur();
																			}
																		}
																		if(cv_address !== null){
																			if (cv_address.value != "n/a" && cv_address.value){
																				cv_address.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				cv_address.blur();
																			}
																		}
																		if(cv_signed !== null){
																			if (cv_signed.value != "n/a" && cv_signed.value){
																				cv_signed.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				cv_signed.blur();
																			}
																		}
																		if(cv_number !== null){
																			if (cv_number.value != "n/a" && cv_number.value){
																				cv_number.focus();
																				document.execCommand("selectAll",true);
																				document.execCommand("forwardDelete",true);
																				cv_number.blur();
																			}
																		}

																    document.getElementById("wdve_vat_exemption").style.display = "none";
																		if(document.getElementById("wdve_vat_exemption_charity") != null){
															        document.getElementById("wdve_vat_exemption_charity").style.display = "none";
																		}
															      document.getElementById("disability-flag").value = "0";
																		jQuery(document.body).trigger("update_checkout");
															    } //end of radio commands
															}
														</script>';
				if ( 'no' === $this->enable_charity_form ) {
					$radio_exemption .= '<script>
																document.getElementById("wdve_exempt_charity").style.display = "none"
																document.getElementById("wdve_exempt_charity_label").style.display = "none"
															</script>';
				}
				if ('no' === $this->show_advanced_fields){
					$radio_exemption .= '<style>
																.disability-hidden-fields{
																	display:none;
																}
																</style>';
				}else{
					$radio_exemption .= '<style>
																#wdve_vat_exemption label,
																#wdve_vat_exemption_charity label{
																	width:fit-content;
																	float:left;
																}
																#wdve_vat_exemption input,
																#wdve_vat_exemption_charity input{
																	float: left;
																	clear: none;
																	width: 200px;
																	border: none;
																	border-bottom: 1px solid #ddd;
																	font-size: 12px;
																	padding: 5px 10px 5px 10px;
																	text-align: center;
																}
																#wdve_vat_exemption .focus-visible,
																#wdve_vat_exemption input:focus,
																#wdve_vat_exemption_charity .focus-visible,
																#wdve_vat_exemption_charity input:focus{
																	outline:none!important;
																	padding: 5px 10px 5px 10px!important;
																	text-align:center;
																	border-bottom: 1px solid #ddd;

																}
																#wdve_vat_exemption #disability_reason,
																#wdve_vat_exemption #disability_person_address,
																#wdve_vat_exemption_charity #charity_vat_address{
																	width:370px;
																}
																</style>';
				}

				return $radio_exemption;
			}

			/**
			 * Render VAT exemption field after order notes field in checkout
			 * page.
			 *
			 * @access public
			 * @return void
			 */
			public function exemption_field() {
				// Check cart contents for eligible items.
				$eligible = false;
				foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

					$product = $values['data'];

					$product_id = $product->get_id();
					if ( $product->is_type( 'variation' ) ) {
						$product_id = version_compare( WC_VERSION, '3.0', '<' ) ? $product->id : $product->get_parent_id();
					}

					$disability_exemption = get_post_meta( $product_id, 'disability_exemption', true );

					if ( 'yes' === $disability_exemption ) {
						$eligible = true;
						break;
					}
				}

				if ( ! $eligible ) {
					return;
				}

				/**
				*
				*Disability VAT fields
				*
				*
				**/
				//Choose which exemption to show
				echo $this->chooseExemption();

				//Change placeholder text
				if ( 'yes' === $this->require_disability_name ) {
					$disability_placeholder = '(required)';
					$disability_placeholder_name = '(required)';
				}else{
						$disability_placeholder = '(optional)';
						$disability_placeholder_name = '(optional)';
				}

				//Show advanced Fields
				if ( 'no' == $this->show_advanced_fields){
					$disability_show_advanced_value = 'n/a';
					$disability_show_advanced_class = 'disability-hidden-fields';
					//Disability label
					$disability_name_label = 'Name of person to which VAT exemption applies';
					$disability_reason_label = 'Reason for VAT exemption';
					//Charity Label
					$charity_name_label = 'Charity name/address';
					$charity_number_label = 'Charity registration number';

				}else{
					$disability_show_advanced_value = '';
					$disability_show_advanced_class = '';
					//Disability label
					$disability_name_label = 'I (person name)';
					$disability_reason_label = 'declare that I have the following disability or chronic sickness';
					//Charity label
					$charity_name_label = 'Your name';
					$charity_number_label = 'charity VAT number';

				}

				echo '<div id="wdve_vat_exemption">';
				echo '<h3>' . __( 'VAT Exemptions', 'wc_disability_exemption' ) . '</h3>';
				echo wpautop( '<small>' . wptexturize( $this->checkout_message ) . '</small>' );

				woocommerce_form_field( 'disability_person_name', array(
					'type'        => 'text',
					'class'       => array( 'disability-fields vat-number update_totals_on_change form-row-wide' ),
					'label'       => __( $disability_name_label, 'wc_disability_exemption' ),
					'placeholder' => __( $disability_placeholder, 'wc_disability_exemption' ),
				) );

				woocommerce_form_field( 'disability_person_address', array(
					'type'        => 'text',
					'class'       => array( 'disability-fields vat-number update_totals_on_change form-row-wide', $disability_show_advanced_class ),
					'label'       => __( 'of (full address)', 'wc_disability_exemption' ),
					'placeholder' => __( $disability_placeholder_name, 'wc_disability_exemption' ),
				), $disability_show_advanced_value );

				woocommerce_form_field( 'disability_reason', array(
					'type'        => 'text',
					'class'       => array( 'disability-fields vat-number update_totals_on_change form-row-wide' ),
					'label'       => __( $disability_reason_label, 'wc_disability_exemption' ),
					'placeholder' => __( $disability_placeholder, 'wc_disability_exemption' ),
				) );

				woocommerce_form_field( 'disability_person_signed', array(
					'type'        => 'text',
					'class'       => array( 'disability-fields vat-number update_totals_on_change form-row-wide', $disability_show_advanced_class ),
					'label'       => __( 'signed', 'wc_disability_exemption' ),
					'placeholder' => __( $disability_placeholder_name, 'wc_disability_exemption' ),
				), $disability_show_advanced_value );

				woocommerce_form_field( 'disability_current_date', array(
					'type'        => 'date',
					'class'       => array( 'disability-fields vat-number update_totals_on_change form-row-wide', $disability_show_advanced_class ),
					'id'					=> 'disability-date',
					'label'       => __( 'date', 'wc_disability_exemption' ),
					'placeholder' => __( $disability_placeholder_name, 'wc_disability_exemption' ),
					'custom_attributes' => array( 'disabled' => false)
				) );

				woocommerce_form_field( 'disability_flag', array(
					'type'        => 'text',
					'label'       => __( 'disability_flag', 'wc_disability_exemption' ),
					'id' 					=> __('disability-flag', 'wc_disability_exemption'),
					'default' 					=> __(0, 'wc_disability_exemption'),
				) );

				echo '</div>';

				if ( 'yes' === $this->enable_charity_form ) {
					echo '<div id="wdve_vat_exemption_charity">';
					echo '<h3>' . __( 'Charity VAT Exemption', 'wc_disability_exemption' ) . '</h3>';
					echo wpautop( '<small>' . wptexturize( $this->checkout_charity_message ) . '</small>' );

					woocommerce_form_field( 'charity_your_name', array(
						'type'        => 'text',
						'class'       => array( 'charity_fields update_totals_on_change form-row-wide' ),
						'label'       => __( $charity_name_label, 'wc_disability_exemption' ),
						'placeholder' => __( $disability_placeholder_name, 'wc_disability_exemption' ),
					) );

					woocommerce_form_field( 'charity_vat_address', array(
						'type'        => 'text',
						'class'       => array( 'charity_fields charity-registration-number update_totals_on_change form-row-wide', $disability_show_advanced_class ),
						'label'       => __( 'charity VAT address', 'wc_disability_exemption' ),
						'placeholder' => __( $disability_placeholder_name, 'wc_disability_exemption' ),
					), $disability_show_advanced_value );

					woocommerce_form_field( 'charity_vat_number', array(
						'type'        => 'text',
						'class'       => array( 'charity_fields charity-registration-number update_totals_on_change form-row-wide' ),
						'label'       => __( $charity_number_label, 'wc_disability_exemption' ),
						'placeholder' => __( $disability_placeholder_name, 'wc_disability_exemption' ),
					) );

					woocommerce_form_field( 'charity_signed', array(
						'type'        => 'text',
						'class'       => array( 'charity_fields charity-registration-number update_totals_on_change form-row-wide', $disability_show_advanced_class ),
						'label'       => __( 'charity signed', 'wc_disability_exemption' ),
						'placeholder' => __( $disability_placeholder_name, 'wc_disability_exemption' ),
					), $disability_show_advanced_value );

					woocommerce_form_field( 'disability_current_date', array(
						'type'        => 'date',
						'class'       => array( 'disability-fields vat-number update_totals_on_change form-row-wide', $disability_show_advanced_class ),
						'id'					=> 'disability-date-charity',
						'label'       => __( 'Date', 'wc_disability_exemption' ),
						'placeholder' => __( '', 'wc_disability_exemption' ),
						'custom_attributes' => array( 'disabled' => false)
					), $disability_show_advanced_value );

					echo '</div>';
				}
			}

			/**
			 * Maybe update checkout total when order review section is updated
			 * via AJAX.
			 *
			 * @param mixed $form_data Form data.
			 * @return void
			 */
			public function ajax_update_checkout_totals( $form_data ) {
				parse_str( $form_data, $result );

				//Disability
				if ( isset($result['disability_person_name']) || isset($result['disability_reason']) || isset($result['disability_person_address']) || isset($result['disability_person_signed']) ) {
					$this->disability_person_name = wc_clean( $result['disability_person_name'] );
					$this->disability_reason = wc_clean( $result['disability_reason'] );
					$this->disability_person_address = wc_clean( $result['disability_person_address'] );
					$this->disability_person_signed = wc_clean( $result['disability_person_signed'] );
					$this->disability_current_date = wc_clean( $result['disability_current_date'] );

					if ( $this->disability_person_name || $this->disability_reason || $this->disability_person_address || $this->disability_person_signed) {
						$this->set_session( 'has_disability', 1 );
					} else {
						$this->unset_session( 'has_disability' );
					}
				}

				// Charity
				if ( isset($result['charity_your_name']) || isset($result['charity_vat_address']) || isset($result['charity_vat_number']) || isset($result['charity_signed']) ) {
					$this->charity_your_name = wc_clean( $result['charity_your_name'] );
					$this->charity_vat_address = wc_clean( $result['charity_vat_address'] );
					$this->charity_vat_number = wc_clean( $result['charity_vat_number'] );
					$this->charity_signed = wc_clean( $result['charity_signed'] );
					$this->disability_current_date = wc_clean( $result['disability_current_date'] );

					if ( $this->charity_your_name || $this->charity_vat_address || $this->charity_vat_number || $this->charity_signed) {
						$this->set_session( 'is_charity', 1 );
					} else {
						$this->unset_session( 'is_charity' );
					}
				}

				//Disability
				if ( $this->disability_person_name && isset( $result['disability_person_name'] ) ) {
					$this->disability_person_name = wc_clean( $result['disability_person_name'] );
				}

				if ( $this->disability_reason && isset( $result['disability_reason'] ) ) {
					$this->disability_reason = wc_clean( $result['disability_reason'] );
				}

				if ( $this->disability_person_address && isset( $result['disability_person_address'] ) ) {
					$this->disability_person_address = wc_clean( $result['disability_person_address'] );
				}

				if ( $this->disability_person_signed && isset( $result['disability_person_signed'] ) ) {
					$this->disability_person_signed = wc_clean( $result['disability_person_signed'] );
				}

				if ( $this->disability_current_date && isset( $result['disability_current_date'] ) ) {
					$this->disability_current_date = wc_clean( $result['disability_current_date'] );
				}


				//Charity
				if ( $this->charity_your_name && isset( $result['charity_your_name'] ) ) {
					$this->charity_your_name = wc_clean( $result['charity_your_name'] );
				}

				if ( $this->charity_vat_address && isset( $result['charity_vat_address'] ) ) {
					$this->charity_vat_address = wc_clean( $result['charity_vat_address'] );
				}

				if ( $this->charity_vat_number && isset( $result['charity_vat_number'] ) ) {
					$this->charity_vat_number = wc_clean( $result['charity_vat_number'] );
				}

				if ( $this->charity_signed && isset( $result['charity_signed'] ) ) {
					$this->charity_signed = wc_clean( $result['charity_signed'] );
				}

				//If choice back to none
				if(isset($result['disability_flag']) && $result['disability_flag'] == 0){
					$this->disability_person_name = '';
					$this->disability_reason = '';
					$this->disability_person_address = '';
					$this->disability_person_signed = '';
					$this->charity_your_name = '';
					$this->charity_vat_address = '';
					$this->charity_vat_number = '';
					$this->charity_signed = '';
					$this->disability_current_date = '';
					$this->unset_session( 'has_disability' );
					$this->unset_session( 'is_charity' );
				}

			}

			/**
			 * Checkout process.
			 *
			 * Maybe set / unset session related to disability VAT exemption.
			 *
			 * @access public
			 * @return void
			 */
			public function process_checkout() {
				// Make disability VAT Required
				if (!empty($_POST["disability_flag"])){
					if($_POST['disability_flag'] == 1){
						if ( 'yes' === $this->require_disability_name ) {
							if (!$_POST['disability_person_name']) {
								wc_add_notice( __( 'Disability person name is empty' ), 'error' );
							}
						}

						if ( 'yes' === $this->require_disability_name ) {
							if (!$_POST['disability_reason']) {
								wc_add_notice( __( 'Disability reason is empty' ), 'error' );
							}
						}

						if ( 'yes' === $this->require_disability_name ) {
							if (!$_POST['disability_person_address']) {
								wc_add_notice( __( 'Disability person address is empty' ), 'error' );
							}
						}

						if ( 'yes' === $this->require_disability_name ) {
							if (!$_POST['disability_person_signed']) {
								wc_add_notice( __( 'Disability person signed is empty' ), 'error' );
							}
						}
					}
					// Make Charity VAT Required
					if($_POST['disability_flag'] == 2){
						if ( 'yes' === $this->require_disability_name ) {
							if (!$_POST['charity_your_name']) {
								wc_add_notice( __( 'Charity person name is empty' ), 'error' );
							}
						}

						if ( 'yes' === $this->require_disability_name ) {
							if (!$_POST['charity_vat_address']) {
								wc_add_notice( __( 'Charity VAT address is empty' ), 'error' );
							}
						}

						if ( 'yes' === $this->require_disability_name ) {
							if (!$_POST['charity_vat_number']) {
								wc_add_notice( __( 'Charity VAT number is empty' ), 'error' );
							}
						}

						if ( 'yes' === $this->require_disability_name ) {
							if (!$_POST['charity_signed']) {
								wc_add_notice( __( 'Charity person signed is empty' ), 'error' );
							}
						}
					}
				}

				//Commented for bug PLUGINS-463
				//Disability
				$this->disability_reason      = isset( $_POST['disability_reason'] ) ? wc_clean( $_POST['disability_reason'] ) : '';
				$this->disability_person_name = isset( $_POST['disability_person_name'] ) ? wc_clean( $_POST['disability_person_name'] ) : '';
				$this->disability_person_address = isset( $_POST['disability_person_address'] ) ? wc_clean( $_POST['disability_person_address'] ) : '';
				$this->disability_person_signed = isset( $_POST['disability_person_signed'] ) ? wc_clean( $_POST['disability_person_signed'] ) : '';
				$this->disability_current_date = isset( $_POST['disability_current_date'] ) ? wc_clean( $_POST['disability_current_date'] ) : '';

				// if ( $this->disability_reason || $this->disability_person_name || $this->disability_person_address || $this->disability_person_signed ) {
				// 	$this->set_session( 'has_disability', 1 );
				// } else {
				// 	$this->unset_session( 'has_disability' );
				// }
				//

				//Charity
				$this->charity_your_name      = isset( $_POST['charity_your_name'] ) ? wc_clean( $_POST['charity_your_name'] ) : '';
				$this->charity_vat_address = isset( $_POST['charity_vat_address'] ) ? wc_clean( $_POST['charity_vat_address'] ) : '';
				$this->charity_vat_number = isset( $_POST['charity_vat_number'] ) ? wc_clean( $_POST['charity_vat_number'] ) : '';
				$this->charity_signed = isset( $_POST['charity_signed'] ) ? wc_clean( $_POST['charity_signed'] ) : '';
				$this->disability_current_date = isset( $_POST['disability_current_date'] ) ? wc_clean( $_POST['disability_current_date'] ) : '';

				// if (  $this->charity_your_name || $this->charity_vat_address || $this->charity_vat_number || $this->charity_signed ) {
				// 	$this->set_session( 'is_charity', 1 );
				// } else {
				// 	$this->unset_session( 'is_charity' );
				// }
			}

			/**
			 * Update order meta after checkout.
			 *
			 * @param int $order_id Order ID.
			 * @return void
			 */
			public function update_order_meta( $order_id ) {
		 	 	$current_date_now = date("Y-m-d");
				if ( $this->get_session( 'has_disability' ) == 1) {
					update_post_meta( $order_id, 'VAT Exemption person', $this->disability_person_name );
					update_post_meta( $order_id, 'VAT Exemption reason', $this->disability_reason );
					update_post_meta( $order_id, 'VAT Exemption address', $this->disability_person_address );
					update_post_meta( $order_id, 'VAT Exemption signed', $this->disability_person_signed );
					update_post_meta( $order_id, 'VAT Exemption current date', $current_date_now );

					$this->unset_session( 'has_disability' );
				}

				if ( $this->get_session( 'is_charity' ) == 1) {
	 				 $current_date_now = date("Y-m-d");
					 update_post_meta( $order_id, 'Charity person name', $this->charity_your_name );
					 update_post_meta( $order_id, 'Charity VAT address', $this->charity_vat_address );
					 update_post_meta( $order_id, 'Charity VAT number', $this->charity_vat_number );
					 update_post_meta( $order_id, 'Charity signed', $this->charity_signed );
 					update_post_meta( $order_id, 'VAT Exemption current date', $current_date_now );

					$this->unset_session( 'is_charity' );
				}

				if ($this->show_ip_address == 'yes'){
					update_post_meta( $order_id, 'Customer IP', $this->get_user_ip() );
				}
			}

			/**
			 * Order the meta related to VAT exemption in email.
			 *
			 * @param array $keys Meta keys.
			 *
			 * @return array Meta keys.
			 */
			public function order_meta_keys( $keys ) {
				$keys[] = 'Charity person name';
				$keys[] = 'Charity VAT address';
				$keys[] = 'Charity VAT number';
				$keys[] = 'Charity signed';
				$keys[] = 'VAT Exemption person';
				$keys[] = 'VAT Exemption address';
				$keys[] = 'VAT Exemption reason';
				$keys[] = 'VAT Exemption signed';
				$keys[] = 'VAT Exemption current date';
				$keys[] = 'Charity Name/Address';
				$keys[] = 'Charity Reg. No';
				$keys[] = 'Customer IP';
				return $keys;
			}

			/**
			 * Get user IP address.
			 *
			 * @return string IP address.
			 */
			public function get_user_ip() {
				return ( isset( $_SERVER['HTTP_X_FORWARD_FOR'] ) && ! empty( $_SERVER['HTTP_X_FORWARD_FOR'] ) ) ? $_SERVER['HTTP_X_FORWARD_FOR'] : $_SERVER['REMOTE_ADDR'];
			}

			/**
			 * Set session with name `$name` and value `$value`.
			 *
			 * @param string $name  Session's name.
			 * @param mixed  $value Session's value.
			 * @return void
			 */
			private function set_session( $name, $value ) {
				WC()->session->{$name} = $value;
			}

			/**
			 * Retrieve session's value of a given `$name`.
			 *
			 * @param mixed $name Session's name.
			 * @return mixed Session's value.
			 */
			private function get_session( $name ) {
				return isset( WC()->session->{$name} ) ? WC()->session->{$name} : '';
			}

			/**
			 * Unset session's of a given `$name`.
			 *
			 * @param string $name Session's name.
			 * @return void
			 */
			private function unset_session( $name ) {
				unset( WC()->session->{$name} );
			}

			/**
			 * Inline script to trigger update checkout when VAT fields are
			 * changed.
			 *
			 * @see https://github.com/woothemes/woocommerce-disability-vat-exemption/issues/2
			 */
			public function inline_script() {
				if ( ! is_checkout() ) {
					return;
				}

				// dirtyInput of shipping address fields are updated when user is logged in.
				if ( is_user_logged_in() ) {
					return;
				}

				?>
				<script>
				if ( typeof jQuery !== 'undefined' ) {
					jQuery( function() {
						jQuery( document.body ).on( 'init_checkout', bindVATFieldsChange );

						function bindVATFieldsChange() {
							jQuery( document.body ).on( 'change', '.vat-number .input-text', function() {
								jQuery( 'form.checkout' ).trigger( 'update' );
							} );
						}
					} );
				}
				</script>
				<?php
			}
		}

		/**
		 * Return instance of WC_Disability_VAT_Exemption.
		 *
		 * @since 1.3.3
		 *
		 * @return WC_Disability_VAT_Exemption
		 */
		function wc_dve() {
			static $instance;

			if ( ! isset( $instance ) ) {
				$instance = new WC_Disability_VAT_Exemption();
			}

			return $instance;
		}

		wc_dve();
	}
}
