<?php

    /**
     * WC_Gateway_Opayo_Reporting class.
     */
    class WC_Gateway_Opayo_Reporting {

		Private static $id 						 = "opayo_reporting";

	    Private static $testurl 				 = "https://test.sagepay.com/access/access.htm";
		Private static $liveurl 				 = "https://live.sagepay.com/access/access.htm";

		Private static $sagepay_payment_methods  		= "sagepayform, sagepaydirect, opayopi";
		Private static $sagepay_payment_methods_array  	= array( 'sagepayform', 'sagepaydirect', 'opayopi' );

		Private static $opayo_thirdman_action  	 = "OK, HOLD, REJECT";

		// List of txstateid used to update order status to cancelled
		Private static $cancel_txstateids 		 = array( '8', '11', '18', '22' );

		// Arrays of acceptable values
		Private static $cvvAddress_success 		 = array( 'MATCHED' );
		Private static $cvvAddress_check 		 = array( 'NOTPROVIDED','NOTCHECKED','PARTIAL' );
		Private static $cvvAddress_fail 		 = array( 'NOTMATCHED' );

		Private static $thressds_success 		 = array( 'OK','AUTHENTICATED' );
        Private static $thressds_check 			 = array( 'INCOMPLETE','NOTCHECKED','ERROR','ATTEMPTONLY','NOAUTH','CANTAUTH','MALFORMED','INVALID','NOTAVAILABLE' );                    
        Private static $thressds_fail 		 	 = array( 'NOTAUTHED' );
        

    	/**
         * __construct function.
         *
         * @access public
         * @return void
         */
        public function __construct() {

			// Add Invoice meta box to completed orders
			if( function_exists( 'simplexml_load_string' ) ) {
				add_action( 'add_meta_boxes', array( __CLASS__,'opayo_reporting_add_meta_box' ), 10, 2 );
			}

			// Settings on WooCommerce Advanced tab
			add_filter( 'woocommerce_get_sections_advanced', array( __CLASS__, 'opayo_reporting_heading' ) );
			add_filter( 'woocommerce_get_settings_advanced', array( __CLASS__, 'opayo_reporting_settings' ), 10, 2 );

			add_action( 'woocommerce_settings_save_advanced', array( __CLASS__, 'opayo_reporting_save_options' ) );

			// Action scheduler
			add_action( 'init' , array( __CLASS__,'opayo_reporting_get_transaction_report') );
			add_action( 'woocommerce_opayo_reporting_get_transaction_report', array( __CLASS__, 'action_scheduler_opayo_reporting_get_transaction_report' ), 10, 2 );


        } // END __construct

        /**
         * opayo_reporting
         *
         * Make the API request
         * 
         * @param  [type] $order [description]
         * @return [type]        [description]
         */
        private static function opayo_reporting( $order ) {

        	$settings 	= get_option( 'woocommerce_opayo_reporting_options' );
        	$output		= NULL;
        	$order_id 	= $order->get_id();
        	$url 		= WC_Gateway_Opayo_Reporting::get_url( $order );

        	$transaction_detail_xml 	= WC_Gateway_Opayo_Reporting::get_transaction_detail_xml( 'getTransactionDetail', $order );
        	$xml 						= $transaction_detail_xml['xml'];
        	$signature 					= $transaction_detail_xml['signature'];

	        $output = WC_Gateway_Opayo_Reporting::sagepay_post( $xml, $url );

	        if( $output ) {

		        $xml 	= simplexml_load_string( $output['body'], "SimpleXMLElement", LIBXML_NOCDATA );
				$json 	= json_encode($xml);
				$array 	= json_decode($json,TRUE);

				// Log the request and result
				if( isset( $settings['opayo_reporting_debugging'] ) && $settings['opayo_reporting_debugging'] == 'yes' ) {
					WC_Gateway_Opayo_Reporting::log( 'Full Request and result' );
					WC_Gateway_Opayo_Reporting::log( $url );
					
					WC_Gateway_Opayo_Reporting::log( $transaction_detail_xml['xml'] );
					WC_Gateway_Opayo_Reporting::log( $array );
				}

		        return $array;

		    }

		    return NULL;

		}

        /**
         * thirdman_reporting
         *
         * Make the API request
         * 
         * @param  [type] $order [description]
         * @return [type]        [description]
         */
        private static function thirdman_reporting( $order ) {

        	$output		= NULL;
        	$order_id 	= $order->get_id();

        	$t3maction  = WC_Gateway_Opayo_Reporting::get_thirdman_action( $order_id );

        	if( isset( $t3maction ) && in_array( $t3maction, array( 'OK', 'HOLD', 'REJECT' ) ) ) {

	        	$url 		= WC_Gateway_Opayo_Reporting::get_url( $order );

	        	$thirdman_detail_xml 	= WC_Gateway_Opayo_Reporting::get_thirdman_detail_xml( 'getT3MDetail', $order );
	        	$xml 					= $thirdman_detail_xml['xml'];
	        	$signature 				= $thirdman_detail_xml['signature'];

		        $output = WC_Gateway_Opayo_Reporting::sagepay_post( $xml, $url );

		        if( $output ) {

			        $xml 	= simplexml_load_string( $output['body'], "SimpleXMLElement", LIBXML_NOCDATA );
					$json 	= json_encode($xml);
					$array 	= json_decode($json,TRUE);

					// Log the request and result
					if( isset( $settings['opayo_reporting_debugging'] ) && $settings['opayo_reporting_debugging'] == 'yes' ) {
						WC_Gateway_Opayo_Reporting::log( 'Full ThirdMan Request and result' );
						WC_Gateway_Opayo_Reporting::log( $url );
						
						WC_Gateway_Opayo_Reporting::log( $thirdman_detail_xml['xml'] );
						WC_Gateway_Opayo_Reporting::log( $array );
					}

			        return $array;

			    }

			}

		    return NULL;

		}

		/**
		 * [validate_account description]
		 * @param  [type] $order [description]
		 * @return [type]        [description]
		 */
		private static function validate_account( $order ) {

			$payment_method = $order->get_payment_method();
			$settings 		= get_option( 'woocommerce_opayo_reporting_options' );
			$url 			= WC_Gateway_Opayo_Reporting::get_url( $order );

			$vendor_details = array( 
					'vendor' 	=> WC_Gateway_Opayo_Reporting::get_vendor_name( $payment_method, $settings ),
					'user' 		=> WC_Gateway_Opayo_Reporting::get_user_name( $payment_method, $settings ),
					'password' 	=> WC_Gateway_Opayo_Reporting::get_password( $payment_method, $settings )
				);

			$content  = '<command>version</command>';
			$content .= '<vendor>' . $vendor_details['vendor'] . '</vendor>';
			$content .= '<user>' . $vendor_details['user'] . '</user>';

			$signature = WC_Gateway_Opayo_Reporting::get_xml_signature( $content, $vendor_details );
			
			$xml  = '';
			$xml .= '<vspaccess>';
			$xml .= $content;
			$xml .= '<signature>' . md5( $signature ) . '</signature>';
			$xml .= '</vspaccess>';

			$output = WC_Gateway_Opayo_Reporting::sagepay_post( $xml, $url );

			if( $output ) {

				// Log the result
		        $response 	= simplexml_load_string( $output['body'], "SimpleXMLElement", LIBXML_NOCDATA );
				$json 		= json_encode( $response );
				$array 		= json_decode( $json, TRUE );

				if( isset( $array['errorcode'] ) && $array['errorcode'] == '0000' ) {

					if( isset( $settings['opayo_reporting_debugging'] ) && $settings['opayo_reporting_debugging'] == 'yes' ) {
						WC_Gateway_Opayo_Reporting::log( 'Account validation sucessful' );
						WC_Gateway_Opayo_Reporting::log( $url );
						WC_Gateway_Opayo_Reporting::log( $xml );
						WC_Gateway_Opayo_Reporting::log( $array );
					}

					return true;
				}

				// Log things, there has been a problem
				if( isset( $settings['opayo_reporting_debugging'] ) && $settings['opayo_reporting_debugging'] == 'yes' ) {
					WC_Gateway_Opayo_Reporting::log( 'Account validation failed' );
					WC_Gateway_Opayo_Reporting::log( $url );
					WC_Gateway_Opayo_Reporting::log( $xml );
					WC_Gateway_Opayo_Reporting::log( $array );
				}

			}

			return false;

		}

		/**
		 * [validate_api_details description]
		 * @param  [type] $payment_method [description]
		 * @param  [type] $settings       [description]
		 * @return [type]                 [description]
		 */
		private static function validate_api_details( $order ) {

			$payment_method 	= $order->get_payment_method();
			$settings 			= get_option( 'woocommerce_opayo_reporting_options' );

			$vendor_name 		= WC_Gateway_Opayo_Reporting::get_vendor_name( $payment_method, $settings );
			$user_name 			= WC_Gateway_Opayo_Reporting::get_user_name( $payment_method, $settings );
			$password 			= WC_Gateway_Opayo_Reporting::get_password( $payment_method, $settings );

			if( !isset($vendor_name) || strlen($vendor_name) == 0 ) {
				return false;
			}

			if( !isset($user_name) || strlen($user_name) == 0 ) {
				return false;
			}

			if( !isset($password) || strlen($password) == 0 ) {
				return false;
			}

			// Validate the account with Opayo
			$validate_account 	= WC_Gateway_Opayo_Reporting::validate_account( $order );

			if( !$validate_account ) {
				return false;
			}

			return true;

		}

		/**
		 * [opayo_reporting_get_transaction_report description]
		 * @return NULL
		 */
		public static function opayo_reporting_get_transaction_report() {

			$settings 		= get_option( 'woocommerce_opayo_reporting_options' );
			$schedule_time 	= isset( $settings['opayo_reporting_action_scheduler_time'] ) ? $settings['opayo_reporting_action_scheduler_time'] : 3600;

			// Update the order status if necessary 
			if( isset( $settings['opayo_reporting_action_scheduler'] ) && $settings['opayo_reporting_action_scheduler'] == 'yes' ) {

				$next = WC()->queue()->get_next( 'woocommerce_opayo_reporting_get_transaction_report' );

				if ( ! $next ) {
					WC()->queue()->cancel_all( 'woocommerce_opayo_reporting_get_transaction_report' );
					WC()->queue()->schedule_single( time()+$schedule_time, 'woocommerce_opayo_reporting_get_transaction_report' );
				}

			}
			 
		}

		/**
		 * [action_scheduler_opayo_reporting_get_transaction_report description]
		 * @param  [type] $args  [description]
		 * @param  string $group [description]
		 * @return [type]        [description]
		 */
		public static function action_scheduler_opayo_reporting_get_transaction_report( $args = NULL, $group = '' ) {
            global $wpdb;

			$valid_api_details 	= true;

			$settings 			= get_option( 'woocommerce_opayo_reporting_options' );

			// Show the meta box if Fraud checks are enabled.
			if( $valid_api_details ) {

	            $reporting_order_status_array = apply_filters( 'woocommerce_opayo_reporting_query_order_status_array', "wc-processing, wc-authorised" );

				$args =	array(
							'numberposts' => -1,
							'post_type'   => 'shop_order',
							'post_status' => $reporting_order_status_array,
							'meta_query' 	=> array(
								'relation' 	=> 'AND',
								array(
									'key'    => '_payment_method',
									'value'  => WC_Gateway_Opayo_Reporting::$sagepay_payment_methods,
									'compare'=> 'IN'
								),						
								array(
									'key'    => '_opayo_thirdman_action',
									'value'  => WC_Gateway_Opayo_Reporting::$opayo_thirdman_action,
									'compare'=> 'NOT IN'
								),
							)
						);

				WC_Gateway_Opayo_Reporting::log( $args );

	        	$results = get_posts( $args );
				
				if( isset($results) ) {

					// Process each order
	                foreach ( $results as $result ) {

	                	$counter++;

	                	$order_id = $result->ID;

	                	// Get the order
	                    $order = new WC_Order( $order_id );

	                    // Order paid with Opayo?
	                    if( in_array( $order->get_payment_method(), WC_Gateway_Opayo_Reporting::$sagepay_payment_methods ) ) {

		                    $sageresult = get_post_meta( $order_id, '_sageresult', TRUE );
							$output 	= WC_Gateway_Opayo_Reporting::opayo_reporting( $order, $sageresult );

							update_post_meta( $order_id, '_opayo_reporting_output', $output );

							// Update order with Thirdman Action
							WC_Gateway_Opayo_Reporting::update_order_thirdman_action( $order, $output );

							// Update order with Thirdman Report
							WC_Gateway_Opayo_Reporting::update_order_thirdman_report( $order, $output, $sageresult );
		                    
		                    // Update the order status if necessary 
							if( isset( $settings['opayo_reporting_update_status'] ) && $settings['opayo_reporting_update_status'] == 'yes' ) {
								WC_Gateway_Opayo_Reporting::update_order_status( $order, $output );
							}

						}
					
	                }

	                WC_Gateway_Opayo_Reporting::log( $counter . ' orders found' );

	            } else {
	            	WC_Gateway_Opayo_Reporting::log( 'No orders found' );
	            }

	        }

		}

		/**
		 * [update_order_status description]
		 * @param  [type] $order_id [description]
		 * @return [type]           [description]
		 */
		private static function update_order_status( $order, $output ) {

			// Allow filtering 
			$opayo_reporting_update_order_status_on_cancel 				= apply_filters( 'opayo_reporting_update_order_status_on_cancel', true, $order, $output );
			$opayo_reporting_update_order_status_on_t3maction_hold 		= apply_filters( 'opayo_reporting_update_order_status_on_t3maction_hold', true, $order, $output );;
			$opayo_reporting_update_order_status_on_t3maction_reject 	= apply_filters( 'opayo_reporting_update_order_status_on_t3maction_reject', true, $order, $output );;

			// Cancel orders that have been canceled in MySagePay
			if( isset( $output['txstateid'] ) && in_array( $output['txstateid'], WC_Gateway_Opayo_Reporting::$cancel_txstateids ) && $opayo_reporting_update_order_status_on_cancel ) {
				// Update the order status
                $order->update_status('cancelled');
                $order->save();
			}

			// Update orders to fraud-screen order status
			if( isset( $output['t3maction'] ) && $output['t3maction'] == 'HOLD' && $opayo_reporting_update_order_status_on_t3maction_hold ) {
				// Update the order status
                $order->update_status( 'fraud-screen', _x( 'Opayo Thirdman Action ', 'woocommerce-gateway-sagepay-form' ) . $output['t3maction'] . _x( '. Login to MySagePay and check this order before shipping.', 'woocommerce-gateway-sagepay-form' ) );
                $order->save();
			}

			// Update orders to fraud-screen order status
			if( isset( $output['t3maction'] ) && $output['t3maction'] == 'REJECT' && $opayo_reporting_update_order_status_on_t3maction_reject ) {
				// Update the order status
                $order->update_status( 'fraud-screen', _x( 'Opayo Thirdman Action ', 'woocommerce-gateway-sagepay-form' ) . $output['t3maction'] . _x( '. Login to MySagePay and check this order before shipping.', 'woocommerce-gateway-sagepay-form' ) );
                $order->save();
			}

		}

		/**
		 * [update_order_thirdman_score description]
		 * @param  [type] $order [description]
		 * @return [type]           [description]
		 */
		private static function update_order_thirdman_action( $order, $output ) {

			$order_id = $order->get_id();

			if( isset( $output['t3maction'] ) ) {
				update_post_meta( $order_id, '_opayo_thirdman_action', $output['t3maction'] );
			}

		}

		/**
		 * [update_order_thirdman_score description]
		 * @param  [type] $order [description]
		 * @return [type]           [description]
		 */
		private static function update_order_thirdman_report( $order, $output, $sageresult ) {

			$order_id = $order->get_id();

			$output   = WC_Gateway_Opayo_Reporting::third_reporting( $order, $output, $sageresult );
			update_post_meta( $order_id, '_opayo_thirdman_report', $output );
			
		}

        /**
         * sagepay_post
         *
         * Post to Sage
         * 
         * @param  [type] $data [description]
         * @param  [type] $url  [description]
         * @return [type]       [description]
         */
        private static function sagepay_post( $data, $url ) {

        	$res  = wp_remote_post( 
        				$url, array(
							'method' 		=> 'POST',
							'timeout' 		=> 45,
							'redirection' 	=> 5,
							'httpversion' 	=> '1.0',
							'blocking' 		=> true,
							'headers' 		=> array('Content-Type'=> 'application/x-www-form-urlencoded'),
							'body' 			=> "XML=".$data,
							'cookies' 		=> array()
						)
					);

			if( is_wp_error( $res ) ) {
				WC_Gateway_Opayo_Reporting::log( $res->get_error_message() );
            } else {
                return $res;
            }

            return NULL;

        }

        /**
         * log
         *
         * Log things
         * 
         * @param  [type] $to_log [description]
         * @return [type]         [description]
         */
		private static function log( $to_log ) {

			if( !isset( $logger ) ) {
                $logger      = new stdClass();
                $logger->log = new WC_Logger();
            }

            $logger->log->add( WC_Gateway_Opayo_Reporting::$id, print_r( $to_log, TRUE ) );

		}

		// Getters
		
		/**
		 * [get_transaction_id description]
		 * @param  [type] $order_id [description]
		 * @return [type]           [description]
		 */
		private static function get_transaction_id( $order_id ) {

			$sageresult = get_post_meta( $order_id, '_sageresult', TRUE );

			if( isset($sageresult) && isset($sageresult['fraudid']) ) {
				return str_replace( array('{','}'),'',$sageresult['fraudid'] );
			} elseif( isset($sageresult) && isset($sageresult['VPSTxId']) ) {
				return str_replace( array('{','}'),'',$sageresult['VPSTxId'] );
			} elseif( isset($sageresult) && isset($sageresult['transactionId']) ) {
				return str_replace( array('{','}'),'',$sageresult['transactionId'] );
			}

			return NULL;
		}

		/**
		 * [get_t3mid description]
		 * @param  [type] $order_id [description]
		 * @return [type]           [description]
		 */
		private static function get_t3mid( $order_id ) {

			$reporting_output = get_post_meta( $order_id, '_opayo_reporting_output', TRUE );

			if( isset($reporting_output) && isset($reporting_output['t3mid']) ) {
				return $reporting_output['t3mid'];
			}

			return NULL;
		}
		
        /**
         * get_transaction_detail_xml
         *
         * Create the XML to send to Sage
         * 
         * @param  [type] $command [description]
         * @param  [type] $params  [description]
         * @return [type]          [description]
         *
         * https://www.sagepay.co.uk/file/1186/download-document/reportingandapiprotocol102v0.5.pdf
         *
         */
		private static function get_transaction_detail_xml( $command, $order ) {
			$settings 		= get_option( 'woocommerce_opayo_reporting_options' );

			$order_id 		= $order->get_id();
			$payment_method = $order->get_payment_method();

			$vpstxid = WC_Gateway_Opayo_Reporting::get_transaction_id( $order_id );

			$vendor_details = array( 
					'vendor' 	=> WC_Gateway_Opayo_Reporting::get_vendor_name( $payment_method, $settings ),
					'user' 		=> WC_Gateway_Opayo_Reporting::get_user_name( $payment_method, $settings ),
					'password' 	=> WC_Gateway_Opayo_Reporting::get_password( $payment_method, $settings )
				);

			$content  = '<command>' . $command . '</command>';
			$content .= '<vendor>' . $vendor_details['vendor'] . '</vendor>';
			$content .= '<user>' . $vendor_details['user'] . '</user>';
			$content .= '<vpstxid>' . $vpstxid . '</vpstxid>';

			$signature = WC_Gateway_Opayo_Reporting::get_xml_signature( $content, $vendor_details );
			
			$xml  = '';
			$xml .= '<vspaccess>';
			$xml .= $content;
			$xml .= '<signature>' . md5( $signature ) . '</signature>';
			$xml .= '</vspaccess>';

			return array( 
					'xml' 		=> $xml, 
					'signature' => $signature 
				);

		}

        /**
         * get_thirdman_detail_xml
         *
         * Create the XML to send to Sage
         * 
         * @param  [type] $command [description]
         * @param  [type] $params  [description]
         * @return [type]          [description]
         *
         * https://www.sagepay.co.uk/file/1186/download-document/reportingandapiprotocol102v0.5.pdf
         *
         */
		private static function get_thirdman_detail_xml( $command, $order ) {

			$order_id 		= $order->get_id();
			$payment_method = $order->get_payment_method();

			$t3mid 			= WC_Gateway_Opayo_Reporting::get_t3mid( $order_id );

			if( !is_null( $t3mid ) ) {

				$settings = get_option( 'woocommerce_opayo_reporting_options' );

				$vendor_details = array( 
						'vendor' 	=> WC_Gateway_Opayo_Reporting::get_vendor_name( $payment_method, $settings ),
						'user' 		=> WC_Gateway_Opayo_Reporting::get_user_name( $payment_method, $settings ),
						'password' 	=> WC_Gateway_Opayo_Reporting::get_password( $payment_method, $settings )
					);

				$content  = '<command>' . $command . '</command>';
				$content .= '<vendor>' . $vendor_details['vendor'] . '</vendor>';
				$content .= '<user>' . $vendor_details['user'] . '</user>';
				$content .= '<t3mtxid>' . $t3mid . '</t3mtxid>';

				$signature = WC_Gateway_Opayo_Reporting::get_xml_signature( $content, $vendor_details );
				
				$xml  = '';
				$xml .= '<vspaccess>';
				$xml .= $content;
				$xml .= '<signature>' . md5( $signature ) . '</signature>';
				$xml .= '</vspaccess>';

				return array( 
						'xml' 		=> $xml, 
						'signature' => $signature 
					);
			}

			return NULL;

		}
		
        /**
         * get_xml_signature
         *
         * Build the XML signature
         * 
         * @param  [type] $command [description]
         * @param  [type] $params  [description]
         * @return [type]          [description]
         */
		private static function get_xml_signature( $content, $vendor_details ) {

			$xml  = $content;
			$xml .= '<password>' . $vendor_details['password'] . '</password>';

			return $xml;
		}

		/**
		 * [get_vendor_name description]
		 * @param  [type] $payment_method [description]
		 * @param  [type] $settings       [description]
		 * @return [type]                 [description]
		 */
		private static function get_vendor_name( $payment_method, $settings ) {

			$vendor_name = NULL;

			$payment_method_settings = get_option( 'woocommerce_' . $payment_method . '_settings' );

			$vendor_name = $payment_method_settings['vendor'];

			return $vendor_name;
		}

		/**
		 * [get_user_name description]
		 * @param  [type] $payment_method [description]
		 * @param  [type] $settings       [description]
		 * @return [type]                 [description]
		 */
		private static function get_user_name( $payment_method, $settings ) {

			$user_name = NULL;

			$payment_method_settings = get_option( 'woocommerce_' . $payment_method . '_settings' );

			if( $payment_method_settings['status'] == 'live' && isset( $settings['live_opayo_reporting_username'] ) ) {
				return $settings['live_opayo_reporting_username'];
			} elseif( isset( $settings['test_opayo_reporting_username'] ) ) {
				return $settings['test_opayo_reporting_username'];
			}

			return NULL;
		}

		/**
		 * [get_password description]
		 * @param  [type] $payment_method [description]
		 * @param  [type] $settings       [description]
		 * @return [type]                 [description]
		 */
		private static function get_password( $payment_method, $settings ) {

			$password = NULL;

			$payment_method_settings = get_option( 'woocommerce_' . $payment_method . '_settings' );

			if( $payment_method_settings['status'] == 'live' && isset( $settings['live_opayo_reporting_password'] ) ) {
				$password = $settings['live_opayo_reporting_password'];
			} elseif( isset( $settings['test_opayo_reporting_password'] ) ) {
				$password = $settings['test_opayo_reporting_password'];
			}

			return WC_Gateway_Opayo_Reporting::decrypt_value( $password );
		}

		/**
		 * [get_url description]
		 * @param  [type] $order [description]
		 * @return [type]        [description]
		 */
		private static function get_url( $order ) {

			$payment_method = $order->get_payment_method();
			$settings 		= get_option( 'woocommerce_opayo_reporting_options' );

			$payment_method_settings = get_option( 'woocommerce_' . $payment_method . '_settings' );

			if( $payment_method_settings['status'] == 'live' ) {
				return WC_Gateway_Opayo_Reporting::$liveurl;
			} else {
				return WC_Gateway_Opayo_Reporting::$testurl;
			}

		}

		/**
		 * [get_command description]
		 * @param  [type] $order_id [description]
		 * @return [type]           [description]
		 */
		private static function get_command( $order_id ) {

			$command = 'getTransactionDetail';

			return $command;
		}

		/**
		 * [get_encryption_key description]
		 * @return [type] [description]
		 */
		private static function get_encryption_key() {

			$key = NONCE_SALT;

			if( !isset( $key ) || strlen( $key ) > 1 ) {
				$key = 'hax|y3_P-Y[Ybj~%jN_7JDro_!yaS #D23hax|y3_P-Y[Ybj~%jN_7JDro_!yaS #D23';
			}

			return $key;
		}

		/**
		 * [encrypt_value description]
		 * @param  [type] $value [description]
		 * @return [type]        [description]
		 */
		private static function encrypt_value( $value ) {
			$key = WC_Gateway_Opayo_Reporting::get_encryption_key();
			return openssl_encrypt( $value, 'AES-128-CBC', $key, NULL, substr($key, -16) );
		}

		/**
		 * [decrypt_value description]
		 * @param  [type] $value [description]
		 * @return [type]        [description]
		 */
		private static function decrypt_value( $value ) {
			$key = WC_Gateway_Opayo_Reporting::get_encryption_key();
			return openssl_decrypt( $value, 'AES-128-CBC', $key, NULL, substr($key, -16) );
		}

		/**
		 * [get_thirdman_action description]
		 * @param  [type] $order_id [description]
		 * @return [type]           [description]
		 */
		private static function get_thirdman_action( $order_id ) {

			$t3maction 	= NULL;

			$t3maction 	= get_post_meta( $order_id, '_opayo_thirdman_action', TRUE );

			if( empty( $t3action ) || is_null( $tmaction ) ) {
				$output 	= get_post_meta( $order_id, '_opayo_reporting_output', TRUE );
				if( isset( $output['t3action'] ) ) {
					$t3maction 	= $output['t3action'];
				}
			}

			return $t3maction ;
		}

		/**
		 * [get_thirdman_output description]
		 * @param  [type] $order_id [description]
		 * @return [type]           [description]
		 */
		private static function get_thirdman_output ( $order_id ) {

			$thirdman 	= get_post_meta( $order_id, '_opayo_thirdman_output', TRUE );

			if( empty( $thirdman ) || is_null( $thirdman ) ) {

				$order    = new WC_Order( $order_id );
				$thirdman = WC_Gateway_Opayo_Reporting::thirdman_reporting( $order );
				update_post_meta( $order_id, '_opayo_thirdman_output', $thirdman );

				return $thirdman;
			}

			if( isset( $thirdman['errorcode'] ) && $thirdman['errorcode'] == '0000' ) {

				return $thirdman;

			} else {

				$order    = new WC_Order( $order_id );
				$thirdman = WC_Gateway_Opayo_Reporting::thirdman_reporting( $order );
				update_post_meta( $order_id, '_opayo_thirdman_output', $thirdman );

				return $thirdman;

			}

		}

		// Meta Boxes

        /**
         * opayo_reporting_add_meta_box
         *
         * Add a meta box to the edit order screen
         * 
         * @param  [type] $post_type [description]
         * @param  [type] $post      [description]
         * @return [type]            [description]
         */
		public static function opayo_reporting_add_meta_box( $post_type, $post ) {

			if( $post_type == 'shop_order' ) {

				$valid_api_details 	= false;
				$order_id 			= $post->ID;
				$order 				= new WC_Order( $order_id );

				$payment_method 	= $order->get_payment_method();
				$settings 			= get_option( 'woocommerce_opayo_reporting_options' );

				$opayo_thirdman_action = get_post_meta( $order_id, '_opayo_thirdman_action', TRUE );

				// Only get the report if we need to
				if ( isset( $opayo_thirdman_action ) && in_array( $opayo_thirdman_action, array( 'OK', 'HOLD', 'REJECT' ) ) ) {

					add_meta_box( 'opayo-reporting-details', __('Opayo Reporting', 'woocommerce-gateway-sagepay-form'), array( __CLASS__,'opayo_reporting_details_meta_box' ), 'shop_order', 'side', 'high');

					if( isset( $settings['opayo_reporting_debugging'] ) && $settings['opayo_reporting_debugging'] == 'yes' ) {
						add_meta_box( 'opayo-reporting-details-full-result', __('Opayo Reporting (Full Result)', 'woocommerce-gateway-sagepay-form'), array( __CLASS__,'opayo_reporting_details_full_meta_box' ), 'shop_order', 'advanced', 'low');
					}

				} else { 

					$valid_api_details 	= WC_Gateway_Opayo_Reporting::validate_api_details( $order );

					// Show the meta box if Fraud checks are enabled.
					if( $valid_api_details && in_array( $order->get_payment_method(), WC_Gateway_Opayo_Reporting::$sagepay_payment_methods_array ) ) {

						$sageresult = get_post_meta( $order_id, '_sageresult', TRUE );
						$output = WC_Gateway_Opayo_Reporting::opayo_reporting( $order, $sageresult );

						// Update order with full report
						update_post_meta( $order_id, '_opayo_reporting_output', $output );

						// Update order with Thirdman Action
						WC_Gateway_Opayo_Reporting::update_order_thirdman_action( $order, $output );

						add_meta_box( 'opayo-reporting-details', __('Opayo Reporting', 'woocommerce-gateway-sagepay-form'), array( __CLASS__,'opayo_reporting_details_meta_box' ), 'shop_order', 'side', 'high');

						if( isset( $settings['opayo_reporting_debugging'] ) && $settings['opayo_reporting_debugging'] == 'yes' ) {
							add_meta_box( 'opayo-reporting-details-full-result', __('Opayo Reporting (Full Result)', 'woocommerce-gateway-sagepay-form'), array( __CLASS__,'opayo_reporting_details_full_meta_box' ), 'shop_order', 'advanced', 'low');
						}

					}

				}

			}

		}

        /**
         * opayo_reporting_details_meta_box
         *
         * Add the thirdman result to the meta box.
         * 
         * @param  [type] $post [description]
         * @return [type]       [description]
         */
		public static function opayo_reporting_details_meta_box( $post ) {
			global $woocommerce;

			$settings 	= get_option( 'woocommerce_opayo_reporting_options' );

			$order_id 	= $post->ID;

			$order  	= new WC_Order( $order_id );
			$sageresult = get_post_meta( $order_id, '_sageresult', TRUE );
			$note 		= '';

			$output 	= get_post_meta( $order_id, '_opayo_reporting_output', TRUE );
			$thirdman 	= WC_Gateway_Opayo_Reporting::get_thirdman_output ( $order_id );

			// Update the order status if necessary
			if( isset( $settings['opayo_reporting_update_status'] ) && $settings['opayo_reporting_update_status'] == 'yes' ) {
				WC_Gateway_Opayo_Reporting::update_order_status( $order, $output );
			}

			?>
			<div class="opayo_reporting_group">
				<table class="opayo_reporting_list">
				<?php
				if( isset( $output['errorcode'] ) && $output['errorcode'] == '0000' ) {

					echo WC_Gateway_Opayo_Reporting::get_metabox_status( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_t3mscore( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_t3maction( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_t3mid( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_fraudcode( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_fraudcodedetail( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_fraudscreenrecommendation( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_cv2result( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_addressresult( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_postcoderesult( $output );
					echo WC_Gateway_Opayo_Reporting::get_metabox_threedresult( $output );	

				} elseif( isset( $output['errorcode'] ) ) { ?>
					<tr class="left">
						<th><?php echo __('Error Code :', 'woocommerce-gateway-sagepay-form') . '</th><td>' . $output['errorcode']; ?></td>
					</tr>
					<tr class="left">
						<th><?php echo __('Error Description :', 'woocommerce-gateway-sagepay-form') . '</th><td>' . htmlentities( $output['error'] ); ?></td>
					</tr>
				<?php } ?>
				</table>
				<table class="opayo_reporting_thirdman">
				<?php
				if( isset( $thirdman['errorcode'] ) && $thirdman['errorcode'] == '0000' ) {	

					echo WC_Gateway_Opayo_Reporting::get_metabox_threedrules( $thirdman );

				} elseif( isset( $thirdman['errorcode'] ) ) { ?>
					<tr class="left">
						<th><?php echo __('Error Code :', 'woocommerce-gateway-sagepay-form') . '</th><td>' . $thirdman['errorcode']; ?></td>
					</tr>
					<tr class="left">
						<th><?php echo __('Error Description :', 'woocommerce-gateway-sagepay-form') . '</th><td>' . htmlentities( $thirdman['error'] ); ?></td>
					</tr>
				<?php } ?>
				</table>
				<div class="clear"></div>
			</div><?php
			
		}

        /**
         * opayo_reporting_details_meta_box
         *
         * Add the thirdman result to the meta box.
         * 
         * @param  [type] $post [description]
         * @return [type]       [description]
         */
		public static function opayo_reporting_details_full_meta_box( $post ) {
			global $woocommerce;

			$order_id = $post->ID;

			$order  	= new WC_Order( $order_id );
			$sageresult = get_post_meta( $order_id, '_sageresult', TRUE );
			$note 		= '';

			$output 	= get_post_meta( $order_id, '_opayo_reporting_output', TRUE );
			$thirdman 	= get_post_meta( $order_id, '_opayo_thirdman_output', TRUE );

			if( is_array( $output ) ) {
				array_walk_recursive($output, function (&$val) {
					$val = htmlentities($val, ENT_QUOTES);
				});
			}

			if( is_array( $thirdman ) ) {
				array_walk_recursive($thirdman, function (&$val) {
					$val = htmlentities($val, ENT_QUOTES);
				});
			}

			?>
			<div class="sagepay_group">
				<ul class="totals">
				<?php
				echo '<pre>' . print_r( $output, TRUE ) . '</pre>';
				echo '<pre>' . print_r( $thirdman, TRUE ) . '</pre>';
				?>
				</ul>
				<div class="clear"></div>
			</div><?php
			
		}

		// Metabox Getters
		/**
		 * [get_metabox_status description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_status( $output ) {
			if( isset( $output['status'] ) ){
				$return = '<tr class="left opayo_reporting_item">
							<th class="opayo_reporting_title">' . __('Status :', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['status'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_t3mscore description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_t3mscore( $output ) {
			if( isset( $output['t3mscore'] ) ){

				$opayo_reporting_item_flag = '';
				if( intval( $output['t3mscore'] ) <= 0 ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_success';
				} else {
					$opayo_reporting_item_flag = ' opayo_reporting_item_check';
				}

				$return = '<tr class="left opayo_reporting_item' . $opayo_reporting_item_flag . '">
							<th class="opayo_reporting_title">' . __('3rd Man Score:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['t3mscore'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_t3maction description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_t3maction( $output ) {
			if( isset( $output['t3maction'] ) ){

				$opayo_reporting_item_flag = '';
				if( strtoupper( $output['t3maction'] ) == 'OK' ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_success';
				}

				if( strtoupper( $output['t3maction'] ) == 'HOLD' ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_check';
				}

				if( strtoupper( $output['t3maction'] ) == 'REJECT' ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_fail';
				}

				$return = '<tr class="left opayo_reporting_item' . $opayo_reporting_item_flag . '">
							<th class="opayo_reporting_title">' . __('3rd Man Recommendation:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['t3maction'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_t3mid description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_t3mid( $output ) {
			if( isset( $output['t3mid'] ) ){
				$return = '<tr class="left opayo_reporting_item">
							<th class="opayo_reporting_title">' . __('ThirdMan ID:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['t3mid'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_fraudcode description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_fraudcode( $output ) {
			if( isset( $output['fraudcode'] ) ){
				$return = '<tr class="left opayo_reporting_item">
							<th class="opayo_reporting_title">' . __('Fraud Code:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['fraudcode'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_fraudcodedetail description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_fraudcodedetail( $output ) {
			if( isset( $output['fraudcodedetail'] ) ){
				$return = '<tr class="left opayo_reporting_item">
							<th class="opayo_reporting_title">' . __('Code Detail:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['fraudcodedetail'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_fraudscreenrecommendation description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_fraudscreenrecommendation( $output ) {
			if( isset( $output['fraudscreenrecommendation'] ) ){
				$return = '<tr class="left opayo_reporting_item">
							<th class="opayo_reporting_title">' . __('Fraud Recommendation:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['fraudscreenrecommendation'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_cv2result description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_cv2result( $output ) {
			if( isset( $output['cv2result'] ) ){

				$opayo_reporting_item_flag = '';
				if( in_array( $output['cv2result'], WC_Gateway_Opayo_Reporting::$cvvAddress_success ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_success';
				}

				if( in_array( $output['cv2result'], WC_Gateway_Opayo_Reporting::$cvvAddress_check ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_check';
				}

				if( in_array( $output['cv2result'], WC_Gateway_Opayo_Reporting::$cvvAddress_fail ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_fail';
				}

				$return = '<tr class="left opayo_reporting_item' . $opayo_reporting_item_flag . '">
							<th class="opayo_reporting_title">' . __('CV2 Check:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['cv2result'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_addressresult description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_addressresult( $output ) {
			if( isset( $output['addressresult'] ) ){

				$opayo_reporting_item_flag = '';
				if( in_array( $output['addressresult'], WC_Gateway_Opayo_Reporting::$cvvAddress_success ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_success';
				}

				if( in_array( $output['addressresult'], WC_Gateway_Opayo_Reporting::$cvvAddress_check ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_check';
				}

				if( in_array( $output['addressresult'], WC_Gateway_Opayo_Reporting::$cvvAddress_fail ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_fail';
				}

				$return = '<tr class="left opayo_reporting_item' . $opayo_reporting_item_flag . '">
							<th class="opayo_reporting_title">' . __('Address Check:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['addressresult'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_postcoderesult description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_postcoderesult( $output ) {
			if( isset( $output['postcoderesult'] ) ){

				$opayo_reporting_item_flag = '';
				if( in_array( $output['postcoderesult'], WC_Gateway_Opayo_Reporting::$cvvAddress_success ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_success';
				}

				if( in_array( $output['postcoderesult'], WC_Gateway_Opayo_Reporting::$cvvAddress_check ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_check';
				}

				if( in_array( $output['postcoderesult'], WC_Gateway_Opayo_Reporting::$cvvAddress_fail ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_fail';
				}

				$return = '<tr class="left opayo_reporting_item' . $opayo_reporting_item_flag . '">
							<th class="opayo_reporting_title">' . __('Postcode Check:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['postcoderesult'] . '</td>
						   </tr>';

				return $return;
			}

			return NULL;
		}

		/**
		 * [get_metabox_threedresult description]
		 * @param  [type] $output [description]
		 * @return [type]         [description]
		 */
		private static function get_metabox_threedresult( $output ) {
			if( isset( $output['threedresult'] ) ){

				$opayo_reporting_item_flag = '';
				if( in_array( $output['threedresult'], WC_Gateway_Opayo_Reporting::$thressds_success ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_success';
				}

				if( in_array( $output['threedresult'], WC_Gateway_Opayo_Reporting::$thressds_check ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_check';
				}

				if( in_array( $output['threedresult'], WC_Gateway_Opayo_Reporting::$thressds_fail ) ) {
					$opayo_reporting_item_flag = ' opayo_reporting_item_fail';
				}

				$return = '<tr class="left opayo_reporting_item' . $opayo_reporting_item_flag . '">
							<th class="opayo_reporting_title">' . __('3D Secure:', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">' . $output['threedresult'] . '</td>
						   </tr>';

				return $return;
			}
			
			return NULL;
		}					

		/**
		 * [get_metabox_threedrules description]
		 * @param  [type] $thirdman [description]
		 * @return [type]           [description]
		 */
		private static function get_metabox_threedrules( $thirdman ) {
			if( isset( $thirdman['t3mresults'] ) ){

				// Add script to toggle 
				WC_Gateway_Opayo_Reporting::opayo_reporting_script();

				$return = '<tr class="left opayo_reporting_item">
							<th class="opayo_reporting_title">' . __('ThirdMan Results', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value"><a href="" class="opayo_reporting_toggle">' . __('Show/Hide', 'woocommerce-gateway-sagepay-form') . '</a></td>
						   </tr>
						   <tr class="left opayo_reporting_item opayo_reporting_show_hide">
							<th class="opayo_reporting_title">' . __('Rule', 'woocommerce-gateway-sagepay-form') . '</th>
							<td class="opayo_reporting_value">Score</td>
						   </tr>';

				foreach( $thirdman['t3mresults']['rule'] AS $t3mresult ) {
					$return .= '<tr class="left opayo_reporting_item opayo_reporting_show_hide">
								 <th class="opayo_reporting_title">' . $t3mresult['description'] . ':</th>
								 <td class="opayo_reporting_value">' . $t3mresult['score'] . '</td>
					   			</tr>';
					
				}

				return $return;
			}

			return NULL;
		}

		// SCRIPTS
		
		/**
		 * [opayo_reporting_script description]
		 * @return [type] [description]
		 */
		private static function opayo_reporting_script() {
			wp_enqueue_script(
				'opayo-reporting',
				SAGEPLUGINURL.'assets/js/opayo-reporting.js',
				array( 'jquery' ),
				OPAYOPLUGINVERSION
			);
		}

		// SETTINGS
		
	    /**
         * [opayo_reporting_heading description]
         * @param  [type] $sections [description]
         * @return [type]           [description]
         */
        public static function opayo_reporting_heading( $sections ) {
        	$current_user 		= wp_get_current_user();
	        $allowed_user_role 	= apply_filters( 'opayo_allowed_user_role_opayo_reporting_options', 'administrator', $sections );

			// Only admins can do this!
			if( in_array( $allowed_user_role, $current_user->roles ) ) {        	
        		$sections['woocommerce_opayo_reporting_options'] = __( 'Opayo Reporting Options', 'woocommerce-gateway-sagepay-form' );
        	}

        	return $sections;

        }

        /**
         * [opayo_reporting_settings description]
         * @param  [type] $settings        [description]
         * @param  [type] $current_section [description]
         * @return [type]                  [description]
         */
        public static function opayo_reporting_settings( $settings, $current_section ) {
        	$current_user 		= wp_get_current_user();
	        $allowed_user_role 	= apply_filters( 'opayo_allowed_user_role_opayo_reporting_options', 'administrator', $current_section );

        	if ( 'woocommerce_opayo_reporting_options' === $current_section && in_array( $allowed_user_role, $current_user->roles ) ) {

        		$reporting_settings = get_option( 'woocommerce_opayo_reporting_options' );

        		$settings = 
					array(
						array(
							'title' 		=> __( 'Opayo Reporting Options', 'woocommerce-gateway-sagepay-form' ),
							'type'  		=> 'title',
							'desc'  		=> 'It is recommended that you create a new MySagePay user for this section to avoid password issues.',
							'id'    		=> 'opayo_reporting_options',
						),
						array(
							'title'   		=> __( 'Testing Username', 'woocommerce-gateway-sagepay-form' ),
							'desc'    		=> __( 'Testing MySagePay Username', 'woocommerce-gateway-sagepay-form' ),
							'id'      		=> 'test_opayo_reporting_username',
							'type'    		=> 'text',
							'default' 		=> '',
							'placeholder' 	=> __( 'Test MySagePay Username', 'woocommerce-gateway-sagepay-form' ),
							'value' 		=> isset( $reporting_settings['test_opayo_reporting_username'] ) ? $reporting_settings['test_opayo_reporting_username'] : NULL
						),
						array(
							'title'   		=> __( 'Testing Password', 'woocommerce-gateway-sagepay-form' ),
							'desc'    		=> __( 'Testing MySagePay Password', 'woocommerce-gateway-sagepay-form' ),
							'id'      		=> 'test_opayo_reporting_password',
							'type'    		=> 'password',
							'default' 		=> '',
							'placeholder' 	=> __( 'Test MySagePay Password', 'woocommerce-gateway-sagepay-form' ),
							'value' 		=> isset( $reporting_settings['test_opayo_reporting_password'] ) ? WC_Gateway_Opayo_Reporting::decrypt_value( $reporting_settings['test_opayo_reporting_password'] ) : NULL
						),
						array(
							'title'   		=> __( 'Live Username', 'woocommerce-gateway-sagepay-form' ),
							'desc'    		=> __( 'Live MySagePay Username', 'woocommerce-gateway-sagepay-form' ),
							'id'      		=> 'live_opayo_reporting_username',
							'type'    		=> 'text',
							'default' 		=> '',
							'placeholder' 	=> __( 'Live MySagePay Username', 'woocommerce-gateway-sagepay-form' ),
							'value' 		=> isset( $reporting_settings['live_opayo_reporting_username'] ) ? $reporting_settings['live_opayo_reporting_username'] : NULL
						),
						array(
							'title'   		=> __( 'Live Password', 'woocommerce-gateway-sagepay-form' ),
							'desc'    		=> __( 'Live MySagePay Password', 'woocommerce-gateway-sagepay-form' ),
							'id'      		=> 'live_opayo_reporting_password',
							'type'    		=> 'password',
							'default' 		=> '',
							'placeholder' 	=> __( 'Live MySagePay Password', 'woocommerce-gateway-sagepay-form' ),
							'value' 		=> isset( $reporting_settings['live_opayo_reporting_password'] ) ? WC_Gateway_Opayo_Reporting::decrypt_value( $reporting_settings['live_opayo_reporting_password'] ) : NULL
						),
						array(
							'title'   		=> __( 'Update Order Status', 'woocommerce-gateway-sagepay-form' ),
							'desc'    		=> __( 'Update the order status in WooCommerce if the status changes in MySagePay', 'woocommerce-gateway-sagepay-form' ),
							'id'      		=> 'opayo_reporting_update_status',
							'type'          => 'select',
				    		'options'       => array('yes'=>'Yes','no'=>'No'),
							'default' 		=> 'no',
							'value' 		=> isset( $reporting_settings['opayo_reporting_update_status'] ) ? $reporting_settings['opayo_reporting_update_status'] : 'no'
						),
						array(
							'title'   		=> __( 'Automate Reporting?', 'woocommerce-gateway-sagepay-form' ),
							'desc'    		=> __( 'Use an automated process to get Opayo transaction report', 'woocommerce-gateway-sagepay-form' ),
							'id'      		=> 'opayo_reporting_action_scheduler',
							'type'          => 'select',
				    		'options'       => array('yes'=>'Yes','no'=>'No'),
							'default' 		=> 'no',
							'value' 		=> isset( $reporting_settings['opayo_reporting_action_scheduler'] ) ? $reporting_settings['opayo_reporting_action_scheduler'] : 'no'
						),
						array(
							'title'   		=> __( 'Automate Reporting Schedule', 'woocommerce-gateway-sagepay-form' ),
							'desc'    		=> __( 'If the Automate Reporting option is "YES" how often should the reports run?<br />Time is shown in seconds, default is hourly. Reduce this value for busier sites.', 'woocommerce-gateway-sagepay-form' ),
							'id'      		=> 'opayo_reporting_action_scheduler_time',
							'type'          => 'text',
							'default' 		=> '3600',
							'value' 		=> isset( $reporting_settings['opayo_reporting_action_scheduler_time'] ) ? $reporting_settings['opayo_reporting_action_scheduler_time'] : '3600'
						),
						array(
							'title'   		=> __( 'Debugging', 'woocommerce-gateway-sagepay-form' ),
							'desc'    		=> __( 'Show full report from Opayo when editing order', 'woocommerce-gateway-sagepay-form' ),
							'id'      		=> 'opayo_reporting_debugging',
							'type'          => 'select',
				    		'options'       => array('yes'=>'Yes','no'=>'No'),
							'default' 		=> 'no',
							'value' 		=> isset( $reporting_settings['opayo_reporting_debugging'] ) ? $reporting_settings['opayo_reporting_debugging'] : 'no'
						),
						array(
							'type' 			=> 'sectionend',
							'id'   			=> 'opayo_reporting_options',
						),
						array(
							'title' 		=> '',
							'type'  		=> 'title',
							'desc'  		=> 'For more details on setting up the Opayo Reporting options please review the docs <a href="https://docs.woocommerce.com/document/opayo-reporting/" target="_blank">here</a>',
							'id'    		=> 'opayo_reporting_options',
						),
					);
			}

        	return $settings;

        }

        /**
         * [opayo_reporting_save_options description]
         * @return [type] [description]
         */
        public static function opayo_reporting_save_options() {

        	$section 			= isset( $_GET['section'] ) ? wc_clean( $_GET['section'] ) : NULL;
        	$current_user 		= wp_get_current_user();
	        $allowed_user_role 	= apply_filters( 'opayo_allowed_user_role_opayo_reporting_options', 'administrator', $section );

			// Only admins can do this!
        	if ( $section === 'woocommerce_opayo_reporting_options' && in_array( $allowed_user_role, $current_user->roles ) ) {
        	
        		$test_opayo_reporting_password 	= isset( $_POST['test_opayo_reporting_password'] ) ? wc_clean( $_POST['test_opayo_reporting_password'] ) : NULL;
        		$live_opayo_reporting_password 	= isset( $_POST['live_opayo_reporting_password'] ) ? wc_clean( $_POST['live_opayo_reporting_password'] ) : NULL;

        		$opayo_reporting = array( 
	        			'test_opayo_reporting_username' 		=> isset( $_POST['test_opayo_reporting_username'] ) ? wc_clean( $_POST['test_opayo_reporting_username'] ) : NULL,
		        		'test_opayo_reporting_password' 		=> WC_Gateway_Opayo_Reporting::encrypt_value( $test_opayo_reporting_password ),
		        		'live_opayo_reporting_username' 		=> isset( $_POST['live_opayo_reporting_username'] ) ? wc_clean( $_POST['live_opayo_reporting_username'] ) : NULL,
		        		'live_opayo_reporting_password' 		=> WC_Gateway_Opayo_Reporting::encrypt_value( $live_opayo_reporting_password ),
		        		'opayo_reporting_update_status' 		=> isset( $_POST['opayo_reporting_update_status'] ) ? wc_clean( $_POST['opayo_reporting_update_status'] ) : NULL,
		        		'opayo_reporting_action_scheduler'		=> isset( $_POST['opayo_reporting_action_scheduler'] ) ? wc_clean( $_POST['opayo_reporting_action_scheduler'] ) : NULL,
		        		'opayo_reporting_action_scheduler_time'	=> isset( $_POST['opayo_reporting_action_scheduler_time'] ) ? wc_clean( $_POST['opayo_reporting_action_scheduler_time'] ) : NULL,
		        		'opayo_reporting_debugging' 			=> isset( $_POST['opayo_reporting_debugging'] ) ? wc_clean( $_POST['opayo_reporting_debugging'] ) : NULL,

        		);

        		update_option( 'woocommerce_opayo_reporting_options', $opayo_reporting );

        	}

    	    // Remove options set by WooCommerce
    		delete_option( 'test_opayo_reporting_username' );
	        delete_option( 'test_opayo_reporting_password' );
	        delete_option( 'live_opayo_reporting_username' );
	        delete_option( 'live_opayo_reporting_password' );
	        delete_option( 'opayo_reporting_update_status' );
	        delete_option( 'opayo_reporting_action_scheduler' );
	        delete_option( 'opayo_reporting_action_scheduler_time' );
	        delete_option( 'opayo_reporting_debugging' );

        }

	} // End class
	
	$WC_Gateway_Opayo_Reporting = new WC_Gateway_Opayo_Reporting;
