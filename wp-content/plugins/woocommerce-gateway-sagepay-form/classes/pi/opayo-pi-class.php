<?php

    /**
     * WC_Gateway_Opayo_Pi class.
     *
     * @extends WC_Payment_Gateway
     */
    class WC_Gateway_Opayo_Pi extends WC_Payment_Gateway {
        /**
         * [$sage_cardtypes description]
         * Set up accepted card types for card type drop down
         * From Version 3.3.0
         * @var array
         */
        var $sage_cardtypes = array(
                                'MasterCard'        => 'MasterCard',
                                'MasterCard Debit'  => 'MasterCard Debit',
                                'Visa'              => 'Visa',
                                'Visa Debit'        => 'Visa Debit',
                                'Discover'          => 'Discover',
                                'American Express'  => 'American Express',
                                'Maestro'           => 'Maestro',
                                'JCB'               => 'JCB',
                                'Laser'             => 'Laser'
                            );

        /**
         * __construct function.
         *
         * @access public
         * @return void
         */
        public function __construct() {

            // Sandbox basic profile (AVS, CV2 and 3-D Secure checks are turned off)
            // vendorName: sandbox
            // integrationKey: hJYxsw7HLbj40cB8udES8CDRFLhuJ8G54O6rDpUXvE6hYDrria
            // integrationPassword: o2iHSrFybYMZpmWOQMuhsXP52V4fBtpuSDshrKDSWsBY1OiN6hwd9Kb12z4j5Us5u
            
            // Sandbox extra checks profile (Strict AVS, CV2 and 3-D Secure checks applied)
            // vendorName: sandboxEC
            // integrationKey: dq9w6WkkdD2y8k3t4olqu8H6a0vtt3IY7VEsGhAtacbCZ2b5Ud
            // integrationPassword: hno3JTEwDHy7hJckU4WuxfeTrjD0N92pIaituQBw5Mtj7RG3V8zOdHCSPKwJ02wAV

            $this->id                                = 'opayopi';
            $this->method_title                      = __( 'Opayo Pi', 'woocommerce-gateway-sagepay-form' );
            $this->method_description                = $this->sagepay_system_status();
            $this->icon                              = apply_filters( 'wc_sagepaypi_icon', '' );
            $this->has_fields                        = false;

            $this->default_title                     = __( 'Credit Card via Opayo', 'woocommerce-gateway-sagepay-form' );
            $this->default_description               = __( 'Pay via Credit / Debit Card with Opayo secure card processing.', 'woocommerce-gateway-sagepay-form' );
            $this->default_order_button_text         = __( 'Pay securely with Opayo', 'woocommerce-gateway-sagepay-form' );
            $this->default_status                    = 'testing';
            $this->default_VendorName                = 'sandbox';
            $this->default_Integration_Key           = '';
            $this->default_Integration_Password      = '';
            $this->default_Test_Integration_Key      = 'hJYxsw7HLbj40cB8udES8CDRFLhuJ8G54O6rDpUXvE6hYDrria';
            $this->default_Test_Integration_Password = 'o2iHSrFybYMZpmWOQMuhsXP52V4fBtpuSDshrKDSWsBY1OiN6hwd9Kb12z4j5Us5u';
            $this->default_txtype                    = 'PAYMENT';
            $this->default_cardtypes                 = $this->sage_cardtypes;
            $this->default_tokens                    = 'no';
            $this->default_tokens_message            = '';
            $this->default_debug                     = 'no';
            $this->default_notification              = get_bloginfo('admin_email');
            $this->default_vendortxcodeprefix        = 'wc_';
            $this->default_checkout_form             = 'woocommerce';
            $this->default_threeds                   = '2.0';
            $this->default_pimagicvalue              = 'NO';
            
            // Load the form fields
            $this->init_form_fields();

            // Load the settings.
            $this->init_settings();

            $this->enabled                           = $this->settings['enabled'];
            $this->title                             = isset( $this->settings['title'] ) ? $this->settings['title'] : $this->default_title;
            $this->description                       = isset( $this->settings['description'] ) ? $this->settings['description'] : $this->default_description;
            $this->order_button_text                 = isset( $this->settings['order_button_text'] ) ? $this->settings['order_button_text'] : $this->default_order_button_text;
            $this->status                            = isset( $this->settings['status'] ) ? $this->settings['status'] : $this->default_status;
            $this->vendor                            = isset( $this->settings['vendor'] ) ? $this->settings['vendor'] : $this->default_VendorName;
            $this->Live_Integration_Key              = isset( $this->settings['Integration_Key'] ) ? $this->settings['Integration_Key'] : $this->default_Integration_Key;
            $this->Live_Integration_Password         = isset( $this->settings['Integration_Password'] ) ? $this->settings['Integration_Password'] : $this->default_Integration_Password;
            $this->Test_Integration_Key              = isset( $this->settings['Test_Integration_Key'] ) ? $this->settings['Test_Integration_Key'] : $this->default_Test_Integration_Key;
            $this->Test_Integration_Password         = isset( $this->settings['Test_Integration_Password'] ) ? $this->settings['Test_Integration_Password'] : $this->default_Test_Integration_Password;
            $this->txtype                            = isset( $this->settings['txtype'] ) ? $this->settings['txtype'] : $this->default_txtype;
            $this->cardtypes                         = isset( $this->settings['cardtypes'] ) ? $this->settings['cardtypes'] : $this->default_cardtypes;
            $this->tokens                            = isset( $this->settings['tokens'] ) && $this->settings['tokens'] == 'yes' ? true : false;
            $this->tokens_message                    = '';
            $this->debug                             = isset( $this->settings['debug'] ) && $this->settings['debug'] == 'yes' ? true : false;
            $this->notification                      = isset( $this->settings['notification'] ) ? $this->settings['notification'] : $this->default_notification;
            $this->vendortxcodeprefix                = isset( $this->settings['vendortxcodeprefix'] ) ? $this->settings['vendortxcodeprefix'] : $this->default_vendortxcodeprefix;
            $this->checkout_form                     = isset( $this->settings['checkout_form'] ) ? $this->settings['checkout_form'] : $this->default_checkout_form;
            $this->threeds                           = isset( $this->settings['threeds'] ) ? $this->settings['threeds'] : $this->default_threeds;
            $this->pimagicvalue                      = isset( $this->settings['pimagicvalue'] ) ? $this->settings['pimagicvalue'] : $this->default_pimagicvalue;

            $this->cvv_script                        = apply_filters( 'woocommerce_sagepay_pi_use_cvv_for_token_payments', TRUE );
            $this->sagelink                          = false;
            $this->sagelogo                          = false;

            // ReferrerID
            $this->referrerid                        = 'F4D0E135-F056-449E-99E0-EC59917923E1';

            // Make sure $this->vendortxcodeprefix is clean
            $this->vendortxcodeprefix = str_replace( '-', '_', $this->vendortxcodeprefix );

            // Set the integration key and password for Live/Test
            if ( $this->status == 'live' ) {
                $this->Integration_Key      = $this->Live_Integration_Key;
                $this->Integration_Password = $this->Live_Integration_Password;
            } else {
                $this->Integration_Key      = $this->Test_Integration_Key;
                $this->Integration_Password = $this->Test_Integration_Password;
            }

            // URLS
            if( $this->status == 'live' ) {
                $this->merchant_session_keys_url    = 'https://pi-live.sagepay.com/api/v1/merchant-session-keys';
                $this->card_identifiers_url         = 'https://pi-live.sagepay.com/api/v1/card-identifiers';
                $this->transaction_url              = 'https://pi-live.sagepay.com/api/v1/transactions';
                $this->callbackURL                  = 'https://pi-live.sagepay.com/api/v1/transactions/<transactionId>/3d-secure';
                $this->callbackURLTwo               = 'https://pi-live.sagepay.com/api/v1/transactions/<transactionId>/3d-secure-challenge';
                $this->retrieve_url                 = 'https://pi-live.sagepay.com/api/v1/transactions/<transactionId>';
                $this->instructions_url             = 'https://pi-live.sagepay.com/api/v1/transactions/<transactionId>/instructions';
            } else {
                $this->merchant_session_keys_url    = 'https://pi-test.sagepay.com/api/v1/merchant-session-keys';
                $this->card_identifiers_url         = 'https://pi-test.sagepay.com/api/v1/card-identifiers';
                $this->transaction_url              = 'https://pi-test.sagepay.com/api/v1/transactions';
                $this->callbackURL                  = 'https://pi-test.sagepay.com/api/v1/transactions/<transactionId>/3d-secure';
                $this->callbackURLTwo               = 'https://pi-test.sagepay.com/api/v1/transactions/<transactionId>/3d-secure-challenge';
                $this->retrieve_url                 = 'https://pi-test.sagepay.com/api/v1/transactions/<transactionId>';
                $this->instructions_url             = 'https://pi-test.sagepay.com/api/v1/transactions/<transactionId>/instructions';
            }

            // Supports
            $this->supports = array(
                                'products',
                                'refunds',
                                'subscriptions',
                                'subscription_cancellation',
                                'subscription_reactivation',
                                'subscription_suspension',
                                'subscription_amount_changes',
                                'subscription_payment_method_change',
                                'subscription_payment_method_change_customer',
                                'subscription_payment_method_change_admin',
                                'subscription_date_changes',
                                'multiple_subscriptions',
                                'pre-orders',
                                'tokenization'
                            );

            // Logs
            if ( $this->debug ) {
                $this->log = new WC_Logger();
            }

            // WC version
            $this->wc_version = get_option( 'woocommerce_version' );

            // Add test card info to the description if in test mode
            if ( $this->status != 'live' ) {
                $this->description .= ' ' . sprintf( __( '<br /><br />TEST MODE ENABLED.<br />In test mode, you can use Visa card number 4929000000006 with any CVC and a valid expiration date or check the documentation (<a href="%s">Test card details for your test transactions</a>) for more card numbers.<br /><br />3D Secure password is "password"', 'woocommerce-gateway-sagepay-form' ), 'http://www.sagepay.co.uk/support/12/36/test-card-details-for-your-test-transactions' );
                $this->description  = trim( $this->description );
            }

            // Set URLs for loading script files from Sage.
            $this->checkout_test_script_url = 'https://pi-test.sagepay.com/api/v1/js/sagepay.js';
            $this->checkout_live_script_url = 'https://pi-live.sagepay.com/api/v1/js/sagepay.js';
            $this->dropin_test_script_url   = 'https://pi-test.sagepay.com/api/v1/js/sagepay-dropin.js';
            $this->dropin_live_script_url   = 'https://pi-live.sagepay.com/api/v1/js/sagepay-dropin.js';

            // Set checkout script.
            $this->checkout_script_url      = $this->status != 'live' ? $this->checkout_test_script_url : $this->checkout_live_script_url;
            $this->dropin_script_url        = $this->status != 'live' ? $this->dropin_test_script_url : $this->dropin_live_script_url;

            // Add scripts and files for admin.
            add_action( 'admin_init', array( $this, 'admin_init' ) );


            // WooCommerce payment gateway API
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

            // Check this is enabled 
            if( $this->enabled == 'yes' ) {
                /**
                 *  API
                 *  woocommerce_api_{lower case class name}
                 */
                add_action( 'woocommerce_api_wc_gateway_opayo_pi', array( $this, 'check_response' ) );
                add_action( 'woocommerce_receipt_opayopi', array($this, 'authorise_3dsecure') );

                // Capture authorised payments
                add_action ( 'woocommerce_order_action_opayopi_process_payment', array( $this, 'process_instruction' ) );

                // Pre-Orders
                if ( class_exists( 'WC_Pre_Orders_Order' ) ) {
                    add_action( 'wc_pre_orders_process_pre_order_completion_payment_' . $this->id, array( $this, 'process_instruction' ) );
                }

            }

        } // END __construct

        /**
         * init_form_fields function.
         *
         * @access public
         * @return void
         */
        function init_form_fields() {
            include ( SAGEPLUGINPATH . 'assets/php/opayo-pi-admin.php' );
        }

        public function admin_init() {

            // Plugin Links
            // add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this,'plugin_links' ) );

            // Enqueue Admin Scripts and CSS
            // add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ), 99 );

            // Add 'Capture Authorised Payment' to WooCommerce Order Actions
            // add_filter( 'woocommerce_order_actions', array( $this, 'worldpay_online_woocommerce_order_actions' ) );

            // Set wc-authorized in WooCommerce order statuses.
            // add_filter( 'wc_order_statuses', array( $this, 'worldpay_online_order_statuses' ) );
        }

        /**
         * [get_icon description] Add selected card icons to payment method label, defaults to Visa/MC/Amex/Discover
         * @return [type] [description]
         */
        public function get_icon() {
            return WC_Sagepay_Common_Functions::get_icon( $this->cardtypes, $this->sagelink, $this->sagelogo, $this->id );
        }

        /**
         * Payment form on checkout page
         * http://integrations.sagepay.co.uk/content/getting-started-integrate-using-drop-checkout
         */
        public function payment_fields() {

            if( isset( $this->checkout_form ) && $this->checkout_form == 'woocommerce' ) {

                // WooCommerce checkout form
                include_once( 'opayo-pi-payment-fields-class.php' );
                $payment_fields = new Opayo_Pi_Payment_Fields();
                $payment_fields->fields();

            } else {
?>
                <fieldset id = "opayopi-cc-form" class="wc-payment-form">
<?php           
                    do_action( 'woocommerce_credit_card_form_before', $this->id );
                    $merchantSessionKeyArray  = $this->get_merchantSessionKey();
                    $merchantSessionKey       = $merchantSessionKeyArray["merchantSessionKey"];
                    $merchantSessionKeyExpiry = $merchantSessionKeyArray["expiry"];
?>
                    <div id="hosted-payments-container"></div>
                    <script type="text/javascript" src="<?php echo $this->checkout_script_url; ?>"></script>
                    <script type="text/javascript">
                        formName = document.getElementsByName('checkout')[0] || document.getElementById('order_review') || document.getElementById('add_payment_method');
                      sagepayCheckout({
                        merchantSessionKey: '<?php echo $merchantSessionKey; ?>',
                        containerSelector:  '#hosted-payments-container',
                      }).form({ formName });
                    </script>
<?php
                    do_action( 'woocommerce_credit_card_form_after', $this->id ); 
?>
                    <div class="clear"></div>
                </fieldset>
<?php

            }
        }

        /**
         * process_payment function.
         *
         * @access public
         * @param mixed $order_id
         * @return void
         */
        function process_payment( $order_id ) {

            $save_card = false;

            $order = new WC_Order( $order_id );

            // Clear the session
            WC()->session->set( "opayo_3ds", "" );

            // Clear order meta
            delete_post_meta( $order_id, '_opayo_3ds' );

            $merchantSessionKeyArray  = $this->get_merchantSessionKey();

            // Check for errors
            if( isset( $merchantSessionKeyArray['errors'] ) ) {
                $order->add_order_note( $merchantSessionKeyArray['errors'][0]['description'] );
                throw new Exception( __( 'There was an error processing your payment. Please check your details and try again.<br /><br />The error message is : ', 'woocommerce-gateway-sagepay-form' ) . print_r( $merchantSessionKeyArray['errors'][0]['description'], TRUE ) );
                exit;
            }

            // Check for errors
            if( isset( $merchantSessionKeyArray["description"] ) && isset( $merchantSessionKeyArray["code"] ) ) {
                $order->add_order_note( $merchantSessionKeyArray["description"] );
                throw new Exception( __( 'There was an error processing your payment. Please check your details and try again.<br /><br />The error message is : ', 'woocommerce-gateway-sagepay-form' ) . print_r( $merchantSessionKeyArray["description"], TRUE ) );
                exit;
            }

            $merchantSessionKey       = $merchantSessionKeyArray["merchantSessionKey"];
            $merchantSessionKeyExpiry = $merchantSessionKeyArray["expiry"];

            $card_details = array(
                                "merchantSessionKey" => $merchantSessionKey,
                                "cardholderName"     => $order->get_billing_first_name() . " " . $order->get_billing_last_name(),
                                "cardNumber"         => wc_clean( $_POST['opayopi-card-number'] ),
                                "expiryDate"         => wc_clean( $_POST['opayopi-card-expiry'] ),
                                "securityCode"       => wc_clean( $_POST['opayopi-card-cvc'] )
                            );

            if( $this->status != 'live' && $this->pimagicvalue != 'NO' ) {
                $card_details["cardholderName"] = $this->pimagicvalue;
            }

            $cardIdentifier = $this->get_cardIdentifier( $card_details );

            // Check for errors
            if( isset( $cardIdentifier['errors'] ) ) {
                throw new Exception( __( 'There was an error processing your payment. Please check your details and try again.<br /><br />The error message is : ', 'woocommerce-gateway-sagepay-form' ) . print_r( $cardIdentifier['errors'][0]['description'], TRUE ) );
                exit;
            }

            // Save the card?
            if( isset( $_POST['wc-opayopi-new-payment-method'] ) ) {
                $save_card = $this->get_save_card( wc_clean( $_POST['wc-opayopi-new-payment-method'] ), $order );
            }

            $data = array(  
                "transactionType"   => $this->get_txtype( $order ),
                "paymentMethod"     => array (
                                            "card" => array(
                                                            "merchantSessionKey"  => $merchantSessionKey,
                                                            "cardIdentifier"      => $cardIdentifier["cardIdentifier"],
                                                            "save"                => $save_card
                                                        ),
                                        ),
                "vendorTxCode"      => WC_Sagepay_Common_Functions::build_vendortxcode( $order, $this->id, $this->vendortxcodeprefix ),
                "amount"            => $order->get_total() * 100,
                "currency"          => WC_Sagepay_Common_Functions::get_order_currency( $order ),
                "description"       =>  __( 'Order', 'woocommerce-gateway-sagepay-form' ) . ' ' . str_replace( '#' , '' , $order->get_order_number() ),
                "customerFirstName" => $order->get_billing_first_name(),
                "customerLastName"  => $order->get_billing_last_name(),
                "billingAddress"    => array (
                                            "address1"      => $order->get_billing_address_1(),
                                            "city"          => $order->get_billing_city(),
                                            "postalCode"    => $order->get_billing_postcode(),
                                            "country"       => $order->get_billing_country(),
                                        ),
                "entryMethod"       => "Ecommerce",
                "apply3DSecure"     => "UseMSPSetting",
                "applyAvsCvcCheck"  => "UseMSPSetting",
                "customerEmail"     => $order->get_billing_email(),
                "customerPhone"     => $order->get_billing_phone(),
                "referrerId"        => $this->referrerid
            );

            // 3D Secure 2.0
            if( $this->threeds === '2.0' ) {

                if( 0 == $order->get_user_id() ) {
                    $threeDSReqAuthMethod = "NoThreeDSRequestorAuthentication";
                } else {
                    $threeDSReqAuthMethod = "LoginWithThreeDSRequestorCredentials";
                }

                $data["strongCustomerAuthentication"] = array(
                    "website"                   => home_url(),
                    "notificationURL"           => $order->get_checkout_payment_url( true ),
                    "browserIP"                 => get_post_meta( $order_id, '_customer_ip_address', true ),
                    "browserAcceptHeader"       => isset( $_SERVER['HTTP_ACCEPT'] ) ? $_SERVER['HTTP_ACCEPT'] : "text/html, application/json",
                    "browserJavascriptEnabled"  => wc_clean( $_POST['browserJavascriptEnabled'] ) == 'true' ? 1 : 0,
                    "browserJavaEnabled"        => wc_clean( $_POST['browserJavaEnabled'] ) == 'true' ? 1 : 0,
                    "browserLanguage"           => isset( $_POST['browserLanguage'] ) && $_POST['browserLanguage'] != '' ? wc_clean( $_POST['browserLanguage'] ) : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2),
                    "browserColorDepth"         => wc_clean( $_POST['browserColorDepth'] ),
                    "browserScreenHeight"       => wc_clean( $_POST['browserScreenHeight'] ),
                    "browserScreenWidth"        => wc_clean( $_POST['browserScreenWidth'] ),
                    "browserTZ"                 => wc_clean( $_POST['browserTZ'] ),
                    "browserUserAgent"          => isset( $_POST['browserUserAgent'] ) && $_POST['browserUserAgent'] != '' ? wc_clean( $_POST['browserUserAgent'] ) : $_SERVER['HTTP_USER_AGENT'],
                    "challengeWindowSize"       => $this->get_challenge_window_size( wc_clean( $_POST['browserScreenWidth'] ), wc_clean( $_POST['browserScreenHeight'] ) ),
                    "transType"                 => "GoodsAndServicePurchase",
                    "acctID"                    => $order->get_user_id(),
                    "threeDSRequestorAuthenticationInfo" => array(
                        "threeDSReqAuthMethod" => $threeDSReqAuthMethod,
                    ),
                );

            }

            $result = $this->remote_post( $data, $this->transaction_url, NULL, 'Basic' ); 

            // Check for errors
            if( isset( $result['errors'] ) ) {
                $order->add_order_note( $result['errors'][0]['description'] );
                throw new Exception( __( 'There was an error processing your payment. Please check your details and try again.<br /><br />The error message is : ', 'woocommerce-gateway-sagepay-form' ) . print_r( $result['errors'][0]['description'], TRUE ) );
                exit;
            }

            switch( strtoupper( $result['status'] ) ) {

                case 'OK':
                    $this->successful_payment( $result, $order, __('Payment completed', 'woocommerce-gateway-sagepay-form') );

                    $result['result']   = 'success';
                    $result['redirect'] = $this->get_return_url( $order );
                    
                    return $result;

                break;

                case '3DAUTH':
                    /**
                     * This order requires 3D Secure authentication
                     */                 
                    // Set the session variables for 3D Secure
                    WC()->session->set( "opayo_3ds", $result );

                    // Fall back if session not available
                    update_post_meta( $order_id, '_opayo_3ds', $result );

                    /**
                     * go to the pay page for 3d securing
                     */
                    $result['result']   = 'success';
                    $result['redirect'] = $order->get_checkout_payment_url( true );
                    
                    return $result;

                break;

                case 'NOTAUTHED':
                case "REJECTED":
                case "MALFORMED": 
                case "INALID":
                case "ERROR":
                    wp_redirect( $order->get_checkout_payment_url( true ) );
                    exit;
                break;
            }

            return array(
                'result'    => 'success',
                'redirect'  => $order->get_checkout_payment_url( true )
            );
            
        }

        /**
         * [authorise_3dsecure description]
         * @param  [type] $order_id [description]
         * @return [type]           [description]
         */
        function authorise_3dsecure( $order_id ) {

            $order = new WC_Order( $order_id );

            $opayo_3ds  = get_post_meta( $order_id, '_opayo_3ds', TRUE );

            if( !isset( $opayo_3ds['status'] ) ) {
                $opayo_3ds  = WC()->session->get( "opayo_3ds" );
            }

            $transactionId = isset( $opayo_3ds['transactionId'] ) ? $opayo_3ds['transactionId'] : $_POST['transactionId'];

            // wp_mail( 'andrew@chromeorange.co.uk', 'authorise_3dsecure ' . time(), '<pre>' . print_r( $_REQUEST, TRUE ) . '</pre><pre>' . print_r( $opayo_3ds, TRUE ) . '</pre>' );

            if ( isset($_POST['PaRes']) || isset($_POST['cres']) ) {

                if ( isset($_POST['PaRes']) ) {

                    $transactionId = isset( $opayo_3ds['MD'] ) ? $opayo_3ds['MD'] : $_POST['MD'];

                    // set the URL that will be posted to.
                    $url = str_replace( '<transactionId>', $transactionId, $this->callbackURL );

                    $data = array(
                        "paRes" => $_POST['PaRes']
                    );

                }

                if ( isset($_POST['cres']) ) {

                    // set the URL that will be posted to.
                    $url = str_replace( '<transactionId>', $transactionId, $this->callbackURLTwo );

                    $data = array(
                        "cRes" => $_POST['cres']
                    );

                }

                // Send $data to Opayo
                $result = $this->remote_post( $data, $url, NULL, 'Basic' );

                if( isset( $result['status'] ) && in_array( $result['status'], array( 'Ok','Authenticated' ) ) ) {

                    // Get the full status of the transaction
                    // $remote_get_url = str_replace( '<transactionId>', $transactionId, $this->retrieve_url );
                    // $result = $this->remote_get( $remote_get_url, NULL, 'Basic' );

                    $this->successful_payment( $result, $order, __('Payment completed', 'woocommerce-gateway-sagepay-form') );
                    wp_redirect( $this->get_return_url( $order ) );
                    exit;
                } else {
                    // throw new Exception( __('Payment failed.<br />Please check your billing address and card details, including the CVC number on the back of the card.<br />Your card has not been charged', 'woocommerce-gateway-sagepay-form')  );
                    wp_redirect( $order->get_checkout_payment_url() );
                    exit;
                }

            }

            $transactionId  = $opayo_3ds["transactionId"];
            $acsUrl         = $opayo_3ds["acsUrl"];
            $paReq          = isset( $opayo_3ds["paReq"] ) ? $opayo_3ds["paReq"] : NULL;
            $cReq           = isset( $opayo_3ds["cReq"] ) ? $opayo_3ds["cReq"] : NULL;

            if( !is_null( $cReq ) ) {
                $p = array(
                    "field_name"    => "creq",
                    "field_value"   => $cReq
                );

                $m = array(
                    "field_name" => "threeDSSessionData",
                    "field_value" => $transactionId
                );
            } else {
                $p = array(
                    "field_name"    => "PaReq",
                    "field_value"   => $paReq
                );

                $m = array(
                    "field_name" => "MD",
                    "field_value" => $transactionId
                );
            }

            $form = '<form id="submitForm" method="post" action="' . $acsUrl . '">
                        <input type="hidden" name="' . $p['field_name'] . '" value="' . $p['field_value'] . '"/>
                        <input type="hidden" name="' . $m['field_name'] . '" value="' . $m['field_value'] . '"/>
                        <input type="hidden" id="termUrl" name="TermUrl" value="' . $order->get_checkout_payment_url( true ) . '"/>
                        <script>
                            document.getElementById("submitForm").submit();
                        </script>
                    </form>';

            WC_Sagepay_Common_Functions::sagepay_debug( $form, $this->id, __('3D Secure Form ', 'woocommerce-gateway-sagepay-form'), FALSE );

            echo $form;
            
        }

        /**
         * check_sagepay_response function.
         *
         * @access public
         * @return void
         */
        function check_response() {

        }

        /**
         * successful_payment function.
         *
         * @access public
         * @param mixed $sagepay_return_values
         * @return void
         */
        function successful_payment( $result, $order, $message = NULL ) {

            $ordernote  = array();
            $order_note = '';

            $order_id   = $order->get_id();

            if( !is_array( $result ) ) {
                $result = json_decode( $result, TRUE );
            }

            // Add order note
            self::add_order_note( $result, $order, $message );

            if ( class_exists('WC_Pre_Orders') && WC_Pre_Orders_Order::order_contains_pre_order( $order_id ) && WC_Pre_Orders_Order::order_will_be_charged_upon_release( $order_id ) ) {
                // mark order as pre-ordered / reduce order stock
                WC_Pre_Orders_Order::mark_order_as_pre_ordered( $order );
            } else {
                // Complete the order
                $order->payment_complete( isset( $result['transactionId'] ) ? $result['transactionId'] : '' );

                if( $result['transactionType'] === 'Deferred' ) {
                    $order->update_status( 'authorised', _x( 'Payment authorised, you will need to capture this payment before shipping. Use the "Capture Authorised Payment" option in the "Order Actions" dropdown.<br /><br />', 'woocommerce-gateway-sagepay-form' ) );       
                }
            }

        }

        /**
         * [get_merchantSessionKey description]
         * @return [type] [description]
         */
        function get_merchantSessionKey() {

            $Basic_authentication_key = base64_encode( $this->Integration_Key . ':' . $this->Integration_Password );
            $data = array( 
                        "vendorName" => $this->vendor 
                    );

            $merchantSessionKeyArray  = $this->remote_post( $data, $this->merchant_session_keys_url, NULL, 'Basic' );

            $iso_date = (new DateTime())->format('c');

            // $this->check_merchantSessionKey_expiry( $merchantSessionKeyArray["expiry"] );

            return $merchantSessionKeyArray;

        }

        /**
         * [get_cardIdentifier description]
         * @param  [type] $merchantSessionKey [description]
         * @return [type]                     [description]
         */
        function get_cardIdentifier( $card_details = NULL ) {

            if( !$card_details ) {
                return;
            }

            $merchantSessionKey = $card_details['merchantSessionKey'];
            $cardholderName     = $card_details['cardholderName'];
            $cardNumber         = $this->get_clean_card_number( $card_details['cardNumber'] );
            $expiryDate         = $this->get_clean_expiry_date( $card_details['expiryDate'] );
            $securityCode       = $card_details['securityCode'];

            $data = array( 
                        "cardDetails" => array( 
                                            "cardholderName"  => $cardholderName,
                                            "cardNumber"      => $cardNumber,
                                            "expiryDate"      => $expiryDate,
                                            "securityCode"    => $securityCode
                                        )
                    );

            $cardIdentifier  = $this->remote_post( $data, $this->card_identifiers_url, $merchantSessionKey, 'Bearer' );

            return $cardIdentifier;

        }

        /**
         * [check_merchantSessionKey_expiry description]
         * @param  [type] $expiry [description]
         * @return [type]         [description]
         */
        function check_merchantSessionKey_expiry( $expiry ) {

            $current_date_time = (new DateTime())->format('c');

            // $expiry = new DateTime( $expiry );

            wp_mail( 'andrew@chromeorange.co.uk', 'check_merchantSessionKey_expiry ' . time(), '<pre>' . print_r( $current_date_time > $expiry, TRUE ) . '</pre>' . print_r( $current_date_time, TRUE ) . ' ' . print_r( $expiry, TRUE )  );

            // throw new Exception( $interval );

        }

        /**
         * [get_txtype description]
         * @param  [type] $order [description]
         * @return [type]        [description]
         */
        function get_txtype( $order ) {

            $order_id = $order->get_id();

            wp_mail( 'andrew@chromeorange.co.uk', 'This ' . time(), print_r( $this, TRUE ) );

            // Paying for a "Pay Later" Pre Order
            if( isset( $_GET['pay_for_order'] ) && $_GET['pay_for_order'] == true && class_exists( 'WC_Pre_Orders' ) && WC_Pre_Orders_Order::order_contains_pre_order( $order_id ) ) {
                return 'PAYMENT';
            }
            
            if( class_exists( 'WC_Pre_Orders' ) && WC_Pre_Orders_Order::order_contains_pre_order( $order_id ) && WC_Pre_Orders_Order::order_will_be_charged_upon_release( $order_id ) ) {
                return 'DEFERRED';
            } else {
                return $this->txtype;
            }

        }

        /**
         * [get_save_card description]
         * @param  [type] $save_card [description]
         * @param  [type] $order     [description]
         * @return [type]            [description]
         */
        function get_save_card( $save_card, $order ) {

            $order_id = $order->get_id();
            
            // Set $save based on the checkbox on the checkout form.
            $save = isset( $save_card ) ? $save_card : false;

            // Make sure we save the card if the cart contains a Pre-Order
            if ( class_exists('WC_Pre_Orders') && WC_Pre_Orders_Order::order_contains_pre_order( $order_id ) && WC_Pre_Orders_Order::order_will_be_charged_upon_release( $order_id ) ) {
                $save = true;
            }

            // Make sure we save the card if the cart contains a Subscription
            if ( function_exists( 'wcs_order_contains_subscription' ) && ( wcs_order_contains_subscription( $order ) ) ) {
                $save = true;
            }          

            return $save;
        }

        /**
         * [add_order_note description]
         * @param [type] $result  [description]
         * @param [type] $order   [description]
         * @param [type] $message [description]
         */
        function add_order_note( $result, $order, $message = NULL ) {

            // Order ID
            $order_id = $order->get_id();

            // Get order note
            $order_note = '';
            $ordernote = self::get_order_note( $result );

            if( !is_null( $message ) ) {
                $message = $message . '<br />';
            }
            // Make the order note
            if( !empty( $ordernote ) ) {
                foreach ( $ordernote as $key => $value ) {
                    $order_note .= $key . ' : ' . $value . "\r\n";
                }
            }

            $order->add_order_note( $message . $order_note );

            // Add transaction information to 
            update_post_meta( $order_id , '_sageresult', $result );

            // Security Checks
            if( isset( $result['avsCvcCheck']['status'] ) ) {
                update_post_meta( $order_id, '_AVSCV2' , strtoupper( $result['avsCvcCheck']['status'] ) );
            }

            if( isset( $result['avsCvcCheck']['address'] ) ) {
                update_post_meta( $order_id, '_AddressResult' , strtoupper( $result['avsCvcCheck']['address'] ) );
            }

            if( isset( $result['avsCvcCheck']['postalCode'] ) ) {
                update_post_meta( $order_id, '_PostCodeResult' , strtoupper( $result['avsCvcCheck']['postalCode'] ) );
            }

            if( isset( $result['avsCvcCheck']['securityCode'] ) ) {
                update_post_meta( $order_id, '_CV2Result' , strtoupper( $result['avsCvcCheck']['securityCode'] ) );
            }

            if( isset( $result['3DSecure']['status'] ) ) {
                update_post_meta( $order_id, '_SecureStatus' , strtoupper( $result['3DSecure']['status'] ) );
            }

        }

        /**
         * [get_order_note description]
         * @param  [type] $result [description]
         * @return [type]         [description]
         */
        function get_order_note( $result ) {

            $ordernote = array();

            if( isset( $result['status'] ) ){
                $ordernote['status'] = $result['status'];
            }

            if( isset( $result['statusCode'] ) ) {
                $ordernote['statusCode'] = $result['statusCode'];
            }

            if( isset( $result['statusDetail'] ) ) {
                $ordernote['statusDetail'] = $result['statusDetail'];
            }

            if( isset( $result['transactionId'] ) ) {
                $ordernote['transactionId'] = $result['transactionId'];
            }

            if( isset( $result['transactionType'] ) ) {
                $ordernote['transactionType'] = $result['transactionType'];
            }

            if( isset( $result['retrievalReference'] ) ) {
                $ordernote['retrievalReference'] = $result['retrievalReference'];
            }

            if( isset( $result['bankResponseCode'] ) ) {
                $ordernote['bankResponseCode'] = $result['bankResponseCode'];
            }

            if( isset( $result['bankAuthorisationCode'] ) ) {
                $ordernote['bankAuthorisationCode'] = $result['bankAuthorisationCode'];
            }

            if( isset( $result['paymentMethod']['card'] ) ) {
                $ordernote['cardType']          = $result['paymentMethod']['card']['cardType'];
                $ordernote['lastFourDigits']    = $result['paymentMethod']['card']['lastFourDigits'];
                $ordernote['expiryDate']        = $result['paymentMethod']['card']['expiryDate'];
            }

            if( isset( $result['amount'] ) ) {
                $ordernote['totalAmount']       = $result['amount']['totalAmount'];
                $ordernote['saleAmount']        = $result['amount']['saleAmount'];
                $ordernote['surchargeAmount']   = $result['amount']['surchargeAmount'];
            }

            if( isset( $result['currency'] ) ) {
                $ordernote['currency'] = $result['currency'];
            }

            if( isset( $result['avsCvcCheck'] ) ) {
                $ordernote['avsCvcCheckStatus']         = $result['avsCvcCheck']['status'];
                $ordernote['avsCvcCheckAddress']        = $result['avsCvcCheck']['address'];
                $ordernote['avsCvcCheckPostalCode']     = $result['avsCvcCheck']['postalCode'];
                $ordernote['avsCvcCheckSecurityCode']   = $result['avsCvcCheck']['securityCode'];
            }

            if( isset( $result['3DSecure'] ) ) {
                $ordernote['3DSecureStatus'] = $result['3DSecure']['status'];
            }

            return $ordernote;

        }

        /**
         * [remote_post description]
         * @param  [type] $data          [description]
         * @param  [type] $url           [description]
         * @param  [type] $authorization [description]
         * @param  [type] $auth_method   [description]
         * @return [type]                [description]
         */
        function remote_post( $data, $url, $authorization = NULL, $auth_method = NULL ) {

            include_once( 'opayo-pi-remote-post-class.php' );

            $result = new Opayo_Pi_Remote_Post( $data, $url, $authorization, $auth_method );

            return $result->post();

        }

        /**
         * [remote_get description]
         * @param  [type] $url           [description]
         * @param  [type] $authorization [description]
         * @param  [type] $auth_method   [description]
         * @return [type]                [description]
         */
        function remote_get( $url, $authorization = NULL, $auth_method = NULL ) {
            global $wp_version;

            if ( is_null( $authorization ) ) {
                $authorization = base64_encode( $this->Integration_Key . ':' . $this->Integration_Password );
            }

            if( !is_null( $auth_method ) ) {
                $headers = array(
                    "Authorization" => $auth_method . " " . $authorization
                );
            }

            $args = array(
                        'timeout'     => 150,
                        'redirection' => 5,
                        'httpversion' => '1.0',
                        'user-agent'  => 'WordPress/' . $wp_version . '; ' . home_url(),
                        'blocking'    => true,
                        'headers'     => $headers,
                        'cookies'     => array(),
                        'body'        => null,
                        'compress'    => false,
                        'decompress'  => true,
                        'sslverify'   => true,
                        'stream'      => false,
                        'filename'    => null
                    );          

            $return = wp_remote_get( $url, $args );

            return wp_remote_retrieve_body( $return );

        }

        /**
         * [get_clean_card_number description]
         * @param  [type] $card_number [description]
         * @return [type]              [description]
         */
        function get_clean_card_number( $card_number ) {

            return str_replace( array( ' ', '-' ), '', $card_number );

        }

        /**
         * [get_clean_expiry_date description]
         * @param  [type] $expiry_date [description]
         * @return [type]              [description]
         */
        function get_clean_expiry_date ( $expiry_date ) {

            $expiry_date    = array_map( 'trim', explode( '/', $expiry_date ) );
            $month          = str_pad( $expiry_date[0], 2, "0", STR_PAD_LEFT );
            $year           = $expiry_date[1];

            return $month . $year;
        }

        /**
         * Returns the plugin's url without a trailing slash
         */
        public function get_plugin_url() {
            return str_replace( '/classes/pi', '/', untrailingslashit( plugins_url( '/', __FILE__ ) ) );
        }

        /**
         * [sagepay_system_status description]
         * @return [type] [description]
         */
        function sagepay_system_status() {
            $description = __( 'Take payments securly with Opayo Pi', 'woocommerce-gateway-sagepay-form' );
            return $description;
        }

        /**
         * Enqueues our SagePay Pi checkout script.
         * @since 5.0.0
         */
        public function checkout_script() {
            wp_enqueue_script(
                'opayo-pi-checkout',
                $this->checkout_script_url,
                array( 'jquery' ),
                SAGEVERSION
            );
        }

        /**
         * SagePay Pi Refund Processing
         * @param  [type]        $order_id [description]
         * @param  [type]        $amount   [description]
         * @param  [type]        $reason   [description]
         * @return [type]                  [description]
         */
        function process_refund( $order_id, $amount = NULL, $reason = '' ) {

            $payment_method = $order->get_payment_method();

            if( isset( $payment_method ) && $payment_method == 'opayopi' ) {
            
                include_once( 'opayo-pi-refunds-class.php' );
                $refund = new WC_Gateway_Opayo_Pi_Refunds( $order_id, $amount, $reason );

                return $refund->refund();

            }

        } // process_refund

        /**
         * SagePay Pi Instruction Processing
         * @param  Varien_Object $payment [description]
         * @param  [type]        $amount  [description]
         * @return [type]                 [description]
         */
        function process_instruction( $order, $instruction = 'release' ) {

            $payment_method = $order->get_payment_method();

            wp_mail( 'andrew@chromeorange.co.uk', 'process_instruction ' . time(), $payment_method . ' ' . $instruction );

            if( isset( $payment_method ) && $payment_method == 'opayopi' ) {
            
                include_once( 'opayo-pi-instructions-class.php' );
                $instruction = new WC_Gateway_Opayo_Pi_Instructions( $order, $instruction );

                return $instruction->instruction();

            }

        } // process_instruction

        /**
         * Return challenge window size
         *
         * 01 = 250 x 400
         * 02 = 390 x 400
         * 03 = 500 x 600
         * 04 = 600 x 400
         * 05 = Full screen
         */
        function get_challenge_window_size( $width, $height ) {

            if( $width <= '250' ) {
                return 'Small';
            }

            if( $width <= '390' ) {
                return 'Medium';
            }

            if( $width <= '500' ) {
                return 'Large';
            }

            if( $width <= '600' ) {
                return 'ExtraLarge';
            }

            return 'FullScreen';

        }

    } // END CLASS
