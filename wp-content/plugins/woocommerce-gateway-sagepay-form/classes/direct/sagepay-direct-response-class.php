<?php
    /**
     * Process Response for SagePay Direct
     */
    class Sagepay_Direct_Response extends WC_Gateway_Sagepay_Direct {

        private $sageresult;
        private $order;

        public function __construct( $sageresult, $order ) {

            parent::__construct();

            $this->sageresult   = $sageresult;
            $this->order        = $order;
            $this->settings     = get_option( 'woocommerce_sagepaydirect_settings' );

        }

        function process() {
            // woocommerce order instance
            $order = $this->order;

            // settings
            $settings = $this->settings;

            // Drop out if SagePay Direct is not enabled
            if( !isset( $settings['enabled'] ) || $settings['enabled'] != "yes" ) {
                wp_die( "Sage Request Failure<br />" . 'Access denied', "Sage Failure", array( 'response' => 200 ) );
                exit;
            }

            // Make sure we've got the WooCommerce order object
            if( !is_object( $order ) ) {
                $order = wc_get_order( $order );
            }

            $order_id   = $order->get_id();

            $sageresult = $this->sageresult;

            switch( strtoupper( $sageresult['Status'] ) ) {
                case 'OK':
                case 'REGISTERED':
                case 'AUTHENTICATED':

                    // Add the sanitized card details to $sageresult
                    $_SagePaySantizedCardDetails = get_post_meta( $order_id, '_SagePaySantizedCardDetails', TRUE );

                    if( isset($_SagePaySantizedCardDetails) && $_SagePaySantizedCardDetails != '' ) {

                        // Unset the ExpiryDate from Sage, make sure $sageresult is nice and tidy
                        unset( $sageresult['ExpiryDate'] );

                        // Add the card details to $sageresult
                        $sageresult['CardNumber']   = $_SagePaySantizedCardDetails['CardNumber'];
                        $sageresult['ExpiryDate']   = $_SagePaySantizedCardDetails['ExpiryDate'];
                        $sageresult['CardType']     = $_SagePaySantizedCardDetails['CardType'];

                        // Add GiftAidPayment for using in renewals.
                        if( isset( $_SagePaySantizedCardDetails['GiftAidPayment'] ) && $_SagePaySantizedCardDetails['GiftAidPayment'] == '1' ) {
                            $sageresult['GiftAidPayment']     = $_SagePaySantizedCardDetails['GiftAidPayment'];
                        }
                 
                    }

                    // Add Order notes to Admin email
                    if( $this->sagepaytransinfo ) {
                        update_post_meta( $order_id, '_sageresult' , $sageresult );
                    }

                    // Add Order Note
                    $this->add_order_note( __('Payment completed', 'woocommerce-gateway-sagepay-form'), $sageresult, $order );

                    // Update Order Meta
                    $this->update_order_meta( $sageresult, $order_id );

                    // Update transaction type
                    $_SagePayTransactionType = get_post_meta( $order_id, '_SagePayTxType', TRUE );

                    // Set the order status
                    // Paying for a "Pay Later" Pre Order
                    if( isset( $_GET['pay_for_order'] ) && $_GET['pay_for_order'] == true && class_exists( 'WC_Pre_Orders' ) && WC_Pre_Orders_Order::order_contains_pre_order( $order_id ) ) {

                        $VPSTxId = isset( $result['VPSTxId'] ) ? str_replace( array('{','}'),'',$sageresult['VPSTxId'] ) : NULL;

                        $order->payment_complete( $VPSTxId );

                        if ( $sageresult['Status'] === 'AUTHENTICATED' || $sageresult['Status'] === 'REGISTERED' || ( isset($_SagePayTransactionType) && $_SagePayTransactionType == 'DEFERRED' ) ) {
                            $order->update_status( 'authorised', _x( 'Payment authorised, you will need to capture this payment before shipping. Use the "Capture Authorised Payment" option in the "Order Actions" dropdown.<br /><br />', 'woocommerce-gateway-sagepay-form' ) );
                        }

                    } elseif ( class_exists('WC_Pre_Orders') && WC_Pre_Orders_Order::order_contains_pre_order( $order_id ) && WC_Pre_Orders_Order::order_will_be_charged_upon_release( $order_id ) ) {
                        // mark order as pre-ordered / reduce order stock
                        WC_Pre_Orders_Order::mark_order_as_pre_ordered( $order );
                    } else {

                        $VPSTxId = isset( $result['VPSTxId'] ) ? str_replace( array('{','}'),'',$sageresult['VPSTxId'] ) : NULL;

                        $order->payment_complete( $VPSTxId );

                        if ( $sageresult['Status'] === 'AUTHENTICATED' || $sageresult['Status'] === 'REGISTERED' || ( isset($_SagePayTransactionType) && $_SagePayTransactionType == 'DEFERRED' ) ) {
                            if( $order->get_total() > 0 ) {
                                $order->update_status( 'authorised', _x( 'Payment authorised, you will need to capture this payment before shipping. Use the "Capture Authorised Payment" option in the "Order Actions" dropdown.<br /><br />', 'woocommerce-gateway-sagepay-form' ) );
                            }
                        }

                    }

                    // Allow plugins to hook in to successful payment
                    do_action( 'woocommerce_sagepay_direct_payment_complete', $sageresult, $order );

                    // If $sageresult['FraudResponse'] is set and the check has failed then update the order status so that the store owner can check the order
                    if( isset( $sageresult['FraudResponse'] ) && ( $sageresult['FraudResponse'] === 'DENY' || $sageresult['FraudResponse'] === 'CHALLENGE' ) ) {
                        // Mark for fraud screening
                        $order->update_status( 'fraud-screen', _x( 'Sage Fraud Response ', 'woocommerce-gateway-sagepay-form' ) . $sageresult['FraudResponse'] . _x( '. Login to MySagePay and check this order before shipping.', 'woocommerce-gateway-sagepay-form' ) );
                    }

                    // Empty Cart
                    if( is_callable( 'wc_empty_cart' ) ) {
                        wc_empty_cart(); 
                    }

                    // Clean up Order Meta
                    $delete_card_details = apply_filters( 'opayo_delete_sanitized_card_details', true, $order_id );
                    if( $delete_card_details ) {
                        delete_post_meta( $order_id, '_SagePaySantizedCardDetails' );
                    }
                    delete_post_meta( $order_id, '_sage_3dsecure' );

                    // Redirect customer to thank you page
                    $order_received_url = $order->get_checkout_order_received_url();

                    // Set url tracking
                    if( $this->threeDS_tracking ) {
                        $order_received_url = add_query_arg( array(
                            'utm_nooverride' => 1
                        ), $order_received_url );
                    }

                    // Go to the pay page for 3d securing
                    $sageresult['result']   = 'success';
                    $sageresult['redirect'] = $order_received_url;
                    
                    return $sageresult;

                break;

                case 'PAYPALOK':

                    // Empty Cart
                    if( is_callable( 'wc_empty_cart' ) ) {
                        wc_empty_cart(); 
                    }

                    $paypalok_result = $this->send_paypal_complete( $sageresult, $order );

                break;

                case '3DAUTH':

                    // This order requires 3D Secure authentication
                    WC()->session->set( "sage_3ds", "" );
                    delete_post_meta( $order_id, '_sage_3ds' );
                     
                    if( strtoupper( $sageresult['3DSecureStatus'] ) == 'OK' ) {
                    
                        // Set url tracking
                        $complete3d = $order->get_checkout_payment_url( true );
                        if( $this->threeDS_tracking ) {
                            $complete3d = add_query_arg( array(
                                'utm_nooverride' => 1
                            ), $complete3d );
                        }

                        $sage_3ds                   = $sageresult;
                        $sage_3ds["TermURL"]        = $order->get_checkout_payment_url( true );
                        $sage_3ds["Complete3d"]     = $complete3d;
                        $sage_3ds['VendorTxCode']   = get_post_meta( $order_id, '_VendorTxCode', TRUE );

                        if( isset( $sageresult['change_payment_method'] ) && $sageresult['change_payment_method'] != "" ) {
                            $sage_3ds['change_payment_method'] = $sageresult['change_payment_method'];
                        }

                        // Set the session variables for 3D Direct
                        WC()->session->set( "sage_3ds", $sage_3ds );
                        // Order meta for failed session
                        update_post_meta( $order_id, '_sage_3ds', $sage_3ds );

                        // Go to the pay page for 3d securing
                        $sageresult['result']   = 'success';
                        $sageresult['redirect'] = $order->get_checkout_payment_url( true );

                        // Set url tracking
                        if( $this->threeDS_tracking ) {
                            $sageresult['redirect'] = add_query_arg( array(
                                'utm_nooverride' => 1
                            ), $sageresult['redirect'] );
                        }

                    }

                    return $sageresult;
                
                break;

                case 'PPREDIRECT':

                    // Go to paypal
                    $sageresult['result']   = 'success';
                    $sageresult['redirect'] = $sageresult['PayPalRedirectURL'];
                    
                    return $sageresult;
                
                break;

                case 'EXISTINGTOKEN':

                    // Add Order Note
                    $this->add_order_note( __('Payment completed', 'woocommerce-gateway-sagepay-form'), $sageresult, $order );

                    update_post_meta( $order_id, '_SagePayDirectToken' , str_replace( array('{','}'),'',$sageresult['Token'] ) );

                    // Update Order Meta
                    $this->update_order_meta( $sageresult, $order_id );

                    $order->payment_complete();

                    wp_redirect( $order->get_checkout_order_received_url() ); 
                    exit;
                
                break;

                case 'CHANGEPAYMENTMETHODEXISTINGTOKEN':

                    // Subscription ID
                    $subscription_id    = $order_id;

                    // Set return URL, return customer to My Account -> View Subscription
                    $return_url = wc_get_endpoint_url( 'view-subscription', $subscription_id, wc_get_page_permalink( 'myaccount' ) );

                    if( isset( $sageresult['Token'] ) && $sageresult['Token'] != '' ) {
                        // Get parent order ID
                        $subscription       = new WC_Subscription( $order_id );
                        $parent_order       = $subscription->get_parent();
                        $parent_order_id    = $subscription->get_parent_id();

                        // Old payment method
                        $old_method = get_post_meta( $subscription_id, '_payment_method', TRUE );

                        // Payment method change has been successfull, if the old payment method was PayPal then it needs to be cancelled at PayPal
                        if( class_exists( 'WCS_PayPal_Status_Manager' ) && $old_method == 'paypal') {
                            $payal_subscription_cancelled = WCS_PayPal_Status_Manager::cancel_subscription( $subscription );
                        }

                        // Update Parent Order with new token info
                        update_post_meta( $parent_order_id, '_SagePayDirectToken' , str_replace( array('{','}'),'',$sageresult['Token'] ) );

                        // Update Subscription with token info
                        update_post_meta( $subscription_id, '_SagePayDirectToken' , str_replace( array('{','}'),'',$sageresult['Token'] ) );

                        // Create message for customer
                        // $message = __('Your payment method has been updated.', 'woocommerce-gateway-sagepay-form');
                        // wc_add_notice( $message, 'success' );

                        // Create message for customer
                        $this->sagepay_message( ( __('Your payment method has been updated.', 'woocommerce-gateway-sagepay-form') ) , 'success', $subscription_id );

                    } else {
                        // Set failed payment method change message
                        // $message = __('Your payment method has not updated, please try again.', 'woocommerce-gateway-sagepay-form');
                        // wc_add_notice( $message, 'error' );

                        // Create message for customer
                        $this->sagepay_message( ( __('Your payment method has not updated, please try again.', 'woocommerce-gateway-sagepay-form') ) , 'error', $subscription_id );
                    }

                    // Add a note to the subscription
                    if( isset( $sageresult['StatusDetail'] ) && $sageresult['StatusDetail'] !== '' ) {
                        $order->add_order_note( $sageresult['StatusDetail'] );
                    }

                    wp_redirect( $return_url ); 
                    exit;
                
                break;

                case 'CHANGEPAYMENTMETHODNEWTOKEN' :

                    // Subscription ID
                    $subscription_id        = $order_id;
                    $sageresult['Token']    = get_post_meta( $order_id, '_SagePayDirectToken', TRUE );

                    // Set return URL, return customer to My Account -> View Subscription
                    $return_url = wc_get_endpoint_url( 'view-subscription', $subscription_id, wc_get_page_permalink( 'myaccount' ) );

                    if( isset( $sageresult['Token'] ) && $sageresult['Token'] != '' && isset( $sageresult['OpayoStatus'] ) && in_array( $sageresult['OpayoStatus'], array('OK','AUTHENTICATED') ) ) {
                        
                        // Get parent order ID
                        $subscription       = new WC_Subscription( $order_id );
                        $parent_order       = $subscription->get_parent();
                        $parent_order_id    = $subscription->get_parent_id();

                        // Old payment method
                        $old_method = get_post_meta( $subscription_id, '_payment_method', TRUE );

                        // Update payment method
                        update_post_meta( $subscription_id, '_payment_method', 'sagepaydirect' );

                        // Update VPSProtocol
                        if( isset( $sageresult['VPSProtocol'] ) ) {
                            update_post_meta( $subscription_id, '_VPSProtocol', $sageresult['VPSProtocol'] );
                        }

                        // Payment method change has been successfull, if the old payment method was PayPal then it needs to be cancelled at PayPal
                        if( class_exists( 'WCS_PayPal_Status_Manager' ) && $old_method == 'paypal') {
                            $payal_subscription_cancelled = WCS_PayPal_Status_Manager::cancel_subscription( $subscription );
                        }

                        // Create message for customer
                        // $message = __('Your payment method has been updated.', 'woocommerce-gateway-sagepay-form');
                        // wc_add_notice( $message, 'success' );

                        // Create message for customer
                        $this->sagepay_message( ( __('Your payment method has been updated.', 'woocommerce-gateway-sagepay-form') ) , 'success', $subscription_id );

                    } else {

                        // Remove the failed token from the Subscription
                        delete_post_meta( $order_id, '_SagePayDirectToken' );

                        // Set failed payment method change message
                        // $message = __('Your payment method has not updated, please try again.', 'woocommerce-gateway-sagepay-form');
                        // wc_add_notice( $message, 'error' );

                        // Create message for customer
                        $this->sagepay_message( ( __('Your payment method has not updated, please try again.', 'woocommerce-gateway-sagepay-form') ) , 'error', $subscription_id );

                    }

                    // Add a note to the subscription
                    if( isset( $sageresult['StatusDetail'] ) && $sageresult['StatusDetail'] !== '' ) {
                        $order->add_order_note( $sageresult['StatusDetail'] );
                    }

                    wp_redirect( $return_url ); 
                    exit;
                
                break;

                case 'NOTAUTHED':
                case 'MALFORMED':
                case 'INVALID':
                case 'ERROR':

                    $update_order_status = apply_filters( 'woocommerce_opayo_direct_failed_order_status', 'pending', $order, $sageresult );
                  
                    // Add Order Note
                    $this->add_order_note( __('Payment failed', 'woocommerce-gateway-sagepay-form'), $sageresult, $order );

                    // Create message for customer
                    $this->sagepay_message( ( __('Payment error. Please try again, your card has not been charged.', 'woocommerce-gateway-sagepay-form') . ': ' . $sageresult['StatusDetail'] ) , 'error', $order_id );
                    
                    // Update the order status
                    $order->update_status( $update_order_status );

                    // Clean up Order Meta
                    delete_post_meta( $order_id, '_sage_3dsecure' );

                    // Clear session variables
                    WC()->session->set( "sage_3ds", "" );

                    $sageresult['result']   = 'success';
                    $sageresult['redirect'] = wc_get_checkout_url();

                    return $sageresult;

                break;

                case 'REJECTED':

                    // Add Order Note
                    $this->add_order_note( __('Payment failed, there was a problem with 3D Secure or Address Verification', 'woocommerce-gateway-sagepay-form'), $sageresult, $order );

                    // Create message for customer
                    $this->sagepay_message( (__('Payment error.<br />There was a problem when verifying your card, please check your details and try again.<br />Your card has not been charged.', 'woocommerce-gateway-sagepay-form') ) , 'error', $order_id );

                    // Clean up Order Meta
                    delete_post_meta( $order_id, '_sage_3dsecure' );

                    // Clear session variables
                    WC()->session->set( "sage_3ds", "" );
                
                    $sageresult['result']   = 'success';
                    $sageresult['redirect'] = wc_get_checkout_url();

                    return $sageresult;

                break;

                default :

                    // Should never get here. 
                    WC_Sagepay_Common_Functions::sagepay_debug( $sageresult, 'sagepay_order_cannot_be_paid', 'Logging "default" Order : ' . $order_id, FALSE );

                    if ( $order->needs_payment() ) {                   
                        wp_redirect( $this->get_return_url( $order ) );
                        exit;
                    } else {
                        wp_redirect( $order->get_checkout_order_received_url() );
                        exit;
                    }
            }

        }

        /**
         * [send_paypal_complete description]
         * @param  [type] $sageresult [description]
         * @param  [type] $order      [description]
         * @return [type]             [description]
         */
        function send_paypal_complete( $sageresult, $order ) {

            // make your query.
            $data    = array(
                "VPSProtocol"       =>  $this->vpsprotocol,
                "TxType"            =>  'COMPLETE',
                "VPSTxId"           =>  $sageresult['VPSTxId'],
                "Amount"            =>  $order->get_total(),
                "Accept"            =>  'YES'
            );

            $result = $this->sagepay_post( $data, $this->paypalcompletion );

            // Add Order Note
            $this->add_order_note( __('PayPal Transaction Complete', 'woocommerce-gateway-sagepay-form'), $result, $order );

            $response = new Sagepay_Direct_Response( $result , $order );
            return $response->process();
            
        }

        /**
         * [get_payment_method_title description]
         * @param  [type] $order_id [description]
         * @return [type]           [description]
         */
        function get_payment_method_title( $order_id ) {

            $payment_method_title = get_post_meta( $order_id, '_payment_method_title', TRUE );

            $payment_method_title = apply_filters( 'woocommerce_sagepay_direct_payment_method_title', $payment_method_title, $order_id );

            return $payment_method_title;

        }

    } // End class
