<?php

    /**
     * Sagepay_Direct_Subcription_Renewals class.
     *
     * @extends WC_Gateway_Sagepay_Direct
     * Adds subscriptions support support.
     */
    class Sagepay_Direct_Subcription_Renewals extends WC_Gateway_Sagepay_Direct {

        private $amount_to_charge;
        private $order;

        public function __construct( $amount_to_charge, $order ) {

            parent::__construct();

            $this->amount_to_charge = $amount_to_charge;
            $this->order            = $order;

        }

        /**
         * process scheduled subscription payment
         */
        function process_scheduled_payment() {

            $order = $this->order;

            if( !is_object( $order ) ) {
                $order = new WC_Order( $order );
            }

            // Only go on if the order needs payment.
            if ( $order->needs_payment() ) {

                $order_id = $order->get_id();

                // Get parent order ID
                $subscriptions      = wcs_get_subscriptions_for_renewal_order( $order_id );
                foreach( $subscriptions as $subscription ) {

                    $parent_order      = $subscription->get_parent();

                    $parent_order_id   = $parent_order->get_id();
                    $subscription_id   = $subscription->get_id();

                }

                // Get the id of the subscription
                $subscription_id = key( $subscriptions );

                // Get the last successful renewal order id for this subscription 
                $previous_renewal_id = $this->get_previous_renewal_id( $subscription_id, $parent_order_id );

                // Set $previous_renewal_id = $parent_order_id if the last order does not have a '_RelatedVPSTxId'
                if( !get_post_meta( $previous_renewal_id, '_RelatedVPSTxId', true ) || get_post_meta( $previous_renewal_id, '_RelatedVPSTxId', true ) == '' ) {
                    $previous_renewal_id = $parent_order_id;
                }

                WC_Sagepay_Common_Functions::sagepay_debug( $previous_renewal_id, $this->id . 'Renewal', __('Renewal ID - Opayo Request : ', 'woocommerce-gateway-sagepay-form'), TRUE );

                $VendorTxCode   = 'Renewal-' . $subscription_id . '-' . time();

                // SAGE Line 50 Fix
                $VendorTxCode   = str_replace( 'order_', '', $VendorTxCode );

                // Get related transaction details
                $token = NULL;
                if( $this->saved_cards == 'yes' || $this->vpsprotocol == '4.00' ) {
                    $token = $this->get_token( $previous_renewal_id, $subscription_id, '_SagePayDirectToken' );
                }

                $order_currency = $this->get_order_meta( $previous_renewal_id, $parent_order_id, '_order_currency' );

                if( $token ) {

                    // Update renewal order with token
                    update_post_meta( $order_id, '_SagePayDirectToken', $token );

                    // Get the most recent VPS Protocol used
                    $vpsprotocol    = $this->get_vpsprotocol( $previous_renewal_id, $subscription_id, '_VPSProtocol' );
                    $sageresult     = $this->get_order_meta( $previous_renewal_id, $subscription_id, '_sageresult' );

                    if( is_null( $vpsprotocol ) ) {
                        if( isset( $sageresult ) && isset( $sageresult['VPSProtocol'] ) ) {
                            $vpsprotocol = $sageresult['VPSProtocol'];
                        } else {
                            $vpsprotocol = $this->vpsprotocol;
                        }
                    }

                    // make your query.
                    $data = array(
                        "Token"             =>  $token,
                        "StoreToken"        =>  "1",
                        "ApplyAVSCV2"       =>  "2",
                        "Apply3DSecure"     =>  "2",
                        "VPSProtocol"       =>  $vpsprotocol,
                        "TxType"            =>  "PAYMENT",
                        "Vendor"            =>  $this->vendor,
                        "VendorTxCode"      =>  $VendorTxCode,
                        "Amount"            =>  urlencode( $this->amount_to_charge ),
                        "Currency"          =>  $order_currency,
                        "Description"       =>   __( 'Repeat payment for subscription', 'woocommerce-gateway-sagepay-form' ) . ' ' . str_replace( '#' , '' , $subscription_id ),                        
                        "BillingSurname"    =>  $order->get_billing_last_name(),
                        "BillingFirstnames" =>  $order->get_billing_first_name(),
                        "BillingAddress1"   =>  $order->get_billing_address_1(),
                        "BillingAddress2"   =>  $order->get_billing_address_2(),
                        "BillingCity"       =>  $order->get_billing_city(),
                        "BillingPostCode"   =>  $this->billing_postcode( $order->get_billing_postcode() ),
                        "BillingCountry"    =>  $order->get_billing_country(),
                        "BillingState"      =>  WC_Sagepay_Common_Functions::sagepay_state( $order->get_billing_country(), $order->get_billing_state()  ),
                        "BillingPhone"      =>  $order->get_billing_phone(),
                        "DeliverySurname"   =>  apply_filters( 'woocommerce_sagepay_direct_deliverysurname', $order->get_shipping_last_name(), $order ),
                        "DeliveryFirstnames"=>  apply_filters( 'woocommerce_sagepay_direct_deliveryfirstname', $order->get_shipping_first_name(), $order ),
                        "DeliveryAddress1"  =>  apply_filters( 'woocommerce_sagepay_direct_deliveryaddress1', $order->get_shipping_address_1(), $order ),
                        "DeliveryAddress2"  =>  apply_filters( 'woocommerce_sagepay_direct_deliveryaddress2', $order->get_shipping_address_2(), $order ),
                        "DeliveryCity"      =>  apply_filters( 'woocommerce_sagepay_direct_deliverycity', $order->get_shipping_city(), $order ),
                        "DeliveryPostCode"  =>  apply_filters( 'woocommerce_sagepay_direct_deliverypostcode', $order->get_shipping_postcode(), $order ),
                        "DeliveryCountry"   =>  apply_filters( 'woocommerce_sagepay_direct_deliverycountry', $order->get_shipping_country(), $order ),
                        "DeliveryState"     =>  apply_filters( 'woocommerce_sagepay_direct_deliverystate', WC_Sagepay_Common_Functions::sagepay_state( $order->get_shipping_country(), $order->get_shipping_state()  ), $order ),
                        "DeliveryPhone"     =>  apply_filters( 'woocommerce_sagepay_direct_deliveryphone', $order->get_billing_phone(), $order ),
                        "CustomerEMail"     =>  $order->get_billing_email(),
                        "AllowGiftAid"      =>  $this->allowgiftaid,
                        "AccountType"       =>  $this->accounttype,
                        "BillingAgreement"  =>  $this->billingagreement,
                        "ReferrerID"        =>  $this->referrerid,
                        "Website"           =>  site_url()
                    );

                    $post_url = $this->purchaseURL;

                    // Protocol 4.00
                    if( $vpsprotocol == '4.00' ) {

                        $data["COFUsage"]               = 'SUBSEQUENT';
                        $data["InitiatedType"]          = 'MIT';
                        $data["MITType"]                = 'UNSCHEDULED';

                        $data["ApplyAVSCV2"]            =  "0";
                        unset( $data["Apply3DSecure"] );
                    }

                } else {
                    // Only used for Subscriptions created without a token in Protocol 3.00
                    $VPSProtocol         = $this->get_order_meta( $previous_renewal_id, $parent_order_id, '_VPSProtocol' );
                    $RelatedVPSTxId      = $this->get_order_meta( $previous_renewal_id, $parent_order_id, '_RelatedVPSTxId' );
                    $RelatedVendorTxCode = $this->get_order_meta( $previous_renewal_id, $parent_order_id, '_RelatedVendorTxCode' );
                    $RelatedSecurityKey  = $this->get_order_meta( $previous_renewal_id, $parent_order_id, '_RelatedSecurityKey' );
                    $RelatedTxAuthNo     = $this->get_order_meta( $previous_renewal_id, $parent_order_id, '_RelatedTxAuthNo' );

                    // New API Request for repeat
                    $data  = array(
                                'VPSProtocol'           => $VPSProtocol,
                                'TxType'                => 'REPEAT',
                                'Vendor'                => urlencode( $this->vendor ),
                                'VendorTxCode'          => $VendorTxCode,
                                'Amount'                => urlencode( $this->amount_to_charge ),
                                'Currency'              => $order_currency,
                                'Description'           => 'Repeat payment for subscription ' . $subscription_id,
                                'RelatedVPSTxId'        => $RelatedVPSTxId,
                                'RelatedVendorTxCode'   => $RelatedVendorTxCode,
                                'RelatedSecurityKey'    => $RelatedSecurityKey,
                                'RelatedTxAuthNo'       => $RelatedTxAuthNo,
                            );

                    if( $VPSProtocol == '4.00' ) {
                        $data["DeliverySurname"]       =  apply_filters( 'woocommerce_sagepay_direct_deliverysurname', $order->get_shipping_last_name(), $order );
                        $data["DeliveryFirstnames"]    =  apply_filters( 'woocommerce_sagepay_direct_deliveryfirstname', $order->get_shipping_first_name(), $order );
                        $data["DeliveryAddress1"]      =  apply_filters( 'woocommerce_sagepay_direct_deliveryaddress1', $order->get_shipping_address_1(), $order );
                        $data["DeliveryAddress2"]      =  apply_filters( 'woocommerce_sagepay_direct_deliveryaddress2', $order->get_shipping_address_2(), $order );
                        $data["DeliveryCity"]          =  apply_filters( 'woocommerce_sagepay_direct_deliverycity', $order->get_shipping_city(), $order );
                        $data["DeliveryPostCode"]      =  apply_filters( 'woocommerce_sagepay_direct_deliverypostcode', $order->get_shipping_postcode(), $order );
                        $data["DeliveryCountry"]       =  apply_filters( 'woocommerce_sagepay_direct_deliverycountry', $order->get_shipping_country(), $order );
                        $data["DeliveryState"]         =  apply_filters( 'woocommerce_sagepay_direct_deliverystate', WC_Sagepay_Common_Functions::sagepay_state( $order->get_shipping_country(), $order->get_shipping_state()  ), $order );
                        $data["DeliveryPhone"]         =  apply_filters( 'woocommerce_sagepay_direct_deliveryphone', $order->get_billing_phone(), $order );
                        $data["COFUsage"]              = 'SUBSEQUENT';
                        $data["InitiatedType"]         = 'MIT';
                        $data["MITType"]               = 'UNSCHEDULED';
                    }

                    $post_url = $this->repeatURL;

                }

                // Add GirftAid to renewal if parent order used GiftAid
                if( $this->giftaid == 'yes' ) {
                    $GiftAidPayment = $this->get_order_meta( $previous_renewal_id, $parent_order_id, '_GiftAidPayment' );
                    if( NULL !== $GiftAidPayment ) {
                        $data['GiftAidPayment'] = $GiftAidPayment;
                    }
                }

                // Add the basket
                $basket = WC_Sagepay_Common_Functions::get_basket( $this->basketoption, $order_id );
                if ( $basket != NULL ) {
                    if ( $this->basketoption == 1 ) {
                        $data["Basket"] = $basket;
                    } elseif ( $this->basketoption == 2 ) {
                        $data["BasketXML"] = $basket;
                    }
                }

                // Remove empty values
                $data = array_filter( $data );

                // Send the request to sage for processing
                $result = $this->sagepay_post( $data, $post_url );

                // Add VendorTxCode to result
                $result['VendorTxCode'] = $VendorTxCode;

                // Process the result
                if ( 'OK' != $result['Status'] ) {
                    // Process renewal failure
                    $this->process_renewal_failure( $result, $order, $order_id, $data );
                } else {
                    // Process renewal success
                    $this->process_renewal_success( $result, $order, $order_id, $parent_order_id, $VendorTxCode );
                }

            }

        } // process scheduled subscription payment
 
        /**
         * [get_previous_renewal_id description]
         * @param  [type] $subscription_id [description]
         * @return [type]                  [description]
         */
        function get_previous_renewal_id( $subscription_id, $parent_order_id ) {

            global $wpdb;
            /**
             * Check for previous renewals for this subscription.
             */         
            $previous_renewals = $wpdb->get_results(  $wpdb->prepare( "
                                        SELECT * FROM {$wpdb->postmeta} pm
                                        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
                                        WHERE pm.meta_key = '%s'
                                        AND pm.meta_value = '%s'
                                        AND p.post_status IN ( '%s','%s' )
                                        ORDER BY pm.post_id DESC
                                        LIMIT 1
                                    ", '_subscription_renewal', $subscription_id, 'wc-processing', 'wc-completed' ) 
            );

            /**
             * $previous_renewal_id is used to get the Sage transaction information from the last successful renewal.
             * 
             * Sage archives orders after 2 years, if we use the transaction information from the first order then 
             * orders will fail once the first order is archived.
             */
            if( isset( $previous_renewals[0]->post_id ) && '' != $previous_renewals[0]->post_id ) {
                $previous_renewal_id = $previous_renewals[0]->post_id;
            } else {
                $previous_renewal_id = $parent_order_id;
            }

            return $previous_renewal_id;

        }

        /**
         * [process_renewal_failure description]
         * @param  [type] $result   [description]
         * @param  [type] $order    [description]
         * @param  [type] $order_id [description]
         * @param  [type] $data     [description]
         * @return [type]           [description]
         */
        function process_renewal_failure( $result, $order, $order_id, $data ) {

            $content = 'There was a problem renewing this payment for order ' . $order_id . '. The Transaction ID is ' . isset( $data['RelatedVPSTxId'] ) ? $data['RelatedVPSTxId'] : '' . '. The API Request is <pre>' . 
                print_r( $data, TRUE ) . '</pre>. SagePay returned the error <pre>' . 
                print_r( $result['StatusDetail'], TRUE ) . '</pre> The full returned array is <pre>' . 
                print_r( $result, TRUE ) . '</pre>. ';
                
            wp_mail( $this->notification ,'SagePay Renewal Error ' . $result['Status'] . ' ' . time(), $content );

            $ordernote = '';
            foreach ( $result as $key => $value ) {
                $ordernote .= $key . ' : ' . $value . "\r\n";
            }

            $order->add_order_note( __('Payment failed', 'woocommerce-gateway-sagepay-form') . '<br />' . $ordernote );

            $order->update_status('failed');

            /**
             * Debugging
             */
            if ( !$this->debug == true ) {
                WC_Sagepay_Common_Functions::sagepay_debug( $data, $this->id, __('Failed Renewal - Opayo Request : ', 'woocommerce-gateway-sagepay-form'), TRUE );
                WC_Sagepay_Common_Functions::sagepay_debug( $result, $this->id, __('Failed Renewal - Opayo Response : ', 'woocommerce-gateway-sagepay-form'), TRUE );
            }

            // WC_Subscriptions_Manager::process_subscription_payment_failure_on_order( $order, NULL );
            // Mark the Subscription as failed.
            $subscriptions = wcs_get_subscriptions_for_order( $order );
            if ( ! empty( $subscriptions ) ) {

                foreach ( $subscriptions as $subscription ) {
                    $subscription->payment_failed();
                }
                
            }

        }

        /**
         * [process_renewal_success description]
         * @param  [type] $result          [description]
         * @param  [type] $order           [description]
         * @param  [type] $order_id        [description]
         * @param  [type] $parent_order_id [description]
         * @param  [type] $VendorTxCode    [description]
         * @return [type]                  [description]
         */
        function process_renewal_success( $result, $order, $order_id, $parent_order_id, $VendorTxCode ) {

            $subscriptions = wcs_get_subscriptions_for_order( $order );
            if ( ! empty( $subscriptions ) ) {
                foreach ( $subscriptions as $subscription ) {
                    $subscription->payment_complete();
                }
                // do_action( 'processed_subscription_payments_for_order', $order );
            }

            $successful_ordernote = '';
            foreach ( $result as $key => $value ) {
                $successful_ordernote .= $key . ' : ' . $value . "\r\n";
            }

            $order->add_order_note( __('Payment completed', 'woocommerce-gateway-sagepay-form') . '<br />' . $successful_ordernote );

            // Update the order with the full Sage result
            $result['VendorTxCode'] = $VendorTxCode;
            $this->set_order_meta( $order_id, NULL, $result );
            
            // WC()->cart->empty_cart();
            $order->payment_complete( str_replace( array('{','}'),'',$result['VPSTxId'] ) );

            do_action( 'woocommerce_sagepay_direct_payment_complete', $result, $order );

        }

        /**
         * [set_order_meta description]
         * @param [type] $order_id        [description]
         * @param [type] $parent_order_id [description]
         * @param [type] $result          [description]
         */
        function set_order_meta( $order_id, $parent_order_id, $result ) {

            // Update the new order
            update_post_meta( $order_id, '_sageresult' , $result );

            // Add all of the info from sage as 
            if( is_array($result) ) {

                if( isset( $result['VPSTxId'] ) ) {
                    $result['VPSTxId'] = str_replace( array('{','}'),'',$result['VPSTxId'] );
                }

                if( isset( $result['Token'] ) ) {
                    $result['SagePayDirectToken'] = $result['Token'];
                    unset( $result['Token'] );
                }

                $result['RelatedVPSTxId']       = isset( $result['VPSTxId'] ) ? str_replace( array('{','}'),'',$result['VPSTxId'] ) : NULL;
                $result['RelatedSecurityKey']   = isset( $result['SecurityKey'] ) ? $result['SecurityKey'] : NULL;
                $result['RelatedTxAuthNo']      = isset( $result['TxAuthNo'] ) ? $result['TxAuthNo'] : NULL;
                $result['RelatedVendorTxCode']  = isset( $result['VendorTxCode'] ) ? $result['VendorTxCode'] : NULL;

                foreach ( $result as $key => $value ) {
                    update_post_meta( $order_id, '_'.$key , $value );
                }

                // Update the parent order if necessary
                if( NULL != $parent_order_id ) {
                    // Set the related order meta for new order and parent order
                    update_post_meta( $parent_order_id, '_RelatedVPSTxId' , str_replace( array('{','}'),'',$result['VPSTxId'] ) );
                    update_post_meta( $parent_order_id, '_RelatedSecurityKey' , $result['SecurityKey'] );
                    update_post_meta( $parent_order_id, '_RelatedTxAuthNo' , $result['TxAuthNo'] );
                    update_post_meta( $parent_order_id, '_RelatedVendorTxCode' , $result['VendorTxCode'] );
                }

            }

        }

        /**
         * [get_order_meta description]
         * Get post meta.
         * @param  [type] $previous_renewal_id [description]
         * @param  [type] $parent_order_id     [description]
         * @param  [type] $meta_key            [description]
         * @return [type]                      [description]
         */
        function get_order_meta( $previous_renewal_id, $parent_order_id, $meta_key ) {

            if( NULL != get_post_meta( $previous_renewal_id, $meta_key, TRUE ) ) {
                return get_post_meta( $previous_renewal_id, $meta_key, TRUE );
            }

            if( NULL != get_post_meta( $parent_order_id, $meta_key, TRUE ) ) {
                return get_post_meta( $parent_order_id, $meta_key, TRUE );
            }

            return NULL;

        }

        /**
         * [get_token description]
         * @param  [type] $previous_renewal_id [description]
         * @param  [type] $subscription_id     [description]
         * @param  [type] $meta_key            [description]
         * @return [type]                      [description]
         */
        function get_token( $previous_renewal_id, $subscription_id, $meta_key ) {

            // Subscription should have most up to date token value if payent method has been changed
            if( NULL != get_post_meta( $subscription_id, $meta_key, TRUE ) ) {
                return get_post_meta( $subscription_id, $meta_key, TRUE );
            }

            // Previous order will have last used token
            if( NULL != get_post_meta( $previous_renewal_id, $meta_key, TRUE ) ) {
                return get_post_meta( $previous_renewal_id, $meta_key, TRUE );
            }

            return NULL;
        }

        /**
         * [get_vpsprotocol description]
         * @param  [type] $previous_renewal_id [description]
         * @param  [type] $subscription_id     [description]
         * @param  [type] $meta_key            [description]
         * @return [type]                      [description]
         */
        function get_vpsprotocol( $previous_renewal_id, $subscription_id, $meta_key ) {

            // Subscription should have most up to date token value if payent method has been changed
            if( NULL != get_post_meta( $subscription_id, $meta_key, TRUE ) ) {
                return get_post_meta( $subscription_id, $meta_key, TRUE );
            }

            if( NULL != get_post_meta( $previous_renewal_id, $meta_key, TRUE ) ) {
                return get_post_meta( $previous_renewal_id, $meta_key, TRUE );
            }

            return NULL;

        }

        /**
         * [get_SchemeTraceID description]
         * @param  [type] $parent_order_id [description]
         * @return [type]                  [description]
         */
        function get_SchemeTraceID( $parent_order_id ) {

            $SchemeTraceID = get_post_meta( $parent_order_id, '_SchemeTraceID', TRUE );

            if( isset($SchemeTraceID) && $SchemeTraceID != '' ) {
                return $SchemeTraceID;
            }
            
            return 'SP999999999';

        }

    }
