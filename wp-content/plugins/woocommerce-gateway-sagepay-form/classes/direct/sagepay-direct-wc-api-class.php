<?php
    /**
     * Process Response for SagePay Direct
     */
    class Sagepay_Direct_Api extends WC_Gateway_Sagepay_Direct {

        public function __construct() {

            parent::__construct();

        }

        function process_api() {
            global $wpdb;

            $settings   = get_option( 'woocommerce_sagepaydirect_settings' );

            // Drop out if SagePay Direct is not enabled
            if( !isset( $settings['enabled'] ) || $settings['enabled'] != "yes" ) {
                wp_die( "Sage Request Failure<br />" . 'Access denied', "Sage Failure", array( 'response' => 200 ) );
                exit;
            }

            if ( isset( $_GET["vtx"] ) ) {

                $vtx = wc_clean( $_GET["vtx"] );

                // Check if we have created an invoice before this order
                $stored_value = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key = '_VendorTxCode' AND meta_value = %s;", $vtx ) );

                if ( null !== $stored_value ) {

                    $order_id   = $stored_value->post_id;
                    $order      = wc_get_order( $order_id );

                    // PayPal
                    if( isset( $settings['enabled'] ) && isset( $settings['cardtypes'] ) && $settings['enabled'] == "yes" && ( $key = array_search('PayPal', $settings['cardtypes']) ) !== false ) {

                        $sageresult = $this->process_response( $_POST, $order );

                        if( in_array( $_POST['Status'], array( 'OK', 'PAYPALOK' ) ) ) {
                            $redirect = $this->get_return_url( $order );
                        } else {
                            $redirect = $sageresult['redirect'];
                        }

                        wp_redirect( $redirect );
                        exit;

                    } else {
                        wp_die( "Sage Request Failure<br />" . 'Check the WooCommerce SagePay Settings for error messages', "Sage Failure", array( 'response' => 200 ) );
                    }

                } else {
                    wp_die( "Sage Request Failure<br />" . 'Check the WooCommerce SagePay Settings for error messages', "Sage Failure", array( 'response' => 200 ) );
                }
                
            } else {
                wp_die( "Sage Request Failure<br />" . 'Check the WooCommerce SagePay Settings for error messages', "Sage Failure", array( 'response' => 200 ) );
            }          

        }

    } // End class
