<?php
/**
* 3D Secure for SagePay Direct
*/
class Sagepay_Direct_3DSecure_4 extends WC_Gateway_Sagepay_Direct {

    private $order_id;

    public function __construct( $order_id ) {

    parent::__construct();

        $this->order_id   = $order_id;
        $this->settings   = get_option( 'woocommerce_sagepaydirect_settings' );

    }

    function authorise() {
        global $woocommerce;

        // woocommerce order instance
        $order_id = $this->order_id;
        $order    = wc_get_order( $order_id );


        // Delete _opayo_callback_value
        delete_post_meta( $order_id, '_opayo_callback_value' );

        $sage_3dsecure  = WC()->session->get( "sage_3ds" );

        if( !isset( $sage_3dsecure['Status'] ) ) {
            $sage_3dsecure  = get_post_meta( $order_id, '_sage_3ds', TRUE );
        }

        $key      = 'CRes';
        $value    = '';

        $redirect_url = $this->get_return_url( $order );

        // Check for PARes or CRes and check for _opayo_callback_value
        // _opayo_callback_value is used to make sure this part can not be repeated if customer refreshes page or uses back button
        if ( ( isset($_POST['PARes']) || isset($_POST['PaRes']) || isset($_POST['cres']) || isset($_POST['CRes']) ) ) {

            if( !get_post_meta( $order_id, '_opayo_callback_value', TRUE ) ) {

                try {

                    // set the URL that will be posted to.
                    $url = $this->callbackURL;

                    if( ( isset( $_POST['CRes'] ) && $_POST['CRes'] != '' ) || ( isset( $_POST['cres'] ) && $_POST['cres'] != '' ) ) {

                        $key = 'CRes';

                        if( isset($_POST['CRes']) ) {
                            $value = $_POST['CRes'];
                        } else {
                            $value = $_POST['cres'];
                        }

                        // Store the CRes value
                        // update_post_meta( $order_id, '_opayo_callback_value', $value );

                        // Set the data for Sage
                        $data      = array(
                            $key => $value
                        );

                        $data['VPSTxId'] = isset( $sage_3dsecure['VPSTxId'] ) ? $sage_3dsecure['VPSTxId'] : $_POST['VPSTxId'];

                    }

                    // Fallback for 3DS 1.0
                    if( ( isset( $_POST['PARes'] ) && $_POST['PARes'] != '' ) || ( isset( $_POST['PaRes'] ) && $_POST['PaRes'] != '' ) ) {

                        $key = 'PARes';

                        if( isset($_POST['PARes']) ) {
                            $value = $_POST['PARes'];
                        } else {
                            $value = $_POST['PaRes'];
                        }

                        // Store the PARes value
                        // update_post_meta( $order_id, '_opayo_callback_value', $value );

                        // Set the data for Opayo
                        $data      = array(
                            $key => $value
                        );
                        
                        $data['MD'] = isset( $sage_3dsecure['MD'] ) ? $sage_3dsecure['MD'] : $_POST['MD'];

                    }

                    /**
                     * Send $data to Opayo
                     * @var [type]
                    */
                    $sageresult = $this->sagepay_post( $data, $url );

                    if( isset( $sage_3dsecure['change_payment_method'] ) && class_exists( 'WC_Subscriptions' ) ) {

                        WC()->session->set( "sage_3ds", "" );
                        delete_post_meta( $order_id, '_sage_3ds' );

                        // Move the Opayo Result Status to Status to OpayoStatus
                        $sageresult['OpayoStatus']  = $sageresult['Status'];

                        // Set Status to CHANGEPAYMENTMETHODNEWTOKEN so we can process this as a new token payment method change
                        $sageresult['Status']       = 'CHANGEPAYMENTMETHODNEWTOKEN';

                        // Procee the result in Sagepay Direct Response class
                        $this->process_response( $sageresult, $order );
                        // Stops here.
                    }

                    if( isset( $sageresult['Status']) && $sageresult['Status']!= '' ) {

                        // Successful 3D Secure
                        $sageresult   = $this->process_response( $sageresult, $order );
                        $redirect_url = isset( $sageresult['redirect'] ) ? $sageresult['redirect'] : $this->get_return_url( $order );

                    } else {

                        // No status from Opayo
                        if ( $order->needs_payment() ) {
                            $redirect_url = wc_get_page_permalink('checkout');
                            throw new Exception( __('Payment error, please try again. Your card has not been charged.' , 'woocommerce_sagepayform' ) );
                        } else {
                            // Redirect customer to thank you page
                            $redirect_url = $order->get_checkout_order_received_url();
                        }

                    }

                } catch( Exception $e ) {
                    $this->sagepay_message( $e->getMessage(), 'error', $order_id );
                }

                unset( $_POST['CRes'] );
                unset( $_POST['PARes'] );

                WC()->session->set( "sage_3ds", "" );
                delete_post_meta( $order_id, '_sage_3ds' );

                wp_redirect( $redirect_url );
                exit;
                

            } elseif ( get_post_meta( $order_id, '_opayo_callback_value', TRUE ) ) {

                // Delete _opayo_callback_value
                delete_post_meta( $order_id, '_opayo_callback_value' );

                // This transaction has already generated a CRes or PARes so we don't want to go again!
                if ( $order->needs_payment() ) {
                    $this->sagepay_message( __('Payment error, please try again. Your card has not been charged.' , 'woocommerce_sagepayform' ), 'error', $order_id );
                    wp_redirect( wc_get_page_permalink('checkout') );
                    exit;
                } else {
                    // Redirect customer to thank you page
                    wp_redirect( $order->get_checkout_order_received_url() );
                    exit;
                }

            }

        }

        // Get ready to set form fields for 3DS 1.0/2.0
        $p = $this->pareq_or_creq ( $sage_3dsecure );
        $m = $this->md_or_vpstxid ( $sage_3dsecure );

        // Log data sent for 3DS
        if ( $this->debug == true || $this->status != 'live' ) {
            $log_array = array( 
                    'ACSURL'            => $sage_3dsecure['ACSURL'],
                    $p['field_name']    => $p['field_value'],
                    $m['field_name']    => $m['field_value'],
                    'TermURL'           => $sage_3dsecure['TermURL']
                );
            WC_Sagepay_Common_Functions::sagepay_debug( $log_array, $this->id, __('3D Secure form data : ', 'woocommerce-gateway-sagepay-form'), TRUE );
        }

        // Non-iFrame Method
        $form  = '<form id="submitForm" method="post" action="' . $sage_3dsecure['ACSURL'] . '">';
        $form .= '<input type="hidden" name="' . $p['field_name'] . '" value="' . $p['field_value'] . '"/>';
        $form .= '<input type="hidden" name="' . $m['field_name'] . '" value="' . $m['field_value'] . '"/>';
        $form .= '<input type="hidden" id="termUrl" name="TermUrl" value="' . $sage_3dsecure['TermURL'] . '"/>';
        $form .= '<script>';
        $form .= 'document.getElementById("submitForm").submit();';
        $form .= '</script>';
        $form .= '<noscript><p>Authenticate your card</p><p><input type="submit" value="Submit"></p></noscript>';
        $form .= '</form>';

        // Log form sent for 3DS
        if ( $this->debug == true || $this->status != 'live' ) {
            WC_Sagepay_Common_Functions::sagepay_debug( $form, $this->id, __('3D Secure form data : ', 'woocommerce-gateway-sagepay-form'), TRUE );
        }

        echo $form;
        exit;

    }

    function pareq_or_creq ( $sage_3dsecure ) {

        // Get ready to set form fields for 3DS 1.0/2.0
        if( isset( $sage_3dsecure['PAReq'] ) ) {
            $p = array(
                "field_name"    => "PaReq",
                "field_value"   => $sage_3dsecure['PAReq']
            );
        } else {
            $p = array(
                "field_name"    => "creq",
                "field_value"   => $sage_3dsecure['CReq']
            );
        }

        return $p;

    }

    function md_or_vpstxid ( $sage_3dsecure ) {

        if( isset( $sage_3dsecure['MD'] ) ) {
            $m = array(
                "field_name" => "MD",
                "field_value" => $sage_3dsecure['MD']
            );
        } else {
            $m = array(
                "field_name" => "VPSTxId",
                "field_value" => $sage_3dsecure['VPSTxId']
            );
        }

        return $m;
    }

} // End class
