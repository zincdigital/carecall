<?php
    /**
     * Need to load wp-load.php so that we can use all of the
     * WordPress / WooCommerce / Subscriptions functions
     */
    $parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
    require_once( $parse_uri[0] . 'wp-load.php' );

    $sage_3dsecure = WC()->session->get( "sage_3ds" );

    if( isset( $sage_3dsecure['PAReq'] ) ) {
        $p = array(
                    "field_name" => "PaReq",
                    "field_value" => $sage_3dsecure['PAReq']
                );
    } else {
        $p = array(
                    "field_name" => "creq",
                    "field_value" => $sage_3dsecure['CReq']
                );
    }

    if( isset( $sage_3dsecure['MD'] ) ) {
        $m = array(
                    "field_name" => "MD",
                    "field_value" => $sage_3dsecure['MD']
                );
    } else {
        $m = array(
                    "field_name" => "VPSTxId",
                    "field_value" => $sage_3dsecure['VPSTxId']
                );
    }

    $form  = '<form name="form" action="' . $sage_3dsecure['ACSURL'] . '" method="POST" >' .
             '<input type="hidden" name="' . $p['field_name'] . '" value="' . $p['field_value'] . '"/>' .
             '<input type="hidden" name="' . $m['field_name'] . '" value="' . $m['field_value'] . '"/>' .
             '<input type="hidden" name="TermUrl" value="' . $sage_3dsecure['TermURL'] . '"/>' .
             '<noscript>' .
             '<center><p>Please click button below to Authenticate your card</p><input type="submit" value="Go"/></p></center>' .
             '</noscript>' .
             '</form>';

    $page  = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">' .
             '<html><head>' .
             '<script type="text/javascript"> function OnLoadEvent() { document.form.submit(); }</script>' .
             '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . 
             '<title>3D-Secure Redirect</title>' .             
             '</head>' . 
             '<body OnLoad="OnLoadEvent();">';
    if( isset(  $_SESSION['msg'] ) ) {
        $page .= '<div style="text-align: center; font-family: sans-serif; font-size: 2em; line-height: 1.5; color: #777;" >' . $_SESSION['msg'] . '</div>';
    }

    $page .= $form;

    $page .= '</body></html>';

    echo $page;