<?php
    /**
     * Need to load wp-load.php so that we can use all of the
     * WordPress / WooCommerce / Subscriptions functions
     */
    $parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
    require_once( $parse_uri[0] . 'wp-load.php' );

    $sage_3dsecure = WC()->session->get( "sage_3ds" );

    // alphanumeric unlimited length base64 encoded    
    if ( base64_decode( @$_REQUEST['PaRes'], true ) ) {
        $pares = $_REQUEST['PaRes'];
    } else {
        $pares = "";
    }

    // alphanumeric unlimited length base64 encoded    
    if ( base64_decode( @$_REQUEST['cres'], true ) ) {
        $cres = $_REQUEST['cres'];
    } else {
        $cres = "";
    }

    if( isset( $sage_3dsecure['PAReq'] ) ) {
        $p = array(
                    "field_name" => "PaReq",
                    "field_value" => $sage_3dsecure['PAReq']
                );
    } else {
        $p = array(
                    "field_name" => "creq",
                    "field_value" => $sage_3dsecure['CReq']
                );
    }

    if( isset( $sage_3dsecure['MD'] ) ) {
        $m = array(
                    "field_name" => "MD",
                    "field_value" => $sage_3dsecure['MD']
                );
    } else {
        $m = array(
                    "field_name" => "VPSTxId",
                    "field_value" => $sage_3dsecure['VPSTxId']
                );
    }

    if( isset( $_POST['cres'] ) ) {
        $c = array(
                    "field_name" => "CRes",
                    "field_value" => $cres
                );
    } else {
        $c = array(
                    "field_name" => "PARes",
                    "field_value" => $pares
                );
    }
    
    $page = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">' .
            '<html><head>' .
            '<script type="text/javascript"> function OnLoadEvent() { document.form.submit(); }</script>' .
            '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />' .
            '<title>3D-Secure Callback</title></head>' .
            '<body OnLoad="OnLoadEvent();">' .
            '<form name="form" action="'. $sage_3dsecure['Complete3d'] .'" method="POST"  target="_top" >' .
            '<input type="hidden" name="' . $c['field_name'] . '" value="' . $c['field_value'] . '"/>' .
            '<input type="hidden" name="' . $m['field_name'] . '" value="' . $m['field_value'] . '"/>' .
            '<input type="hidden" name="VendorTxCode" value="' . $sage_3dsecure['VendorTxCode'] . '"/>' .
            '<noscript>' .
            '<center><p>Please click button below to Authenticate your card</p><input type="submit" value="Go"/></p></center>' .
            '</noscript>' .
            '</form></body></html>';
    
    echo $page;
?>
