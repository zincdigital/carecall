(function ($) {

	"use strict";

	$(document).ready(function () {

		$(".products.columns-2").each(function () {
			$("li", this).each(function (i) {
				$(this).prepend("<span class='count'>" + (i + 1) + "</span>");
			});
		});

		// Comments
		$(".commentlist li").addClass("panel panel-default");
		$(".comment-reply-link").addClass("btn btn-default");

		// Forms
		$('select, input[type=text], input[type=email], input[type=password], textarea').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');

		$("input, textarea").each(function () {
			$(this).data('holder', $(this).attr('placeholder'));

			$(this).focusin(function () {
				$(this).attr('placeholder', '');
			});

			$(this).focusout(function () {
				$(this).attr('placeholder', $(this).data('holder'));
			});
		});

		// Change text of button element
		jQuery(".post-type-archive-product .product_type_subscription").text("Add to Basket");
		jQuery(".page-template-page-cart .product_type_subscription").text("Add to Basket");

	});

	$("a").focus(function () {
		$(this).blur();
	});

	$('#testimonials').owlCarousel({
		loop: true,
		autoplay: false,
		autoplaySpeed: 2000,
		margin: 0,
		nav: true,
		dots: false,
		slideBy: 1,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			1000: {
				items: 1
			}
		}
	});
	$('#posts').owlCarousel({
		loop: true,
		autoplay: false,
		autoplaySpeed: 2000,
		margin: 0,
		nav: true,
		dots: false,
		slideBy: 1,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			1000: {
				items: 1
			}
		}
	});



}(jQuery));


window.addEventListener("load", function () {
	jQuery('.link').click(function (e) {
		e.preventDefault();
		var target = jQuery(this).attr('href');
		jQuery('html, body').animate({
			scrollTop: jQuery(target).offset().top - 100
		}, 1000)
	});
});

window.addEventListener("load", function () {
	jQuery('.click a').click(function (e) {
		alert('hello');
		e.preventDefault();
		var target = jQuery(this).attr('href');
		jQuery('html, body').animate({
			scrollTop: jQuery(target).offset().top - 100
		}, 1000)
	});
});