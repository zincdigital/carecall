<?php $args = array('orderby' => 'publish_date', 'order' => 'DESC', 'posts_per_page' => '3') ;?>
<?php $the_query = new WP_Query( $args ); ?>
<?php if ( $the_query->have_posts() ) : ?>
  <div class="row posts">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <div class="col-lg-4">
        <div class="inner-text">
          <a href="<?php the_permalink() ?>"><img src="<?php echo get_the_post_thumbnail_url(); ?>" /></a>
          <p class="title"><?php echo the_title(); ?></p>
          <a href="<?php the_permalink() ?>" class="text"><?php
          $char_limit = 175; //character limit
          $content = $post->post_content; //contents saved in a variable
          echo substr(strip_tags($content), 0, $char_limit);
          ?>...</a>
          <a href="<?php the_permalink()?>" class="btn multi">Read More</a>
        </div>
      </div>
    <?php endwhile;?>
  </div>
<?php endif;?>


<?php wp_reset_postdata(); ?>
