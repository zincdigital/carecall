
<?php $args = array('category_name'  => 'featured', 'orderby' => 'publish_date', 'order' => 'ASC') ;?>
<?php $the_query = new WP_Query( $args ); ?>
<?php if ( $the_query->have_posts() ) : ?>
  <div class="owl-carousel" id="posts">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <div class="container">
      <div class="row">
        <div class="order-2 order-lg-1 col-lg-6">
          <div class="post">
            <p class="title"><?php echo the_title(); ?></p>
            <!-- <p><?//php echo get_the_date(); ?></p> -->
            <a href="<?php the_permalink() ?>" class="text"><?php
            $char_limit = 200; //character limit
            $content = $post->post_content; //contents saved in a variable
            echo substr(strip_tags($content), 0, $char_limit);
            ?>...</a>
            <a href="<?php the_permalink()?>" class="btn multi">Read More</a>
          </div>
        </div>
        <div class="order-1 order-lg-2 col-lg-6">
          <div class="post-image">
            <p><a href="<?php the_permalink() ?>"><?php the_post_thumbnail(); ?></a></p>
          </div>
        </div>
      </div>
    </div>
    <?php endwhile;?>


  <?php endif;?>


  <?php wp_reset_postdata(); ?>
