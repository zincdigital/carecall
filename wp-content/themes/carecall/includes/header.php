<!DOCTYPE html>
<html class="no-js">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NQVKM4J');</script>
<!-- End Google Tag Manager -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-100330937-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-100330937-2');
	</script>
	<title><?php wp_title('•', true, 'right');?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php wp_head(); ?>

	<script>
	document.addEventListener( 'wpcf7mailsent', function( event ) {
		location = '<?php echo home_url(); ?>/contact-thanks';
	}, false );
</script>

<script src="https://kit.fontawesome.com/caa1aef6e7.js"></script>


<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700&display=swap" rel="stylesheet">


</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQVKM4J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<!--[if lt IE 8]>
	<div class="alert alert-warning">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->


<header id="header">
	<div class="container">
		<!-- Fixed navbar -->
		<div class="top-nav">
			<ul>
				<!-- <li class="search"><a href="#">Search</a></li> -->
				<li class="basket"><a href="/basket">Basket <span class="header-cart-count"></span></a></li>
				<!-- <li class="account"><a href="/my-account">Account</a></li> -->
			</ul>
		</div>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark">
			<?php the_custom_logo( $blog_id = 0 ); ?>


			<p class="cta order-2 order-lg-1">We’re on hand to help, call us today on: <a href="tel:0300 303 2682">0300 303 2682</a></p>
			<div class="col-1 d-md-none d-sm-block">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
			<div class="order-1 order-lg-1 collapse navbar-collapse" id="navbarCollapse">
				<?php
				wp_nav_menu( array(
					'theme_location'    => 'navbar-top',
					'depth'             => 2,
					'menu_class'        => 'nav navbar-nav',
					'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
					'walker'            => new wp_bootstrap_navwalker())
				);
				?>
			</div>

		</nav>
	</div>
</header>
