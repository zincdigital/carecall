<?php
/*
The Page Loop
=============
*/
?>

<!--  Page Banners -->
<?php $banner = get_field('banner'); ?>
<div id="banner">
  <?php echo $banner;?>
</div>


<!-- Begin page content -->
<main role="main">

  <?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
  <div class="container">
    <div class="row intro">
      <?php $columns = get_sub_field('columns'); $title = get_sub_field('title'); $textone = get_sub_field('text-1'); $texttwo = get_sub_field('text-2'); $link = get_sub_field('link'); $linktitle = get_sub_field('link-title'); ?>
      <?php if($columns == '1') :?>
      <div class="col-lg-12">
        <h2 class="title"><?=$title?></h2>
        <?=$textone?>
        <p><a href="<?=$link?>" class="btn multi"><?=$linktitle?></a></p>
      </div>
      <?php elseif ($columns == '2') : ?>
      <div class="col-lg-6">
        <h2 class="title"><?=$title?></h2>
        <?=$textone?>
        <p><a href="<?=$link?>" class="btn multi"><?=$linktitle?></a></p>
      </div>
      <div class="col-lg-6">
        <?=$texttwo?>
      </div>
      <?php endif;?>
      <?php endwhile;?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (is_page('About Us')) : ?>
  <?php if( have_rows('content-block-repeat') ): while ( have_rows('content-block-repeat') ) : the_row(); ?>
  <?php $columns = get_sub_field('columns'); $background = get_sub_field('background'); $title = get_sub_field('title-1'); $textone = get_sub_field('text-1'); $texttwo = get_sub_field('text-2'); ?>
  <?php if($columns == '1') :?>
  <div class="block-content full" style="background-color: <?php echo $background ?>;">
    <div class="container">
      <div class="col-lg-12">
        <h2 class="title"><?=$title?></h2>
        <?=$textone?>
      </div>
    </div>
  </div>
  <?php elseif ($columns == '2') : ?>
  <div class="block-content two-col" style="background-color: <?php echo $background ?>;">
    <div class="container">
      <div class="col-lg-12">
        <h2 class="title"><?=$title?></h2>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <?=$textone?>
        </div>
        <div class="col-lg-6">
          <?=$texttwo?>
        </div>
      </div>
    </div>
  </div>
  <?php endif;?>
  <?php endwhile;?>
  </div>
  </div>
  <?php endif; ?>

  <div class="container" id="cases">
    <p class="title text-center">Latest Case Studies</p>
    <div class="row">
      <div class="col-lg-12">
        <?php get_template_part('includes/partials/posts'); ?>
      </div>
    </div>
  </div>
  <?php elseif (is_page('FAQs') ) : ?>
  <div class="block-content grey">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <?php $content = get_field('content-block'); ?>
          <?php echo $content;?>
        </div>
      </div>
    </div>
  </div>
  <div class="testimonials">
    <div class="owl-carousel" id="testimonials"
      style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/quotes.png') no-repeat; background-size: 868px; background-position: center !important; height: 315px; ">
      <?php if( have_rows('testimonials') ): while ( have_rows('testimonials') ) : the_row(); ?>
      <?php $title = get_sub_field('title'); $text = get_sub_field('text'); $author = get_sub_field('author'); $background = get_sub_field('background-image');?>
      <div class="item">
        <div class="inner-item">
          <p class="title"><?=$title?></p>
          <p><?=$text?></p>
        </div>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
  <?php elseif (is_page('Careers') ) : ?>

  <div class="block-content white">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <?php $benefits = get_field('staff-benefits'); ?>
          <?php echo $benefits;?>
        </div>
      </div>
    </div>
  </div>
  <?php elseif (is_page('Contact Us') ) : ?>
  <div class="container">
    <?php if( have_rows('contact-block') ): while ( have_rows('contact-block') ) : the_row(); ?>
    <div class="row">
      <?php $column1 = get_sub_field('column-1'); $column2 = get_sub_field('column-2'); $column3 = get_sub_field('column-3'); $text = get_sub_field('text-block');  ?>
      <div class="col-lg-6 column text-center">
        <?php echo $column1;?>
      </div>
      <div class="col-lg-6 column text-center">
        <?php echo $column2;?>
      </div>
      <div class="col-lg-12 text-block">
        <?php echo $text;?>
      </div>
      <?php endwhile;?>
    </div>
    <?php endif;?>
  </div>
  <div class="block-content grey contact-form">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <?php $content = get_field('content-block'); ?>
          <?php echo $content;?>
        </div>
      </div>
    </div>

  </div>
  <div class="container">
    <h2 class="title text-center" style="margin-top: 40px; float: left; width: 100%;">Customer Services</h2>
    <p class="text-center">Got a question, compliment, comment or complaint? Contact our Customer Service Team.<br /> Our Customer Service Team operates Monday to Friday, 9am to 5pm and aim to respond to all queries within 3 working days.</p>
    <?php if( have_rows('contact-block') ): while ( have_rows('contact-block') ) : the_row(); ?>
    <div class="row">
      <?php $column1 = get_sub_field('column-1'); $column2 = get_sub_field('column-2'); $column3 = get_sub_field('column-3'); $text = get_sub_field('text-block');  ?>
      <div class="col-lg-12 column text-left">
        <?php echo $column1;?>
      </div>
      <div class="col-lg-12 column text-left">
        <?php echo $column2;?>
      </div>
      <div class="col-lg-12 column text-left">
        <?php echo $column3;?>
      </div>
      <?php endwhile;?>
    </div>
    <?php endif;?>
    <hr />
  </div>
  <?php elseif (is_page('Terms & Conditions')) : ?>
  <div class="container pd-btm-60 page-content">
    <div class="row">
      <div class="col-lg-12 text-center">
        <?php $content = get_field('content-block'); ?>
        <?php echo $content;?>
      </div>
    </div>
  </div>
  <?php elseif (is_page('Case Studies')) : ?>
  <div class="container" id="cases">
    <div class="row">
      <div class="col-lg-12">
        <?php get_template_part('includes/partials/cases'); ?>
      </div>
    </div>
  </div>
  <?php endif;?>


  <?php if (is_page('Privacy Policy')) : ?>
  <?php if( have_rows('content-block-repeat') ): while ( have_rows('content-block-repeat') ) : the_row(); ?>
  <?php $columns = get_sub_field('columns'); $background = get_sub_field('background'); $title = get_sub_field('title-1'); $textone = get_sub_field('text-1'); $texttwo = get_sub_field('text-2'); ?>
  <?php if($columns == '1') :?>
  <div class="block-content full" style="background-color: <?php echo $background ?>;">
    <div class="container">
      <div class="col-lg-12">
        <h2 class="title"><?=$title?></h2>
        <?=$textone?>
      </div>
    </div>
  </div>
  <?php elseif ($columns == '2') : ?>
  <div class="block-content two-col" style="background-color: <?php echo $background ?>;">
    <div class="container">
      <div class="col-lg-12">
        <h2 class="title"><?=$title?></h2>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <?=$textone?>
        </div>
        <div class="col-lg-6">
          <?=$texttwo?>
        </div>
      </div>
    </div>
  </div>
  <?php endif;?>
  <?php endwhile;?>
  <?php endif; ?>
  </div>
  </div>
  <?php endif; ?>


  <?php if (is_page('About Us') ) : ?>
  <div class="call-to-action purple">
    <?php $calltext = get_field('call-text'); $calllink = get_field('call-link'); ?>
    <div class="container">
      <p class="link"><a href="<?=$calllink?>" class="btn white">Case Studies</a></p>
      <p><?php echo $calltext?></p>
    </div>
  </div>
  <?php get_template_part('includes/partials/case-studies'); ?>
  <?php endif;?>

</main>