<?php
/*
The Single Posts Loop
=====================
*/
?>

<div class="container post-detail">
  <div class="row">
    <?php if(have_posts()): while(have_posts()): the_post(); ?>
      <div class="col-lg-6 text">
        <h1 class="title"><?php the_title()?></h1>
        <?php the_content()?>
      </div>
      <div class="col-lg-6 image">
        <?php the_post_thumbnail(); ?>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
</div>
</div>
