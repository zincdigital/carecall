
<!--  Banner -->
<?php $banner = get_field('banner'); ?>
<div id="banner">
  <?php echo $banner;?>
</div>

<div class="usp">
  <div class="container">
    <div class="row">
      <?php if( have_rows('block') ): while( have_rows('block') ): the_row(); ?>
        <?php $title = get_sub_field('title'); $text = get_sub_field('text'); $class = get_sub_field('class');?>
        <div class="col-md-4">
          <div class="block <?=$class?>">
            <p class="title"><?=$title?></p>
            <p><?=$text?></p>
          </div>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>
  </div>
</div>
</div>

<!-- Begin page content -->
<main role="main">

  <?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
    <div class="container">
      <div class="row intro">
        <?php $columns = get_sub_field('columns'); $title = get_sub_field('title'); $textone = get_sub_field('text-1'); $texttwo = get_sub_field('text-2'); $link = get_sub_field('link'); $linktitle = get_sub_field('link-title');?>
        <?php if($columns == '1') :?>
          <div class="col-lg-12">
            <h1 class="title"><?=$title?></h1>
            <?=$textone?>
            <p><a href="<?=$link?>" class="btn multi"><?=$linktitle?></a></p>
          </div>
        <?php elseif ($columns == '2') : ?>
          <div class="order-2 order-lg-1 col-lg-6">
            <h1 class="title"><?=$title?></h1>
            <?=$textone?>
            <p><a href="<?=$link?>" class="btn multi"><?=$linktitle?></a></p>
          </div>
          <div class="order-1 order-lg-1 col-lg-6">
            <?=$texttwo?>
          </div>
        <?php endif;?>
      <?php endwhile;?>
    </div>
  </div>
<?php endif; ?>

<!-- <div class="plans">
  <div class="container">
    <div class="row">
      <p>build plans section</p>
    </div>
  </div>
</div> -->

<div class="case-studies">
  <div class="row">
    <div class="col-lg-12 col-xl-12">
      <?php get_template_part('includes/partials/featured-posts'); ?>
    </div>
  </div>
</div>

<div class="processes-wrapper">
  <div class="container">
    <div class="row"><div class="col-lg-12 col-xl-12"><h2 class="title">Our Simple 3 Step Process</h2></div></div>
    <div class="row">
      <div class="col-12 processes">
        <?php if( have_rows('steps') ): while ( have_rows('steps') ) : the_row(); ?>
          <?php $title = get_sub_field('title'); $text = get_sub_field('text'); $image = get_sub_field('image'); ?>
          <div class="box">
            <div class="step">
              <p><img src="<?= $image['url']; ?>" /></p>
              <div class="inner-step">
                <p class="title"><?=$title?></p>
                <p><?=$text?></p>
              </div>
            </div>
          </div>
        <?php endwhile;?>
      <?php endif; ?>
    </div>
  </div>
</div>
</div>

<div class="call-to-action">
  <?php if(have_posts()): while(have_posts()): the_post(); ?>
    <?php $calltext = get_field('call-text'); $calllink = get_field('call-link'); ?>
    <div class="container">
      <p class="link"><a href="<?=$link?>" class="btn white">Contact Us</a></p>
      <p><?php echo $calltext?></p>
    </div>
  <?php endwhile; ?>
<?php endif; ?>
</div>

<div class="testimonials">
  <div class="owl-carousel" id="testimonials" style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/quotes.png') no-repeat; background-size: 868px; background-position: center !important; height: 315px; ">
    <?php if( have_rows('testimonials') ): while ( have_rows('testimonials') ) : the_row(); ?>
      <?php $title = get_sub_field('title'); $text = get_sub_field('text'); $author = get_sub_field('author'); $background = get_sub_field('background-image');?>
      <div class="item">
        <div class="inner-item">
          <p class="title"><?=$title?></p>
          <p><?=$text?></p>
          <p class="author"><?=$author?></p>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
</div>
</div>


</main>
