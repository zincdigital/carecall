<div class="bottom-logos">
	<ul>
		<li><a href="https://www.providesupportedliving.co.uk/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/supported-living.png" alt="Supported Living"></a></li>
		<li><a href="https://www.carecall247.co.uk/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/carecall.png" alt="Carecall247"></a></li>
		<li><a href="https://www.providesupportathome.co.uk/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/support-at-home.png" alt="Supported At Home"></a></li>
		<li><a href="https://www.provide.org.uk/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/provide.png" alt="Provide"></a></li>
	</ul>
</div>

<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3>Navigation</h3>
				<?php dynamic_sidebar('footer-widget-column-1'); ?>
			</div>
			<div class="col-md-3">
				<h3>Plans</h3>
				<?php dynamic_sidebar('footer-widget-column-2'); ?>
			</div>
			<div class="col-md-3">
				<h3>Contact Details</h3>
				<?php dynamic_sidebar('footer-widget-column-3'); ?>
			</div>
			<div class="col-md-3 secure">
				<?php dynamic_sidebar('footer-widget-column-4'); ?>
			</div>
		</div>
	</div>
</footer>

<div class="site-sub-footer">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="credits">site designed and built by <a href="https://www.zincdigital.com">Zinc Digital</a></p>
			<p class="copyright">Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?> - ALL Rights reserved </p>
			<p>	Copyright <?php echo date('Y'); ?> a service delivered by Provide Community Interest Company, a limited company. Registered in England and Wales No. 07320006. Registered address: 900 The Crescent, Colchester Business Park, Colchester, Essex, CO4 9YQ</p>
		</div>
	</div>
</div>
</div>

<?php wp_footer(); ?>

<script>
window.addEventListener("load", function () {
  jQuery('.link').click(function (e) {
		e.preventDefault();
		var target = jQuery(this).attr('href');
		jQuery('html, body').animate({
			scrollTop: jQuery(target).offset().top - 100
		}, 1000)
	});
});
</script>

</body>
</html>
