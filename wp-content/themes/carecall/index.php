<?php get_template_part('includes/header'); ?>

<?php get_template_part('includes/loops/content', get_post_format()); ?>

<?php get_template_part('includes/footer'); ?>
