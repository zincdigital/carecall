<?php get_template_part('includes/header'); ?>

<div class="container post-detail">
  <div class="row">
    <div class="col-lg-12">
      <h1><i class="glyphicon glyphicon-warning-sign"></i> <?php _e('Error', 'theme'); ?> 404</h1>
      <p><?php _e('The page you were looking for does not exist.', 'theme'); ?></p>    
    </div>
  </div>
</div>

<?php get_template_part('includes/footer'); ?>
