<?php
/**
 * Template Name: My Account
 */

get_template_part('includes/header');?>

<!--  Page Banners -->
<?php $banner = get_field('banner'); ?>
<div id="banner">
  <?php echo $banner;?>
</div>

<?php
if(have_posts()): 
	while(have_posts()): the_post(); ?>
		<main id="main" class="site-main shop" role="main">
      <div class="container">
			<?php the_content(); ?>	
      </div>
		</main>
	<?php endwhile; endif; ?>

<?php get_template_part('includes/footer'); ?>