<?php
/*
All the functions are in the PHP pages in the functions/ folder.
*/


require_once locate_template('/functions/setup.php');
require_once locate_template('/functions/enqueues.php');
require_once locate_template('/functions/navbar.php');
require_once locate_template('/functions/widgets.php');
require_once locate_template('/functions/search.php');

add_action('after_setup_theme', 'true_load_theme_textdomain');

function true_load_theme_textdomain(){
    load_theme_textdomain( 'theme', get_template_directory() . '/languages' );
}

function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );


/* Woocommerce */

/* Woocomerce Theme Support*/

function woocommerce_theme_support() {
    add_theme_support( 'woocommerce');
}
 
add_action( 'after_setup_theme', 'woocommerce_theme_support' );



add_filter('woocommerce_show_page_title', '__return_false');
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);


/** * Update AJAX cart on the header */

add_filter( 'woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1 );

function iconic_cart_count_fragments( $fragments ) {
    $fragments['span.header-cart-count'] = '<span class="header-cart-count">(' . WC()->cart->get_cart_contents_count() . ')</span>';
    return $fragments;
}

/** * Remove related products output */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/** * Remove product review from product single page */
add_filter( 'woocommerce_product_tabs', 'helloacm_remove_product_review', 99);
function helloacm_remove_product_review($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}


/** * Remove product category from product single page */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_single_excerpt' );
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

// Up-sells sorting
add_filter( 'woocommerce_upsells_orderby', function() { return 'date'; } );
add_filter( 'woocommerce_upsells_order', function() { return 'desc'; } );

// Cross-sells sorting
add_filter( 'woocommerce_cross_sells_orderby', function() { return 'date'; } );
add_filter( 'woocommerce_cross_sells_order', function() { return 'desc'; } );


// function product_change_title_position() {
// 	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
// 	add_action( 'woocommerce_before_single_product', 'woocommerce_template_single_title', 5 );
// }

// add_action( 'init', 'product_change_title_position' );

add_filter( 'woocommerce_account_menu_items', 'custom_remove_downloads_my_account', 999 );
 
function custom_remove_downloads_my_account( $items ) {
unset($items['downloads']);
return $items;
}

function change_subscription_product_string( $subscription_string, $product, $include )
{
if( $include['sign_up_fee'] ){
$subscription_string = str_replace('sign-up fee', 'set up fee', $subscription_string);
}
return $subscription_string;
}
add_filter( 'woocommerce_subscriptions_product_price_string', 'change_subscription_product_string', 10, 3 );


function custom_pre_get_posts_query( $q ) {

    if (!$q->is_main_query() || !is_shop()) return;
    
    $tax_query = (array) $q->get( 'tax_query' );
    
    $tax_query[] = array(
           'taxonomy' => 'product_cat',
           'field' => 'slug',
           'terms' => array( 'plan', 'uncategorized' ),
           'operator' => 'NOT IN'
    );
    
    
    $q->set( 'tax_query', $tax_query );
    
    }
    add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' );


    /**
 * Change the subscription thank you message after purchase
 *
 * @param  int $order_id
 * @return string
 */
function custom_subscription_thank_you( $order_id ){

    if( WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) {
        $thank_you_message = sprintf( __( '%sThank you for purchasign our subscription! Visit %syour account%s page to know its status.%s', 'woocommerce-subscriptions' ), '<p>', '<a href="' . get_permalink( wc_get_page_id( 'myaccount' ) ) . '">', '</a>','</p>' );

        return $thank_you_message;
    }

}
add_filter( 'woocommerce_subscriptions_thank_you_message', 'custom_subscription_thank_you');