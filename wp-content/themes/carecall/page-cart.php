<?php
/**
 * Template Name: Cart
 */

get_template_part('includes/header');?>

<!--  Page Banners -->
<?php $banner = get_field('banner'); ?>
<div id="banner">
  <?php echo $banner;?>
</div>


<?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
  <div class="container">
    <div class="row intro">
      <?php $columns = get_sub_field('columns'); $title = get_sub_field('title'); $textone = get_sub_field('text-1'); $texttwo = get_sub_field('text-2'); $link = get_sub_field('link'); $linktitle = get_sub_field('link-title'); ?>
      <?php if($columns == '1') :?>
      <div class="col-lg-12">
        <h2 class="title"><?=$title?></h2>
        <?=$textone?>
        <p><a href="<?=$link?>" class="btn multi"><?=$linktitle?></a></p>
      </div>
      <?php elseif ($columns == '2') : ?>
      <div class="col-lg-6">
        <h2 class="title"><?=$title?></h2>
        <?=$textone?>
        <p><a href="<?=$link?>" class="btn multi"><?=$linktitle?></a></p>
      </div>
      <div class="col-lg-6">
        <?=$texttwo?>
      </div>
      <?php endif;?>
      <?php endwhile;?>
    </div>
  </div>
  <?php endif; ?>

<?php
if(have_posts()): 
	while(have_posts()): the_post(); ?>
		<main id="main" class="site-main shop" role="main">
      <div class="container">
			<?php the_content(); ?>	
      </div>
		</main>
	<?php endwhile; endif; ?>

<?php get_template_part('includes/footer'); ?>