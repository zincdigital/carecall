<?php

function theme_enqueues() {

	/* Styles */

	wp_register_style('bootstrap-css', get_template_directory_uri() . '/assets/bootstrap/bootstrap.min.css', false, '4.1.0', null);
	wp_enqueue_style('bootstrap-css');

	wp_register_style('owl-css', get_template_directory_uri() . '/assets/owl/owl.carousel.min.css', false, null);
	wp_enqueue_style('owl-css');

	wp_register_style('owl-default-css', get_template_directory_uri() . '/assets/owl/owl.theme.default.min.css', false, null);
	wp_enqueue_style('owl-default-css');

  wp_register_style('theme-css', get_template_directory_uri() . '/assets/css/main.css', false, '1.0.6', null);
	wp_enqueue_style('theme-css');

	/* Scripts */

	wp_enqueue_script( 'jquery' );
	/* Note: this above uses WordPress's onboard jQuery. You can enqueue other pre-registered scripts from WordPress too. See:
	https://developer.wordpress.org/reference/functions/wp_enqueue_script/#Default_Scripts_Included_and_Registered_by_WordPress */

  wp_register_script('modernizr', get_template_directory_uri() . '/assets/js/modernizr-2.8.3.min.js', false, null, true);
	wp_enqueue_script('modernizr');

	wp_register_script('owl-js', get_template_directory_uri() . '/assets/owl/owl.carousel.min.js', false, null, true);
	wp_enqueue_script('owl-js');

  wp_register_script('bootstrap-js', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', false, null, true);
	wp_enqueue_script('bootstrap-js');

	wp_register_script('theme-js', get_template_directory_uri() . '/assets/js/theme.js', false, null, true);
	wp_enqueue_script('theme-js');

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'theme_enqueues', 100);
