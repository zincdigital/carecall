<?php

function theme_widgets_init() {

  /*
  Footer (three widget areas)
   */
  register_sidebar( array(
    'name'            => __( 'Footer Column 1', 'theme' ),
    'id'              => 'footer-widget-column-1',
    'description'     => __( 'The footer widget area', 'theme' ),
    'before_widget'   => '<div class="%1$s %2$s">',
    'after_widget'    => '</div>',
    'before_title'    => '<h4>',
    'after_title'     => '</h4>',
  ) );
  register_sidebar( array(
    'name'            => __( 'Footer Column 2', 'theme' ),
    'id'              => 'footer-widget-column-2',
    'description'     => __( 'The footer widget area', 'theme' ),
    'before_widget'   => '<div class="%1$s %2$s">',
    'after_widget'    => '</div>',
    'before_title'    => '<h4>',
    'after_title'     => '</h4>',
  ) );
  register_sidebar( array(
    'name'            => __( 'Footer Column 3', 'theme' ),
    'id'              => 'footer-widget-column-3',
    'description'     => __( 'The footer widget area', 'theme' ),
    'before_widget'   => '<div class="%1$s %2$s">',
    'after_widget'    => '</div>',
    'before_title'    => '<h4>',
    'after_title'     => '</h4>',
  ) );
  register_sidebar( array(
    'name'            => __( 'Footer Column 4', 'theme' ),
    'id'              => 'footer-widget-column-4',
    'description'     => __( 'The footer widget area', 'theme' ),
    'before_widget'   => '<div class="%1$s %2$s">',
    'after_widget'    => '</div>',
    'before_title'    => '<h4>',
    'after_title'     => '</h4>',
  ) );
  register_sidebar( array(
    'name'            => __( 'Mini Cart', 'theme' ),
    'id'              => 'mini-cart',
    'description'     => __( 'Mini Cart Area', 'theme' ),
    'before_widget'   => '<div class="%1$s %2$s">',
    'after_widget'    => '</div>',
    'before_title'    => '<h4>',
    'after_title'     => '</h4>',
  ) );

}
add_action( 'widgets_init', 'theme_widgets_init' );
