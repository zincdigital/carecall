<?php
/**
 * Template Name: Plans
 */

get_template_part('includes/header');?>

<script>
 
</script>


<!--  Page Banners -->
<?php $banner = get_field('banner'); ?>
<div id="banner">
  <?php echo $banner;?>
</div>


<?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
  <div class="container">
    <div class="row intro">
      <?php $columns = get_sub_field('columns'); $title = get_sub_field('title'); $textone = get_sub_field('text-1'); $texttwo = get_sub_field('text-2'); $link = get_sub_field('link'); $linktitle = get_sub_field('link-title'); $anchor_link = get_sub_field('anchor_link'); ?>
      <?php if($columns == '1') :?>
      <div class="col-lg-12">
        <h2 class="title"><?=$title?></h2>
        <?=$textone?>
        <?php if(!empty($link)) :?>
          <p><a href="<?=$link?>" class="btn multi"><?=$linktitle?></a></p>
        <?php else : ?>
          <p><a href="<?=$anchor_link?>" class="btn multi link"><?=$linktitle?></a></p>
        <?php endif;?>
      </div>
      <?php elseif ($columns == '2') : ?>
      <div class="col-lg-6">
        <h2 class="title"><?=$title?></h2>
        <?=$textone?>
        <?php if(!empty($link)) :?>
          <p><a href="<?=$link?>" class="btn multi"><?=$linktitle?></a></p>
        <?php else : ?>
          <p><a href="<?=$anchor_link?>" class="btn multi link"><?=$linktitle?></a></p>
        <?php endif;?>
      </div>
      <div class="col-lg-6">
        <?=$texttwo?>
      </div>
      <?php endif;?>
      <?php endwhile;?>
    </div>
  </div>
  <?php endif; ?>

<?php
if(have_posts()): 
	while(have_posts()): the_post(); ?>
		<main id="plans" class="site-main shop" role="main">
      <div class="container">
			<?php the_content(); ?>	
      </div>
		</main>
	<?php endwhile; endif; ?>


  <div class="block-content purple white-text">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <?php $content = get_field('content-block'); ?>
          <?php echo $content;?>
        </div>
      </div>
    </div>
  </div>


  <div class="testimonials">
    <div class="owl-carousel" id="testimonials"
      style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/quotes.png') no-repeat; background-size: 868px; background-position: center !important; height: 315px; ">
      <?php if( have_rows('testimonials') ): while ( have_rows('testimonials') ) : the_row(); ?>
      <?php $title = get_sub_field('title'); $text = get_sub_field('text'); $author = get_sub_field('author'); $background = get_sub_field('background-image');?>
      <div class="item">
        <div class="inner-item">
          <p class="title"><?=$title?></p>
          <p><?=$text?></p>
          <p class="author"><?=$author?></p>
        </div>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>

<?php get_template_part('includes/footer'); ?>